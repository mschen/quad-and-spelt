#ifndef _QUAD_API_H_
#define _QUAD_API_H_

#include <stdint.h>

typedef
struct {
union{
uint8_t v8[N_VAR/8];
uint32_t v32[N_VAR/32];
};
} State;

typedef
struct {
uint64_t v64[2*(N_VAR)/64];
} Col;

typedef
struct {
Col terms[ N_VAR + N_VAR + (N_VAR)*(N_VAR-1)/2];

} Key;


void eval_polys( unsigned char * output , State * state , const Key * polys );





#endif /// _QUAD_API_H_
