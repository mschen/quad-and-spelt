int64 r11
int64 r12
int64 r13
int64 r14
int64 r15
int64 rbx
int64 rbp

caller r11
caller r12
caller r13
caller r14
caller r15
caller rbx
caller rbp

stack64 r11_stack
stack64 r12_stack
stack64 r13_stack
stack64 r14_stack
stack64 r15_stack
stack64 rbx_stack
stack64 rbp_stack


int64 ptr_y
int64 ptr_p
int64 ptr_x

input ptr_y
input ptr_p
input ptr_x

int64 ptr_n
int64 y0
int64 y1
int64 y2
int64 y3
int64 y4
int64 y5
int64 y6

int64 x0
int64 x1
int64 x2
int64 x3

int64 counter
int64 out_counter
int64 offset


stack64 stack_56
stack64 stack_63

stack64 stack_1
stack64 stack_2
stack64 stack_3
stack64 stack_4
stack64 stack_5
stack64 stack_6


int64 c0
int64 c1
int64 c2
int64 c3
int64 q0
int64 q1
int64 q2
int64 zero

enter eval_squad_asm2



r11_stack = r11
r12_stack = r12
r13_stack = r13
r14_stack = r14
r15_stack = r15
rbx_stack = rbx
rbp_stack = rbp

offset = 8
stack_56 = offset

x0 = *(int64 *)(ptr_x + 0)
x1 = *(int64 *)(ptr_x + 8)
x2 = *(int64 *)(ptr_x + 16)
x3 = *(int64 *)(ptr_x + 24)

# read constant term
y0 = *(int64 *)(ptr_p + 0)
y1 = *(int64 *)(ptr_p + 8)
y2 = *(int64 *)(ptr_p + 16)
y3 = *(int64 *)(ptr_p + 24)
y4 = *(int64 *)(ptr_p + 32)
y5 = *(int64 *)(ptr_p + 40)
y6 = *(int64 *)(ptr_p + 48)
ptr_p += 56


#
#   Linear Term
#

counter = 0
loop_start0:
carry? x0 <bt> (uint32) counter
goto skip0 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip0:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start0 if !=


counter = 0
loop_start1:
carry? x1 <bt> (uint32) counter
goto skip1 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip1:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start1 if !=


counter = 0
loop_start2:
carry? x2 <bt> (uint32) counter
goto skip2 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip2:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start2 if !=

counter = 0
loop_start3:
carry? x3 <bt> (uint32) counter
goto skip3 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip3:
ptr_p += 56
counter += 1
=? counter - 32
goto loop_start3 if !=





################################################################################

stack_1 = y1
stack_2 = y2
stack_3 = y3
stack_4 = y4
stack_5 = y5
stack_6 = y6



c0 = 0
zero = 0
q0 = 0



##########################################################################

out_counter = 0
cubic_y0_start:

c2 = 0
q2 = 0


#################  cubic term   #############################
counter = 0
cubic_y0_inner:
c2 <btc> (uint32) out_counter
c0 = *(uint8 *)(ptr_p + 0)
carry ? x0 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 1)
carry ? x1 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 2)
carry ? x2 <bt> (uint32) c0
c2 = c1 if !carry
ptr_p += 3
y0 ^= c2
counter += 1
=? counter - 40
goto cubic_y0_inner if !=

out_counter += 1
=? out_counter - 64
goto cubic_y0_start if !=

*(int64 *)(ptr_y + 0) = y0
####################################################################
y1 = stack_1

out_counter = 0
cubic_y1_start:

c2 = 0
q2 = 0


#################  cubic term   #############################
counter = 0
cubic_y1_inner:
c2 <btc> (uint32) out_counter
c0 = *(uint8 *)(ptr_p + 0)
carry ? x0 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 1)
carry ? x1 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 2)
carry ? x2 <bt> (uint32) c0
c2 = c1 if !carry
ptr_p += 3
y1 ^= c2
counter += 1
=? counter - 40
goto cubic_y1_inner if !=

out_counter += 1
=? out_counter - 64
goto cubic_y1_start if !=

*(int64 *)(ptr_y + 8) = y1
####################################################################
y2 = stack_2

out_counter = 0
cubic_y2_start:

c2 = 0
q2 = 0


#################  cubic term   #############################
counter = 0
cubic_y2_inner:
c2 <btc> (uint32) out_counter
c0 = *(uint8 *)(ptr_p + 0)
carry ? x0 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 1)
carry ? x1 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 2)
carry ? x2 <bt> (uint32) c0
c2 = c1 if !carry
ptr_p += 3
y2 ^= c2
counter += 1
=? counter - 40
goto cubic_y2_inner if !=

out_counter += 1
=? out_counter - 64
goto cubic_y2_start if !=

*(int64 *)(ptr_y + 16) = y2
####################################################################
y3 = stack_3


out_counter = 0
cubic_y3_start:

c2 = 0
q2 = 0


#################  cubic term   #############################
counter = 0
cubic_y3_inner:
c2 <btc> (uint32) out_counter
c0 = *(uint8 *)(ptr_p + 0)
carry ? x0 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 1)
carry ? x1 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 2)
carry ? x2 <bt> (uint32) c0
c2 = c1 if !carry
ptr_p += 3
y3 ^= c2
counter += 1
=? counter - 40
goto cubic_y3_inner if !=

out_counter += 1
=? out_counter - 64
goto cubic_y3_start if !=

*(int64 *)(ptr_y + 24) = y3


####################################################################
y4 = stack_4


out_counter = 0
cubic_y4_start:

c2 = 0
q2 = 0

#################  cubic term   #############################
counter = 0
cubic_y4_inner:
c2 <btc> (uint32) out_counter
c0 = *(uint8 *)(ptr_p + 0)
carry ? x0 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 1)
carry ? x1 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 2)
carry ? x2 <bt> (uint32) c0
c2 = c1 if !carry
ptr_p += 3
y4 ^= c2
counter += 1
=? counter - 40
goto cubic_y4_inner if !=

out_counter += 1
=? out_counter - 64
goto cubic_y4_start if !=

*(int64 *)(ptr_y + 32) = y4
####################################################################
y5 = stack_5

out_counter = 0
cubic_y5_start:

c2 = 0
q2 = 0


#################  cubic term   #############################
counter = 0
cubic_y5_inner:
c2 <btc> (uint32) out_counter
c0 = *(uint8 *)(ptr_p + 0)
carry ? x0 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 1)
carry ? x1 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 2)
carry ? x2 <bt> (uint32) c0
c2 = c1 if !carry
ptr_p += 3
y5 ^= c2
counter += 1
=? counter - 40
goto cubic_y5_inner if !=

out_counter += 1
=? out_counter - 64
goto cubic_y5_start if !=

*(int64 *)(ptr_y + 40) = y5
####################################################################
y6 = stack_6


out_counter = 0
cubic_y6_start:

c2 = 0


#################  cubic term   #############################
counter = 0
cubic_y6_inner:
c2 <btc> (uint32) out_counter
c0 = *(uint8 *)(ptr_p + 0)
carry ? x0 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 1)
carry ? x1 <bt> (uint32) c0
c2 = c1 if !carry
c0 = *(uint8 *)(ptr_p + 2)
carry ? x2 <bt> (uint32) c0
c2 = c1 if !carry
ptr_p += 3
y6 ^= c2
counter += 1
=? counter - 40
goto cubic_y6_inner if !=

out_counter += 1
=? out_counter - 64
goto cubic_y6_start if !=

*(int64 *)(ptr_y + 48) = y6
####################################################################





r11 = r11_stack
r12 = r12_stack
r13 = r13_stack
r14 = r14_stack
r15 = r15_stack
rbx = rbx_stack
rbp = rbp_stack

leave
