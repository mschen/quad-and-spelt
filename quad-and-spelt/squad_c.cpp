
#include "squad_c.h"


//struct squad_poly
//{
//	uint64_t c[7];
//	uint64_t l[224][7];
//	uint16_t q_idx[224*223/2][4]; // 4 for padding
//	uint8_t tri_idx[448][120];
//};

#define NUM_VARS 224
#define NUM_POLYS 448

void rand_poly( squad_poly * polys )
{
	rand_x( polys->c , NUM_POLYS );
	for(int i=0;i<NUM_VARS;i++) rand_x( polys->l[i] , NUM_POLYS );

	int idx = 0;
	for(int i=0;i<NUM_VARS;i++) {
		for(int j=i+1;j<NUM_VARS;j++){
//			for(int k=0;k<3;k++) polys->q_idx[idx][k] = rand()%(NUM_POLYS);
			for(int k=0;k<3;k++) polys->q_idx[idx][k] = rand()%64;
			idx++;
		}
	}
	
	for(int i=0;i<NUM_POLYS;i++) {
		for(int j=0;j<120;j++){
//			polys->tri_idx[i][j] = rand()%NUM_VARS;
			polys->tri_idx[i][j] = rand()%64;
		}	
	}
}


//struct squad_poly
//{
//	uint64_t c[7];
//	uint64_t l[224][7];
//	uint16_t q_idx[224*223/2][4]; // 4 for padding
//	uint8_t tri_idx[448][120];
//};

void eval_c( uint64_t y[7] , squad_poly * polys , uint64_t x[4] )
{
	for(int i=0;i<7;i++) y[i] = polys->c[i];
	for(int j=0;j<NUM_VARS;j++){
		if( !get_bit( x[j>>6] , j&63 )) continue;
		for(int i=0;i<7;i++) y[i] ^= (polys->l[j][i]);
	}
	/// index compiled into code
	int idx = 0;
	for(int j=0;j<NUM_VARS;j++) {
		if( !get_bit(x[j>>6],j&63) ) { idx+=(NUM_VARS-1-j); continue; }
		for(int k=j+1;k<NUM_VARS;k++){
			if(!get_bit(x[k>>6],k&63)) { idx++; continue; }	
			for(int i=0;i<3;i++) { flip_bit( y[(polys->q_idx[idx][i])>>6] , (polys->q_idx[idx][i])&63); }
			idx++;
		}
	}
	///
	for(int i=0;i<NUM_POLYS;i++){
		for(int j=0;j<40;j++){
			uint8_t b = polys->tri_idx[i][j*3]; if( !get_bit(x[b>>6],b&63) ) continue;
			b = polys->tri_idx[i][j*3+1]; if( !get_bit(x[b>>6],b&63) ) continue;
			b = polys->tri_idx[i][j*3+2]; if( !get_bit(x[b>>6],b&63) ) continue;
			flip_bit( y[i>>6] , i&63 );
		}
	}
}



// 448 poly , 224 variable
//struct squad_poly2
//{
//	uint64_t c[7];
//	uint64_t l[224][7];
//	uint8_t q_idx[448][512]; // 256 quadratic term each polynomial
//	uint8_t tri_idx[448][120];  // 40 cubic term each polynomail
//};

void rand_poly2( squad_poly2 * polys )
{
	rand_x( polys->c , NUM_POLYS );
	for(int i=0;i<NUM_VARS;i++) rand_x( polys->l[i] , NUM_POLYS );
	
	for(int i=0;i<NUM_POLYS;i++) {
		for(int j=0;j<512;j++) polys->q_idx[i][j] = rand()%64;
		for(int j=0;j<120;j++) polys->tri_idx[i][j] = rand()%64;
	}
}
