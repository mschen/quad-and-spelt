
# qhasm: int64 r11

# qhasm: int64 r12

# qhasm: int64 r13

# qhasm: int64 r14

# qhasm: int64 r15

# qhasm: int64 rbx

# qhasm: int64 rbp

# qhasm: caller r11

# qhasm: caller r12

# qhasm: caller r13

# qhasm: caller r14

# qhasm: caller r15

# qhasm: caller rbx

# qhasm: caller rbp

# qhasm: stack64 r11_stack

# qhasm: stack64 r12_stack

# qhasm: stack64 r13_stack

# qhasm: stack64 r14_stack

# qhasm: stack64 r15_stack

# qhasm: stack64 rbx_stack

# qhasm: stack64 rbp_stack

# qhasm: int64 ptr_y

# qhasm: int64 ptr_p

# qhasm: int64 ptr_x

# qhasm: input ptr_y

# qhasm: input ptr_p

# qhasm: input ptr_x

# qhasm: int64 ptr_n

# qhasm: int64 y0

# qhasm: int64 y1

# qhasm: int64 y2

# qhasm: int64 y3

# qhasm: int64 y4

# qhasm: int64 y5

# qhasm: int64 y6

# qhasm: int64 x0

# qhasm: int64 x1

# qhasm: int64 x2

# qhasm: int64 x3

# qhasm: int64 counter

# qhasm: int64 out_counter

# qhasm: int64 offset

# qhasm: stack64 stack_56

# qhasm: stack64 stack_63

# qhasm: stack64 stack_1

# qhasm: stack64 stack_2

# qhasm: stack64 stack_3

# qhasm: stack64 stack_4

# qhasm: stack64 stack_5

# qhasm: stack64 stack_6

# qhasm: int64 c0

# qhasm: int64 c1

# qhasm: int64 c2

# qhasm: int64 c3

# qhasm: enter eval_squad_asm4
.text
.p2align 5
.globl _eval_squad_asm4
.globl eval_squad_asm4
_eval_squad_asm4:
eval_squad_asm4:
mov %rsp,%r11
and $31,%r11
add $96,%r11
sub %r11,%rsp

# qhasm: offset = 63
# asm 1: mov  $63,>offset=int64#4
# asm 2: mov  $63,>offset=%rcx
mov  $63,%rcx

# qhasm: stack_63 = offset
# asm 1: movq <offset=int64#4,>stack_63=stack64#1
# asm 2: movq <offset=%rcx,>stack_63=0(%rsp)
movq %rcx,0(%rsp)

# qhasm: r11_stack = r11
# asm 1: movq <r11=int64#9,>r11_stack=stack64#2
# asm 2: movq <r11=%r11,>r11_stack=8(%rsp)
movq %r11,8(%rsp)

# qhasm: r12_stack = r12
# asm 1: movq <r12=int64#10,>r12_stack=stack64#3
# asm 2: movq <r12=%r12,>r12_stack=16(%rsp)
movq %r12,16(%rsp)

# qhasm: r13_stack = r13
# asm 1: movq <r13=int64#11,>r13_stack=stack64#4
# asm 2: movq <r13=%r13,>r13_stack=24(%rsp)
movq %r13,24(%rsp)

# qhasm: r14_stack = r14
# asm 1: movq <r14=int64#12,>r14_stack=stack64#5
# asm 2: movq <r14=%r14,>r14_stack=32(%rsp)
movq %r14,32(%rsp)

# qhasm: r15_stack = r15
# asm 1: movq <r15=int64#13,>r15_stack=stack64#6
# asm 2: movq <r15=%r15,>r15_stack=40(%rsp)
movq %r15,40(%rsp)

# qhasm: rbx_stack = rbx
# asm 1: movq <rbx=int64#14,>rbx_stack=stack64#7
# asm 2: movq <rbx=%rbx,>rbx_stack=48(%rsp)
movq %rbx,48(%rsp)

# qhasm: rbp_stack = rbp
# asm 1: movq <rbp=int64#15,>rbp_stack=stack64#8
# asm 2: movq <rbp=%rbp,>rbp_stack=56(%rsp)
movq %rbp,56(%rsp)

# qhasm: offset = 8
# asm 1: mov  $8,>offset=int64#4
# asm 2: mov  $8,>offset=%rcx
mov  $8,%rcx

# qhasm: stack_56 = offset
# asm 1: movq <offset=int64#4,>stack_56=stack64#9
# asm 2: movq <offset=%rcx,>stack_56=64(%rsp)
movq %rcx,64(%rsp)

# qhasm: x0 = *(int64 *)(ptr_x + 0)
# asm 1: movq   0(<ptr_x=int64#3),>x0=int64#4
# asm 2: movq   0(<ptr_x=%rdx),>x0=%rcx
movq   0(%rdx),%rcx

# qhasm: x1 = *(int64 *)(ptr_x + 8)
# asm 1: movq   8(<ptr_x=int64#3),>x1=int64#5
# asm 2: movq   8(<ptr_x=%rdx),>x1=%r8
movq   8(%rdx),%r8

# qhasm: x2 = *(int64 *)(ptr_x + 16)
# asm 1: movq   16(<ptr_x=int64#3),>x2=int64#6
# asm 2: movq   16(<ptr_x=%rdx),>x2=%r9
movq   16(%rdx),%r9

# qhasm: x3 = *(int64 *)(ptr_x + 24)
# asm 1: movq   24(<ptr_x=int64#3),>x3=int64#3
# asm 2: movq   24(<ptr_x=%rdx),>x3=%rdx
movq   24(%rdx),%rdx

# qhasm: y0 = *(int64 *)(ptr_p + 0)
# asm 1: movq   0(<ptr_p=int64#2),>y0=int64#7
# asm 2: movq   0(<ptr_p=%rsi),>y0=%rax
movq   0(%rsi),%rax

# qhasm: y1 = *(int64 *)(ptr_p + 8)
# asm 1: movq   8(<ptr_p=int64#2),>y1=int64#8
# asm 2: movq   8(<ptr_p=%rsi),>y1=%r10
movq   8(%rsi),%r10

# qhasm: y2 = *(int64 *)(ptr_p + 16)
# asm 1: movq   16(<ptr_p=int64#2),>y2=int64#9
# asm 2: movq   16(<ptr_p=%rsi),>y2=%r11
movq   16(%rsi),%r11

# qhasm: y3 = *(int64 *)(ptr_p + 24)
# asm 1: movq   24(<ptr_p=int64#2),>y3=int64#10
# asm 2: movq   24(<ptr_p=%rsi),>y3=%r12
movq   24(%rsi),%r12

# qhasm: y4 = *(int64 *)(ptr_p + 32)
# asm 1: movq   32(<ptr_p=int64#2),>y4=int64#11
# asm 2: movq   32(<ptr_p=%rsi),>y4=%r13
movq   32(%rsi),%r13

# qhasm: y5 = *(int64 *)(ptr_p + 40)
# asm 1: movq   40(<ptr_p=int64#2),>y5=int64#12
# asm 2: movq   40(<ptr_p=%rsi),>y5=%r14
movq   40(%rsi),%r14

# qhasm: y6 = *(int64 *)(ptr_p + 48)
# asm 1: movq   48(<ptr_p=int64#2),>y6=int64#13
# asm 2: movq   48(<ptr_p=%rsi),>y6=%r15
movq   48(%rsi),%r15

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start0:
._loop_start0:

# qhasm: carry? x0 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x0=int64#4
# asm 2: bt <counter=%ebx,<x0=%rcx
bt %ebx,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto skip0 if !carry
jnc ._skip0

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip0:
._skip0:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start0 if !=
jne ._loop_start0

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start1:
._loop_start1:

# qhasm: carry? x1 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x1=int64#5
# asm 2: bt <counter=%ebx,<x1=%r8
bt %ebx,%r8
# comment:fp stack unchanged by jump

# qhasm: goto skip1 if !carry
jnc ._skip1

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip1:
._skip1:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start1 if !=
jne ._loop_start1

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start2:
._loop_start2:

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x2=int64#6
# asm 2: bt <counter=%ebx,<x2=%r9
bt %ebx,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip2 if !carry
jnc ._skip2

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip2:
._skip2:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start2 if !=
jne ._loop_start2

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start3:
._loop_start3:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x3=int64#3
# asm 2: bt <counter=%ebx,<x3=%rdx
bt %ebx,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip3 if !carry
jnc ._skip3

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip3:
._skip3:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#14
# asm 2: cmp  $32,<counter=%rbx
cmp  $32,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start3 if !=
jne ._loop_start3

# qhasm: stack_1 = y4
# asm 1: movq <y4=int64#11,>stack_1=stack64#1
# asm 2: movq <y4=%r13,>stack_1=0(%rsp)
movq %r13,0(%rsp)

# qhasm: stack_2 = y5
# asm 1: movq <y5=int64#12,>stack_2=stack64#10
# asm 2: movq <y5=%r14,>stack_2=72(%rsp)
movq %r14,72(%rsp)

# qhasm: stack_3 = y6
# asm 1: movq <y6=int64#13,>stack_3=stack64#11
# asm 2: movq <y6=%r15,>stack_3=80(%rsp)
movq %r15,80(%rsp)

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#11
# asm 2: mov  $0,>out_counter=%r13
mov  $0,%r13

# qhasm: 0000_outer_start:
._0000_outer_start:

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#11
# asm 2: cmp  $64,<out_counter=%r13
cmp  $64,%r13
# comment:fp stack unchanged by jump

# qhasm: goto 0000_outer_end if =
je ._0000_outer_end

# qhasm: carry? x0 <bt> (uint32) out_counter
# asm 1: bt <out_counter=int64#11d,<x0=int64#4
# asm 2: bt <out_counter=%r13d,<x0=%rcx
bt %r13d,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 0000_inner_start if carry
jc ._0000_inner_start

# qhasm: offset = 223
# asm 1: mov  $223,>offset=int64#12
# asm 2: mov  $223,>offset=%r14
mov  $223,%r14

# qhasm: offset -= out_counter
# asm 1: sub  <out_counter=int64#11,<offset=int64#12
# asm 2: sub  <out_counter=%r13,<offset=%r14
sub  %r13,%r14

# qhasm: offset *= stack_56
# asm 1: imul  <stack_56=stack64#9,<offset=int64#12
# asm 2: imul  <stack_56=64(%rsp),<offset=%r14
imul  64(%rsp),%r14

# qhasm: ptr_p += offset
# asm 1: add  <offset=int64#12,<ptr_p=int64#2
# asm 2: add  <offset=%r14,<ptr_p=%rsi
add  %r14,%rsi

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#11
# asm 2: add  $1,<out_counter=%r13
add  $1,%r13
# comment:fp stack unchanged by jump

# qhasm: goto 0000_outer_start
jmp ._0000_outer_start

# qhasm: 0000_inner_start:
._0000_inner_start:

# qhasm: counter = 1
# asm 1: mov  $1,>counter=int64#12
# asm 2: mov  $1,>counter=%r14
mov  $1,%r14

# qhasm: counter += out_counter
# asm 1: add  <out_counter=int64#11,<counter=int64#12
# asm 2: add  <out_counter=%r13,<counter=%r14
add  %r13,%r14

# qhasm: 0000_inner_main:
._0000_inner_main:

# qhasm: unsigned>? counter - 63
# asm 1: cmp  $63,<counter=int64#12
# asm 2: cmp  $63,<counter=%r14
cmp  $63,%r14
# comment:fp stack unchanged by jump

# qhasm: goto 0000_inner_end if unsigned>
ja ._0000_inner_end

# qhasm: carry? x0 <bt> (uint32) counter
# asm 1: bt <counter=int64#12d,<x0=int64#4
# asm 2: bt <counter=%r14d,<x0=%rcx
bt %r14d,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto skip00 if !carry
jnc ._skip00

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r15
movzwq 0(%rsi),%r15

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#13w,<y0=int64#7
# asm 2: btc <c0=%r15w,<y0=%rax
btc %r15w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#13
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r15
movzwq 2(%rsi),%r15

# qhasm: y1 <btc> (uint16) c1
# asm 1: btc <c1=int64#13w,<y1=int64#8
# asm 2: btc <c1=%r15w,<y1=%r10
btc %r15w,%r10

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#13
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r15
movzwq 4(%rsi),%r15

# qhasm: y2 <btc> (uint16) c2
# asm 1: btc <c2=int64#13w,<y2=int64#9
# asm 2: btc <c2=%r15w,<y2=%r11
btc %r15w,%r11

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r15
movzwq 6(%rsi),%r15

# qhasm: y3 <btc> (uint16) c0
# asm 1: btc <c0=int64#13w,<y3=int64#10
# asm 2: btc <c0=%r15w,<y3=%r12
btc %r15w,%r12
# comment:fp stack unchanged by fallthrough

# qhasm: skip00:
._skip00:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#12
# asm 2: add  $1,<counter=%r14
add  $1,%r14
# comment:fp stack unchanged by jump

# qhasm: goto 0000_inner_main
jmp ._0000_inner_main

# qhasm: 0000_inner_end:
._0000_inner_end:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#12
# asm 2: mov  $0,>counter=%r14
mov  $0,%r14

# qhasm: loop_start01:
._loop_start01:

# qhasm: carry? x1 <bt> (uint32) counter
# asm 1: bt <counter=int64#12d,<x1=int64#5
# asm 2: bt <counter=%r14d,<x1=%r8
bt %r14d,%r8
# comment:fp stack unchanged by jump

# qhasm: goto skip01 if !carry
jnc ._skip01

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r15
movzwq 0(%rsi),%r15

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#13w,<y0=int64#7
# asm 2: btc <c0=%r15w,<y0=%rax
btc %r15w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#13
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r15
movzwq 2(%rsi),%r15

# qhasm: y1 <btc> (uint16) c1
# asm 1: btc <c1=int64#13w,<y1=int64#8
# asm 2: btc <c1=%r15w,<y1=%r10
btc %r15w,%r10

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#13
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r15
movzwq 4(%rsi),%r15

# qhasm: y2 <btc> (uint16) c2
# asm 1: btc <c2=int64#13w,<y2=int64#9
# asm 2: btc <c2=%r15w,<y2=%r11
btc %r15w,%r11

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r15
movzwq 6(%rsi),%r15

# qhasm: y3 <btc> (uint16) c0
# asm 1: btc <c0=int64#13w,<y3=int64#10
# asm 2: btc <c0=%r15w,<y3=%r12
btc %r15w,%r12
# comment:fp stack unchanged by fallthrough

# qhasm: skip01:
._skip01:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#12
# asm 2: add  $1,<counter=%r14
add  $1,%r14

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#12
# asm 2: cmp  $64,<counter=%r14
cmp  $64,%r14
# comment:fp stack unchanged by jump

# qhasm: goto loop_start01 if !=
jne ._loop_start01

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#12
# asm 2: mov  $0,>counter=%r14
mov  $0,%r14

# qhasm: loop_start02:
._loop_start02:

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#12d,<x2=int64#6
# asm 2: bt <counter=%r14d,<x2=%r9
bt %r14d,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip02 if !carry
jnc ._skip02

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r15
movzwq 0(%rsi),%r15

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#13w,<y0=int64#7
# asm 2: btc <c0=%r15w,<y0=%rax
btc %r15w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#13
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r15
movzwq 2(%rsi),%r15

# qhasm: y1 <btc> (uint16) c1
# asm 1: btc <c1=int64#13w,<y1=int64#8
# asm 2: btc <c1=%r15w,<y1=%r10
btc %r15w,%r10

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#13
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r15
movzwq 4(%rsi),%r15

# qhasm: y2 <btc> (uint16) c2
# asm 1: btc <c2=int64#13w,<y2=int64#9
# asm 2: btc <c2=%r15w,<y2=%r11
btc %r15w,%r11

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r15
movzwq 6(%rsi),%r15

# qhasm: y3 <btc> (uint16) c0
# asm 1: btc <c0=int64#13w,<y3=int64#10
# asm 2: btc <c0=%r15w,<y3=%r12
btc %r15w,%r12
# comment:fp stack unchanged by fallthrough

# qhasm: skip02:
._skip02:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#12
# asm 2: add  $1,<counter=%r14
add  $1,%r14

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#12
# asm 2: cmp  $64,<counter=%r14
cmp  $64,%r14
# comment:fp stack unchanged by jump

# qhasm: goto loop_start02 if !=
jne ._loop_start02

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#12
# asm 2: mov  $0,>counter=%r14
mov  $0,%r14

# qhasm: loop_start03:
._loop_start03:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#12d,<x3=int64#3
# asm 2: bt <counter=%r14d,<x3=%rdx
bt %r14d,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip03 if !carry
jnc ._skip03

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r15
movzwq 0(%rsi),%r15

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#13w,<y0=int64#7
# asm 2: btc <c0=%r15w,<y0=%rax
btc %r15w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#13
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r15
movzwq 2(%rsi),%r15

# qhasm: y1 <btc> (uint16) c1
# asm 1: btc <c1=int64#13w,<y1=int64#8
# asm 2: btc <c1=%r15w,<y1=%r10
btc %r15w,%r10

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#13
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r15
movzwq 4(%rsi),%r15

# qhasm: y2 <btc> (uint16) c2
# asm 1: btc <c2=int64#13w,<y2=int64#9
# asm 2: btc <c2=%r15w,<y2=%r11
btc %r15w,%r11

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r15
movzwq 6(%rsi),%r15

# qhasm: y3 <btc> (uint16) c0
# asm 1: btc <c0=int64#13w,<y3=int64#10
# asm 2: btc <c0=%r15w,<y3=%r12
btc %r15w,%r12
# comment:fp stack unchanged by fallthrough

# qhasm: skip03:
._skip03:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#12
# asm 2: add  $1,<counter=%r14
add  $1,%r14

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#12
# asm 2: cmp  $32,<counter=%r14
cmp  $32,%r14
# comment:fp stack unchanged by jump

# qhasm: goto loop_start03 if !=
jne ._loop_start03

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#11
# asm 2: add  $1,<out_counter=%r13
add  $1,%r13
# comment:fp stack unchanged by jump

# qhasm: goto 0000_outer_start
jmp ._0000_outer_start

# qhasm: 0000_outer_end:
._0000_outer_end:

# qhasm: y4 = stack_1 
# asm 1: movq <stack_1=stack64#1,>y4=int64#11
# asm 2: movq <stack_1=0(%rsp),>y4=%r13
movq 0(%rsp),%r13

# qhasm: y5 = stack_2
# asm 1: movq <stack_2=stack64#10,>y5=int64#12
# asm 2: movq <stack_2=72(%rsp),>y5=%r14
movq 72(%rsp),%r14

# qhasm: y6 = stack_3
# asm 1: movq <stack_3=stack64#11,>y6=int64#13
# asm 2: movq <stack_3=80(%rsp),>y6=%r15
movq 80(%rsp),%r15

# qhasm: stack_4 = y1
# asm 1: movq <y1=int64#8,>stack_4=stack64#1
# asm 2: movq <y1=%r10,>stack_4=0(%rsp)
movq %r10,0(%rsp)

# qhasm: stack_5 = y2
# asm 1: movq <y2=int64#9,>stack_5=stack64#10
# asm 2: movq <y2=%r11,>stack_5=72(%rsp)
movq %r11,72(%rsp)

# qhasm: stack_6 = y3
# asm 1: movq <y3=int64#10,>stack_6=stack64#11
# asm 2: movq <y3=%r12,>stack_6=80(%rsp)
movq %r12,80(%rsp)

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#8
# asm 2: mov  $0,>out_counter=%r10
mov  $0,%r10

# qhasm: 1111_outer_start:
._1111_outer_start:

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#8
# asm 2: cmp  $64,<out_counter=%r10
cmp  $64,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 1111_outer_end if =
je ._1111_outer_end

# qhasm: carry? x1 <bt> (uint32) out_counter
# asm 1: bt <out_counter=int64#8d,<x1=int64#5
# asm 2: bt <out_counter=%r10d,<x1=%r8
bt %r10d,%r8
# comment:fp stack unchanged by jump

# qhasm: goto 1111_inner_start if carry
jc ._1111_inner_start

# qhasm: offset = 159
# asm 1: mov  $159,>offset=int64#9
# asm 2: mov  $159,>offset=%r11
mov  $159,%r11

# qhasm: offset -= out_counter
# asm 1: sub  <out_counter=int64#8,<offset=int64#9
# asm 2: sub  <out_counter=%r10,<offset=%r11
sub  %r10,%r11

# qhasm: offset *= stack_56
# asm 1: imul  <stack_56=stack64#9,<offset=int64#9
# asm 2: imul  <stack_56=64(%rsp),<offset=%r11
imul  64(%rsp),%r11

# qhasm: ptr_p += offset
# asm 1: add  <offset=int64#9,<ptr_p=int64#2
# asm 2: add  <offset=%r11,<ptr_p=%rsi
add  %r11,%rsi

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 1111_outer_start
jmp ._1111_outer_start

# qhasm: 1111_inner_start:
._1111_inner_start:

# qhasm: counter = 1
# asm 1: mov  $1,>counter=int64#9
# asm 2: mov  $1,>counter=%r11
mov  $1,%r11

# qhasm: counter += out_counter
# asm 1: add  <out_counter=int64#8,<counter=int64#9
# asm 2: add  <out_counter=%r10,<counter=%r11
add  %r10,%r11

# qhasm: 1111_inner_main:
._1111_inner_main:

# qhasm: unsigned>? counter - 63
# asm 1: cmp  $63,<counter=int64#9
# asm 2: cmp  $63,<counter=%r11
cmp  $63,%r11
# comment:fp stack unchanged by jump

# qhasm: goto 1111_inner_end if unsigned>
ja ._1111_inner_end

# qhasm: carry? x1 <bt> (uint32) counter
# asm 1: bt <counter=int64#9d,<x1=int64#5
# asm 2: bt <counter=%r11d,<x1=%r8
bt %r11d,%r8
# comment:fp stack unchanged by jump

# qhasm: goto skip11 if !carry
jnc ._skip11

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r12
movzwq 0(%rsi),%r12

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y0=int64#7
# asm 2: btc <c0=%r12w,<y0=%rax
btc %r12w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#10
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r12
movzwq 2(%rsi),%r12

# qhasm: y4 <btc> (uint16) c1
# asm 1: btc <c1=int64#10w,<y4=int64#11
# asm 2: btc <c1=%r12w,<y4=%r13
btc %r12w,%r13

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#10
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r12
movzwq 4(%rsi),%r12

# qhasm: y5 <btc> (uint16) c2
# asm 1: btc <c2=int64#10w,<y5=int64#12
# asm 2: btc <c2=%r12w,<y5=%r14
btc %r12w,%r14

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r12
movzwq 6(%rsi),%r12

# qhasm: y6 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y6=int64#13
# asm 2: btc <c0=%r12w,<y6=%r15
btc %r12w,%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip11:
._skip11:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11
# comment:fp stack unchanged by jump

# qhasm: goto 1111_inner_main
jmp ._1111_inner_main

# qhasm: 1111_inner_end:
._1111_inner_end:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#9
# asm 2: mov  $0,>counter=%r11
mov  $0,%r11

# qhasm: loop_start12:
._loop_start12:

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#9d,<x2=int64#6
# asm 2: bt <counter=%r11d,<x2=%r9
bt %r11d,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip12 if !carry
jnc ._skip12

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r12
movzwq 0(%rsi),%r12

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y0=int64#7
# asm 2: btc <c0=%r12w,<y0=%rax
btc %r12w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#10
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r12
movzwq 2(%rsi),%r12

# qhasm: y4 <btc> (uint16) c1
# asm 1: btc <c1=int64#10w,<y4=int64#11
# asm 2: btc <c1=%r12w,<y4=%r13
btc %r12w,%r13

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#10
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r12
movzwq 4(%rsi),%r12

# qhasm: y5 <btc> (uint16) c2
# asm 1: btc <c2=int64#10w,<y5=int64#12
# asm 2: btc <c2=%r12w,<y5=%r14
btc %r12w,%r14

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r12
movzwq 6(%rsi),%r12

# qhasm: y6 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y6=int64#13
# asm 2: btc <c0=%r12w,<y6=%r15
btc %r12w,%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip12:
._skip12:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#9
# asm 2: cmp  $64,<counter=%r11
cmp  $64,%r11
# comment:fp stack unchanged by jump

# qhasm: goto loop_start12 if !=
jne ._loop_start12

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#9
# asm 2: mov  $0,>counter=%r11
mov  $0,%r11

# qhasm: loop_start13:
._loop_start13:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#9d,<x3=int64#3
# asm 2: bt <counter=%r11d,<x3=%rdx
bt %r11d,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip13 if !carry
jnc ._skip13

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r12
movzwq 0(%rsi),%r12

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y0=int64#7
# asm 2: btc <c0=%r12w,<y0=%rax
btc %r12w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#10
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r12
movzwq 2(%rsi),%r12

# qhasm: y4 <btc> (uint16) c1
# asm 1: btc <c1=int64#10w,<y4=int64#11
# asm 2: btc <c1=%r12w,<y4=%r13
btc %r12w,%r13

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#10
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r12
movzwq 4(%rsi),%r12

# qhasm: y5 <btc> (uint16) c2
# asm 1: btc <c2=int64#10w,<y5=int64#12
# asm 2: btc <c2=%r12w,<y5=%r14
btc %r12w,%r14

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r12
movzwq 6(%rsi),%r12

# qhasm: y6 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y6=int64#13
# asm 2: btc <c0=%r12w,<y6=%r15
btc %r12w,%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip13:
._skip13:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#9
# asm 2: cmp  $32,<counter=%r11
cmp  $32,%r11
# comment:fp stack unchanged by jump

# qhasm: goto loop_start13 if !=
jne ._loop_start13

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 1111_outer_start
jmp ._1111_outer_start

# qhasm: 1111_outer_end:
._1111_outer_end:

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#8
# asm 2: mov  $0,>out_counter=%r10
mov  $0,%r10

# qhasm: 2222_outer_start:
._2222_outer_start:

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#8
# asm 2: cmp  $64,<out_counter=%r10
cmp  $64,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 2222_outer_end if =
je ._2222_outer_end

# qhasm: carry? x2 <bt> (uint32) out_counter
# asm 1: bt <out_counter=int64#8d,<x2=int64#6
# asm 2: bt <out_counter=%r10d,<x2=%r9
bt %r10d,%r9
# comment:fp stack unchanged by jump

# qhasm: goto 2222_inner_start if carry
jc ._2222_inner_start

# qhasm: offset = 95
# asm 1: mov  $95,>offset=int64#9
# asm 2: mov  $95,>offset=%r11
mov  $95,%r11

# qhasm: offset -= out_counter
# asm 1: sub  <out_counter=int64#8,<offset=int64#9
# asm 2: sub  <out_counter=%r10,<offset=%r11
sub  %r10,%r11

# qhasm: offset *= stack_56
# asm 1: imul  <stack_56=stack64#9,<offset=int64#9
# asm 2: imul  <stack_56=64(%rsp),<offset=%r11
imul  64(%rsp),%r11

# qhasm: ptr_p += offset
# asm 1: add  <offset=int64#9,<ptr_p=int64#2
# asm 2: add  <offset=%r11,<ptr_p=%rsi
add  %r11,%rsi

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 2222_outer_start
jmp ._2222_outer_start

# qhasm: 2222_inner_start:
._2222_inner_start:

# qhasm: counter = 1
# asm 1: mov  $1,>counter=int64#9
# asm 2: mov  $1,>counter=%r11
mov  $1,%r11

# qhasm: counter += out_counter
# asm 1: add  <out_counter=int64#8,<counter=int64#9
# asm 2: add  <out_counter=%r10,<counter=%r11
add  %r10,%r11

# qhasm: 2222_inner_main:
._2222_inner_main:

# qhasm: unsigned>? counter - 63
# asm 1: cmp  $63,<counter=int64#9
# asm 2: cmp  $63,<counter=%r11
cmp  $63,%r11
# comment:fp stack unchanged by jump

# qhasm: goto 2222_inner_end if unsigned>
ja ._2222_inner_end

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#9d,<x2=int64#6
# asm 2: bt <counter=%r11d,<x2=%r9
bt %r11d,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip22 if !carry
jnc ._skip22

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r12
movzwq 0(%rsi),%r12

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y0=int64#7
# asm 2: btc <c0=%r12w,<y0=%rax
btc %r12w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#10
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r12
movzwq 2(%rsi),%r12

# qhasm: y4 <btc> (uint16) c1
# asm 1: btc <c1=int64#10w,<y4=int64#11
# asm 2: btc <c1=%r12w,<y4=%r13
btc %r12w,%r13

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#10
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r12
movzwq 4(%rsi),%r12

# qhasm: y5 <btc> (uint16) c2
# asm 1: btc <c2=int64#10w,<y5=int64#12
# asm 2: btc <c2=%r12w,<y5=%r14
btc %r12w,%r14

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r12
movzwq 6(%rsi),%r12

# qhasm: y6 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y6=int64#13
# asm 2: btc <c0=%r12w,<y6=%r15
btc %r12w,%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip22:
._skip22:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11
# comment:fp stack unchanged by jump

# qhasm: goto 2222_inner_main
jmp ._2222_inner_main

# qhasm: 2222_inner_end:
._2222_inner_end:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#9
# asm 2: mov  $0,>counter=%r11
mov  $0,%r11

# qhasm: loop_start23:
._loop_start23:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#9d,<x3=int64#3
# asm 2: bt <counter=%r11d,<x3=%rdx
bt %r11d,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip23 if !carry
jnc ._skip23

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r12
movzwq 0(%rsi),%r12

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y0=int64#7
# asm 2: btc <c0=%r12w,<y0=%rax
btc %r12w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#10
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r12
movzwq 2(%rsi),%r12

# qhasm: y4 <btc> (uint16) c1
# asm 1: btc <c1=int64#10w,<y4=int64#11
# asm 2: btc <c1=%r12w,<y4=%r13
btc %r12w,%r13

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#10
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r12
movzwq 4(%rsi),%r12

# qhasm: y5 <btc> (uint16) c2
# asm 1: btc <c2=int64#10w,<y5=int64#12
# asm 2: btc <c2=%r12w,<y5=%r14
btc %r12w,%r14

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r12
movzwq 6(%rsi),%r12

# qhasm: y6 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y6=int64#13
# asm 2: btc <c0=%r12w,<y6=%r15
btc %r12w,%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip23:
._skip23:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#9
# asm 2: cmp  $32,<counter=%r11
cmp  $32,%r11
# comment:fp stack unchanged by jump

# qhasm: goto loop_start23 if !=
jne ._loop_start23

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 2222_outer_start
jmp ._2222_outer_start

# qhasm: 2222_outer_end:
._2222_outer_end:

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#8
# asm 2: mov  $0,>out_counter=%r10
mov  $0,%r10

# qhasm: 3333_outer_start:
._3333_outer_start:

# qhasm: =? out_counter - 32
# asm 1: cmp  $32,<out_counter=int64#8
# asm 2: cmp  $32,<out_counter=%r10
cmp  $32,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 3333_outer_end if =
je ._3333_outer_end

# qhasm: carry? x3 <bt> (uint32) out_counter
# asm 1: bt <out_counter=int64#8d,<x3=int64#3
# asm 2: bt <out_counter=%r10d,<x3=%rdx
bt %r10d,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto 3333_inner_start if carry
jc ._3333_inner_start

# qhasm: offset = 31
# asm 1: mov  $31,>offset=int64#9
# asm 2: mov  $31,>offset=%r11
mov  $31,%r11

# qhasm: offset -= out_counter
# asm 1: sub  <out_counter=int64#8,<offset=int64#9
# asm 2: sub  <out_counter=%r10,<offset=%r11
sub  %r10,%r11

# qhasm: offset *= stack_56
# asm 1: imul  <stack_56=stack64#9,<offset=int64#9
# asm 2: imul  <stack_56=64(%rsp),<offset=%r11
imul  64(%rsp),%r11

# qhasm: ptr_p += offset
# asm 1: add  <offset=int64#9,<ptr_p=int64#2
# asm 2: add  <offset=%r11,<ptr_p=%rsi
add  %r11,%rsi

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 3333_outer_start
jmp ._3333_outer_start

# qhasm: 3333_inner_start:
._3333_inner_start:

# qhasm: counter = 1
# asm 1: mov  $1,>counter=int64#9
# asm 2: mov  $1,>counter=%r11
mov  $1,%r11

# qhasm: counter += out_counter
# asm 1: add  <out_counter=int64#8,<counter=int64#9
# asm 2: add  <out_counter=%r10,<counter=%r11
add  %r10,%r11

# qhasm: 3333_inner_main:
._3333_inner_main:

# qhasm: unsigned>? counter - 31
# asm 1: cmp  $31,<counter=int64#9
# asm 2: cmp  $31,<counter=%r11
cmp  $31,%r11
# comment:fp stack unchanged by jump

# qhasm: goto 3333_inner_end if unsigned>
ja ._3333_inner_end

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#9d,<x3=int64#3
# asm 2: bt <counter=%r11d,<x3=%rdx
bt %r11d,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip33 if !carry
jnc ._skip33

# qhasm: c0 = *(uint16 *)(ptr_p + 0)
# asm 1: movzwq 0(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 0(<ptr_p=%rsi),>c0=%r12
movzwq 0(%rsi),%r12

# qhasm: y0 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y0=int64#7
# asm 2: btc <c0=%r12w,<y0=%rax
btc %r12w,%rax

# qhasm: c1 = *(uint16 *)(ptr_p + 2 )
# asm 1: movzwq 2(<ptr_p=int64#2),>c1=int64#10
# asm 2: movzwq 2(<ptr_p=%rsi),>c1=%r12
movzwq 2(%rsi),%r12

# qhasm: y4 <btc> (uint16) c1
# asm 1: btc <c1=int64#10w,<y4=int64#11
# asm 2: btc <c1=%r12w,<y4=%r13
btc %r12w,%r13

# qhasm: c2 = *(uint16 *)(ptr_p + 4 )
# asm 1: movzwq 4(<ptr_p=int64#2),>c2=int64#10
# asm 2: movzwq 4(<ptr_p=%rsi),>c2=%r12
movzwq 4(%rsi),%r12

# qhasm: y5 <btc> (uint16) c2
# asm 1: btc <c2=int64#10w,<y5=int64#12
# asm 2: btc <c2=%r12w,<y5=%r14
btc %r12w,%r14

# qhasm: c0 = *(uint16 *)(ptr_p + 6 )
# asm 1: movzwq 6(<ptr_p=int64#2),>c0=int64#10
# asm 2: movzwq 6(<ptr_p=%rsi),>c0=%r12
movzwq 6(%rsi),%r12

# qhasm: y6 <btc> (uint16) c0
# asm 1: btc <c0=int64#10w,<y6=int64#13
# asm 2: btc <c0=%r12w,<y6=%r15
btc %r12w,%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip33:
._skip33:

# qhasm: ptr_p += 8
# asm 1: add  $8,<ptr_p=int64#2
# asm 2: add  $8,<ptr_p=%rsi
add  $8,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11
# comment:fp stack unchanged by jump

# qhasm: goto 3333_inner_main
jmp ._3333_inner_main

# qhasm: 3333_inner_end:
._3333_inner_end:

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10
# comment:fp stack unchanged by jump

# qhasm: goto 3333_outer_start
jmp ._3333_outer_start

# qhasm: 3333_outer_end:
._3333_outer_end:

# qhasm: y1 = stack_4
# asm 1: movq <stack_4=stack64#1,>y1=int64#3
# asm 2: movq <stack_4=0(%rsp),>y1=%rdx
movq 0(%rsp),%rdx

# qhasm: y2 = stack_5
# asm 1: movq <stack_5=stack64#10,>y2=int64#8
# asm 2: movq <stack_5=72(%rsp),>y2=%r10
movq 72(%rsp),%r10

# qhasm: y3 = stack_6
# asm 1: movq <stack_6=stack64#11,>y3=int64#9
# asm 2: movq <stack_6=80(%rsp),>y3=%r11
movq 80(%rsp),%r11

# qhasm: stack_6 = y6
# asm 1: movq <y6=int64#13,>stack_6=stack64#1
# asm 2: movq <y6=%r15,>stack_6=0(%rsp)
movq %r15,0(%rsp)

# qhasm: stack_5 = y5
# asm 1: movq <y5=int64#12,>stack_5=stack64#9
# asm 2: movq <y5=%r14,>stack_5=64(%rsp)
movq %r14,64(%rsp)

# qhasm: c0 = 0
# asm 1: mov  $0,>c0=int64#10
# asm 2: mov  $0,>c0=%r12
mov  $0,%r12

# qhasm: c1 = 0
# asm 1: mov  $0,>c1=int64#12
# asm 2: mov  $0,>c1=%r14
mov  $0,%r14

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#13
# asm 2: mov  $0,>out_counter=%r15
mov  $0,%r15

# qhasm: cubic_y0_start:
._cubic_y0_start:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#10
# asm 2: mov  $0,>counter=%r12
mov  $0,%r12

# qhasm: cubic_y0_inner:
._cubic_y0_inner:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#14
# asm 2: mov  $0,>c2=%rbx
mov  $0,%rbx

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#13d,<c2=int64#14
# asm 2: btc <out_counter=%r15d,<c2=%rbx
btc %r15d,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%rbp
movzbq 0(%rsi),%rbp

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x0=int64#4
# asm 2: bt <c0=%ebp,<x0=%rcx
bt %ebp,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%rbp
movzbq 1(%rsi),%rbp

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x1=int64#5
# asm 2: bt <c0=%ebp,<x1=%r8
bt %ebp,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%rbp
movzbq 2(%rsi),%rbp

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x2=int64#6
# asm 2: bt <c0=%ebp,<x2=%r9
bt %ebp,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y0 ^= c2
# asm 1: xor  <c2=int64#14,<y0=int64#7
# asm 2: xor  <c2=%rbx,<y0=%rax
xor  %rbx,%rax

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#10
# asm 2: add  $1,<counter=%r12
add  $1,%r12

# qhasm: =? counter - 20
# asm 1: cmp  $20,<counter=int64#10
# asm 2: cmp  $20,<counter=%r12
cmp  $20,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y0_inner if !=
jne ._cubic_y0_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#13
# asm 2: add  $1,<out_counter=%r15
add  $1,%r15

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#13
# asm 2: cmp  $64,<out_counter=%r15
cmp  $64,%r15
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y0_start if !=
jne ._cubic_y0_start

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#10
# asm 2: mov  $0,>out_counter=%r12
mov  $0,%r12

# qhasm: cubic_y1_start:
._cubic_y1_start:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#13
# asm 2: mov  $0,>counter=%r15
mov  $0,%r15

# qhasm: cubic_y1_inner:
._cubic_y1_inner:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#14
# asm 2: mov  $0,>c2=%rbx
mov  $0,%rbx

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#10d,<c2=int64#14
# asm 2: btc <out_counter=%r12d,<c2=%rbx
btc %r12d,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%rbp
movzbq 0(%rsi),%rbp

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x0=int64#4
# asm 2: bt <c0=%ebp,<x0=%rcx
bt %ebp,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%rbp
movzbq 1(%rsi),%rbp

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x1=int64#5
# asm 2: bt <c0=%ebp,<x1=%r8
bt %ebp,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%rbp
movzbq 2(%rsi),%rbp

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x2=int64#6
# asm 2: bt <c0=%ebp,<x2=%r9
bt %ebp,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y1 ^= c2
# asm 1: xor  <c2=int64#14,<y1=int64#3
# asm 2: xor  <c2=%rbx,<y1=%rdx
xor  %rbx,%rdx

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#13
# asm 2: add  $1,<counter=%r15
add  $1,%r15

# qhasm: =? counter - 20
# asm 1: cmp  $20,<counter=int64#13
# asm 2: cmp  $20,<counter=%r15
cmp  $20,%r15
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y1_inner if !=
jne ._cubic_y1_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#10
# asm 2: add  $1,<out_counter=%r12
add  $1,%r12

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#10
# asm 2: cmp  $64,<out_counter=%r12
cmp  $64,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y1_start if !=
jne ._cubic_y1_start

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#10
# asm 2: mov  $0,>out_counter=%r12
mov  $0,%r12

# qhasm: cubic_y2_start:
._cubic_y2_start:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#13
# asm 2: mov  $0,>counter=%r15
mov  $0,%r15

# qhasm: cubic_y2_inner:
._cubic_y2_inner:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#14
# asm 2: mov  $0,>c2=%rbx
mov  $0,%rbx

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#10d,<c2=int64#14
# asm 2: btc <out_counter=%r12d,<c2=%rbx
btc %r12d,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%rbp
movzbq 0(%rsi),%rbp

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x0=int64#4
# asm 2: bt <c0=%ebp,<x0=%rcx
bt %ebp,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%rbp
movzbq 1(%rsi),%rbp

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x1=int64#5
# asm 2: bt <c0=%ebp,<x1=%r8
bt %ebp,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%rbp
movzbq 2(%rsi),%rbp

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x2=int64#6
# asm 2: bt <c0=%ebp,<x2=%r9
bt %ebp,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y2 ^= c2
# asm 1: xor  <c2=int64#14,<y2=int64#8
# asm 2: xor  <c2=%rbx,<y2=%r10
xor  %rbx,%r10

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#13
# asm 2: add  $1,<counter=%r15
add  $1,%r15

# qhasm: =? counter - 20
# asm 1: cmp  $20,<counter=int64#13
# asm 2: cmp  $20,<counter=%r15
cmp  $20,%r15
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y2_inner if !=
jne ._cubic_y2_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#10
# asm 2: add  $1,<out_counter=%r12
add  $1,%r12

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#10
# asm 2: cmp  $64,<out_counter=%r12
cmp  $64,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y2_start if !=
jne ._cubic_y2_start

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#10
# asm 2: mov  $0,>out_counter=%r12
mov  $0,%r12

# qhasm: cubic_y3_start:
._cubic_y3_start:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#13
# asm 2: mov  $0,>counter=%r15
mov  $0,%r15

# qhasm: cubic_y3_inner:
._cubic_y3_inner:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#14
# asm 2: mov  $0,>c2=%rbx
mov  $0,%rbx

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#10d,<c2=int64#14
# asm 2: btc <out_counter=%r12d,<c2=%rbx
btc %r12d,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%rbp
movzbq 0(%rsi),%rbp

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x0=int64#4
# asm 2: bt <c0=%ebp,<x0=%rcx
bt %ebp,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%rbp
movzbq 1(%rsi),%rbp

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x1=int64#5
# asm 2: bt <c0=%ebp,<x1=%r8
bt %ebp,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#15
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%rbp
movzbq 2(%rsi),%rbp

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#15d,<x2=int64#6
# asm 2: bt <c0=%ebp,<x2=%r9
bt %ebp,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#14
# asm 2: cmovnc <c1=%r14,<c2=%rbx
cmovnc %r14,%rbx

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y3 ^= c2
# asm 1: xor  <c2=int64#14,<y3=int64#9
# asm 2: xor  <c2=%rbx,<y3=%r11
xor  %rbx,%r11

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#13
# asm 2: add  $1,<counter=%r15
add  $1,%r15

# qhasm: =? counter - 20
# asm 1: cmp  $20,<counter=int64#13
# asm 2: cmp  $20,<counter=%r15
cmp  $20,%r15
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y3_inner if !=
jne ._cubic_y3_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#10
# asm 2: add  $1,<out_counter=%r12
add  $1,%r12

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#10
# asm 2: cmp  $64,<out_counter=%r12
cmp  $64,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y3_start if !=
jne ._cubic_y3_start

# qhasm: *(int64 *)(ptr_y + 0) = y0
# asm 1: movq   <y0=int64#7,0(<ptr_y=int64#1)
# asm 2: movq   <y0=%rax,0(<ptr_y=%rdi)
movq   %rax,0(%rdi)

# qhasm: *(int64 *)(ptr_y + 8) = y1
# asm 1: movq   <y1=int64#3,8(<ptr_y=int64#1)
# asm 2: movq   <y1=%rdx,8(<ptr_y=%rdi)
movq   %rdx,8(%rdi)

# qhasm: *(int64 *)(ptr_y + 16) = y2
# asm 1: movq   <y2=int64#8,16(<ptr_y=int64#1)
# asm 2: movq   <y2=%r10,16(<ptr_y=%rdi)
movq   %r10,16(%rdi)

# qhasm: *(int64 *)(ptr_y + 24) = y3
# asm 1: movq   <y3=int64#9,24(<ptr_y=int64#1)
# asm 2: movq   <y3=%r11,24(<ptr_y=%rdi)
movq   %r11,24(%rdi)

# qhasm: y5 = stack_5
# asm 1: movq <stack_5=stack64#9,>y5=int64#3
# asm 2: movq <stack_5=64(%rsp),>y5=%rdx
movq 64(%rsp),%rdx

# qhasm: y6 = stack_6
# asm 1: movq <stack_6=stack64#1,>y6=int64#7
# asm 2: movq <stack_6=0(%rsp),>y6=%rax
movq 0(%rsp),%rax

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#8
# asm 2: mov  $0,>out_counter=%r10
mov  $0,%r10

# qhasm: cubic_y4_start:
._cubic_y4_start:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#9
# asm 2: mov  $0,>counter=%r11
mov  $0,%r11

# qhasm: cubic_y4_inner:
._cubic_y4_inner:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#10
# asm 2: mov  $0,>c2=%r12
mov  $0,%r12

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#8d,<c2=int64#10
# asm 2: btc <out_counter=%r10d,<c2=%r12
btc %r10d,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r15
movzbq 0(%rsi),%r15

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x0=int64#4
# asm 2: bt <c0=%r15d,<x0=%rcx
bt %r15d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r15
movzbq 1(%rsi),%r15

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x1=int64#5
# asm 2: bt <c0=%r15d,<x1=%r8
bt %r15d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r15
movzbq 2(%rsi),%r15

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x2=int64#6
# asm 2: bt <c0=%r15d,<x2=%r9
bt %r15d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y4 ^= c2
# asm 1: xor  <c2=int64#10,<y4=int64#11
# asm 2: xor  <c2=%r12,<y4=%r13
xor  %r12,%r13

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11

# qhasm: =? counter - 20
# asm 1: cmp  $20,<counter=int64#9
# asm 2: cmp  $20,<counter=%r11
cmp  $20,%r11
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y4_inner if !=
jne ._cubic_y4_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#8
# asm 2: cmp  $64,<out_counter=%r10
cmp  $64,%r10
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y4_start if !=
jne ._cubic_y4_start

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#8
# asm 2: mov  $0,>out_counter=%r10
mov  $0,%r10

# qhasm: cubic_y5_start:
._cubic_y5_start:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#9
# asm 2: mov  $0,>counter=%r11
mov  $0,%r11

# qhasm: cubic_y5_inner:
._cubic_y5_inner:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#10
# asm 2: mov  $0,>c2=%r12
mov  $0,%r12

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#8d,<c2=int64#10
# asm 2: btc <out_counter=%r10d,<c2=%r12
btc %r10d,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r15
movzbq 0(%rsi),%r15

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x0=int64#4
# asm 2: bt <c0=%r15d,<x0=%rcx
bt %r15d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r15
movzbq 1(%rsi),%r15

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x1=int64#5
# asm 2: bt <c0=%r15d,<x1=%r8
bt %r15d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r15
movzbq 2(%rsi),%r15

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x2=int64#6
# asm 2: bt <c0=%r15d,<x2=%r9
bt %r15d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y5 ^= c2
# asm 1: xor  <c2=int64#10,<y5=int64#3
# asm 2: xor  <c2=%r12,<y5=%rdx
xor  %r12,%rdx

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11

# qhasm: =? counter - 20
# asm 1: cmp  $20,<counter=int64#9
# asm 2: cmp  $20,<counter=%r11
cmp  $20,%r11
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y5_inner if !=
jne ._cubic_y5_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#8
# asm 2: cmp  $64,<out_counter=%r10
cmp  $64,%r10
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y5_start if !=
jne ._cubic_y5_start

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#8
# asm 2: mov  $0,>out_counter=%r10
mov  $0,%r10

# qhasm: cubic_y6_start:
._cubic_y6_start:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#9
# asm 2: mov  $0,>counter=%r11
mov  $0,%r11

# qhasm: cubic_y6_inner:
._cubic_y6_inner:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#10
# asm 2: mov  $0,>c2=%r12
mov  $0,%r12

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#8d,<c2=int64#10
# asm 2: btc <out_counter=%r10d,<c2=%r12
btc %r10d,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r15
movzbq 0(%rsi),%r15

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x0=int64#4
# asm 2: bt <c0=%r15d,<x0=%rcx
bt %r15d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r15
movzbq 1(%rsi),%r15

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x1=int64#5
# asm 2: bt <c0=%r15d,<x1=%r8
bt %r15d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#13
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r15
movzbq 2(%rsi),%r15

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#13d,<x2=int64#6
# asm 2: bt <c0=%r15d,<x2=%r9
bt %r15d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#12,<c2=int64#10
# asm 2: cmovnc <c1=%r14,<c2=%r12
cmovnc %r14,%r12

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y6 ^= c2
# asm 1: xor  <c2=int64#10,<y6=int64#7
# asm 2: xor  <c2=%r12,<y6=%rax
xor  %r12,%rax

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11

# qhasm: =? counter - 20
# asm 1: cmp  $20,<counter=int64#9
# asm 2: cmp  $20,<counter=%r11
cmp  $20,%r11
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y6_inner if !=
jne ._cubic_y6_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#8
# asm 2: add  $1,<out_counter=%r10
add  $1,%r10

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#8
# asm 2: cmp  $64,<out_counter=%r10
cmp  $64,%r10
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y6_start if !=
jne ._cubic_y6_start

# qhasm: *(int64 *)(ptr_y + 32) = y4
# asm 1: movq   <y4=int64#11,32(<ptr_y=int64#1)
# asm 2: movq   <y4=%r13,32(<ptr_y=%rdi)
movq   %r13,32(%rdi)

# qhasm: *(int64 *)(ptr_y + 40) = y5
# asm 1: movq   <y5=int64#3,40(<ptr_y=int64#1)
# asm 2: movq   <y5=%rdx,40(<ptr_y=%rdi)
movq   %rdx,40(%rdi)

# qhasm: *(int64 *)(ptr_y + 48) = y6
# asm 1: movq   <y6=int64#7,48(<ptr_y=int64#1)
# asm 2: movq   <y6=%rax,48(<ptr_y=%rdi)
movq   %rax,48(%rdi)

# qhasm: r11 = r11_stack
# asm 1: movq <r11_stack=stack64#2,>r11=int64#9
# asm 2: movq <r11_stack=8(%rsp),>r11=%r11
movq 8(%rsp),%r11

# qhasm: r12 = r12_stack
# asm 1: movq <r12_stack=stack64#3,>r12=int64#10
# asm 2: movq <r12_stack=16(%rsp),>r12=%r12
movq 16(%rsp),%r12

# qhasm: r13 = r13_stack
# asm 1: movq <r13_stack=stack64#4,>r13=int64#11
# asm 2: movq <r13_stack=24(%rsp),>r13=%r13
movq 24(%rsp),%r13

# qhasm: r14 = r14_stack
# asm 1: movq <r14_stack=stack64#5,>r14=int64#12
# asm 2: movq <r14_stack=32(%rsp),>r14=%r14
movq 32(%rsp),%r14

# qhasm: r15 = r15_stack
# asm 1: movq <r15_stack=stack64#6,>r15=int64#13
# asm 2: movq <r15_stack=40(%rsp),>r15=%r15
movq 40(%rsp),%r15

# qhasm: rbx = rbx_stack
# asm 1: movq <rbx_stack=stack64#7,>rbx=int64#14
# asm 2: movq <rbx_stack=48(%rsp),>rbx=%rbx
movq 48(%rsp),%rbx

# qhasm: rbp = rbp_stack
# asm 1: movq <rbp_stack=stack64#8,>rbp=int64#15
# asm 2: movq <rbp_stack=56(%rsp),>rbp=%rbp
movq 56(%rsp),%rbp

# qhasm: leave
add %r11,%rsp
mov %rdi,%rax
mov %rsi,%rdx
ret
