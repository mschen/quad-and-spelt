
# qhasm: int64 r11

# qhasm: int64 r12

# qhasm: int64 r13

# qhasm: int64 r14

# qhasm: int64 r15

# qhasm: int64 rbx

# qhasm: int64 rbp

# qhasm: caller r11

# qhasm: caller r12

# qhasm: caller r13

# qhasm: caller r14

# qhasm: caller r15

# qhasm: caller rbx

# qhasm: caller rbp

# qhasm: stack64 r11_stack

# qhasm: stack64 r12_stack

# qhasm: stack64 r13_stack

# qhasm: stack64 r14_stack

# qhasm: stack64 r15_stack

# qhasm: stack64 rbx_stack

# qhasm: stack64 rbp_stack

# qhasm: int64 ptr_y

# qhasm: int64 ptr_p

# qhasm: int64 ptr_x

# qhasm: input ptr_y

# qhasm: input ptr_p

# qhasm: input ptr_x

# qhasm: int64 ptr_n

# qhasm: int64 y0

# qhasm: int64 y1

# qhasm: int64 y2

# qhasm: int64 y3

# qhasm: int64 y4

# qhasm: int64 y5

# qhasm: int64 y6

# qhasm: int64 x0

# qhasm: int64 x1

# qhasm: int64 x2

# qhasm: int64 x3

# qhasm: int64 counter

# qhasm: int64 out_counter

# qhasm: int64 offset

# qhasm: stack64 stack_56

# qhasm: stack64 stack_63

# qhasm: stack64 stack_1

# qhasm: stack64 stack_2

# qhasm: stack64 stack_3

# qhasm: stack64 stack_4

# qhasm: stack64 stack_5

# qhasm: stack64 stack_6

# qhasm: int64 c0

# qhasm: int64 c1

# qhasm: int64 c2

# qhasm: int64 c3

# qhasm: int64 q0

# qhasm: int64 q1

# qhasm: int64 q2

# qhasm: int64 zero

# qhasm: enter eval_squad_asm2
.text
.p2align 5
.globl _eval_squad_asm2
.globl eval_squad_asm2
_eval_squad_asm2:
eval_squad_asm2:
mov %rsp,%r11
and $31,%r11
add $128,%r11
sub %r11,%rsp

# qhasm: r11_stack = r11
# asm 1: movq <r11=int64#9,>r11_stack=stack64#1
# asm 2: movq <r11=%r11,>r11_stack=0(%rsp)
movq %r11,0(%rsp)

# qhasm: r12_stack = r12
# asm 1: movq <r12=int64#10,>r12_stack=stack64#2
# asm 2: movq <r12=%r12,>r12_stack=8(%rsp)
movq %r12,8(%rsp)

# qhasm: r13_stack = r13
# asm 1: movq <r13=int64#11,>r13_stack=stack64#3
# asm 2: movq <r13=%r13,>r13_stack=16(%rsp)
movq %r13,16(%rsp)

# qhasm: r14_stack = r14
# asm 1: movq <r14=int64#12,>r14_stack=stack64#4
# asm 2: movq <r14=%r14,>r14_stack=24(%rsp)
movq %r14,24(%rsp)

# qhasm: r15_stack = r15
# asm 1: movq <r15=int64#13,>r15_stack=stack64#5
# asm 2: movq <r15=%r15,>r15_stack=32(%rsp)
movq %r15,32(%rsp)

# qhasm: rbx_stack = rbx
# asm 1: movq <rbx=int64#14,>rbx_stack=stack64#6
# asm 2: movq <rbx=%rbx,>rbx_stack=40(%rsp)
movq %rbx,40(%rsp)

# qhasm: rbp_stack = rbp
# asm 1: movq <rbp=int64#15,>rbp_stack=stack64#7
# asm 2: movq <rbp=%rbp,>rbp_stack=48(%rsp)
movq %rbp,48(%rsp)

# qhasm: offset = 8
# asm 1: mov  $8,>offset=int64#4
# asm 2: mov  $8,>offset=%rcx
mov  $8,%rcx

# qhasm: stack_56 = offset
# asm 1: movq <offset=int64#4,>stack_56=stack64#8
# asm 2: movq <offset=%rcx,>stack_56=56(%rsp)
movq %rcx,56(%rsp)

# qhasm: x0 = *(int64 *)(ptr_x + 0)
# asm 1: movq   0(<ptr_x=int64#3),>x0=int64#4
# asm 2: movq   0(<ptr_x=%rdx),>x0=%rcx
movq   0(%rdx),%rcx

# qhasm: x1 = *(int64 *)(ptr_x + 8)
# asm 1: movq   8(<ptr_x=int64#3),>x1=int64#5
# asm 2: movq   8(<ptr_x=%rdx),>x1=%r8
movq   8(%rdx),%r8

# qhasm: x2 = *(int64 *)(ptr_x + 16)
# asm 1: movq   16(<ptr_x=int64#3),>x2=int64#6
# asm 2: movq   16(<ptr_x=%rdx),>x2=%r9
movq   16(%rdx),%r9

# qhasm: x3 = *(int64 *)(ptr_x + 24)
# asm 1: movq   24(<ptr_x=int64#3),>x3=int64#3
# asm 2: movq   24(<ptr_x=%rdx),>x3=%rdx
movq   24(%rdx),%rdx

# qhasm: y0 = *(int64 *)(ptr_p + 0)
# asm 1: movq   0(<ptr_p=int64#2),>y0=int64#7
# asm 2: movq   0(<ptr_p=%rsi),>y0=%rax
movq   0(%rsi),%rax

# qhasm: y1 = *(int64 *)(ptr_p + 8)
# asm 1: movq   8(<ptr_p=int64#2),>y1=int64#8
# asm 2: movq   8(<ptr_p=%rsi),>y1=%r10
movq   8(%rsi),%r10

# qhasm: y2 = *(int64 *)(ptr_p + 16)
# asm 1: movq   16(<ptr_p=int64#2),>y2=int64#9
# asm 2: movq   16(<ptr_p=%rsi),>y2=%r11
movq   16(%rsi),%r11

# qhasm: y3 = *(int64 *)(ptr_p + 24)
# asm 1: movq   24(<ptr_p=int64#2),>y3=int64#10
# asm 2: movq   24(<ptr_p=%rsi),>y3=%r12
movq   24(%rsi),%r12

# qhasm: y4 = *(int64 *)(ptr_p + 32)
# asm 1: movq   32(<ptr_p=int64#2),>y4=int64#11
# asm 2: movq   32(<ptr_p=%rsi),>y4=%r13
movq   32(%rsi),%r13

# qhasm: y5 = *(int64 *)(ptr_p + 40)
# asm 1: movq   40(<ptr_p=int64#2),>y5=int64#12
# asm 2: movq   40(<ptr_p=%rsi),>y5=%r14
movq   40(%rsi),%r14

# qhasm: y6 = *(int64 *)(ptr_p + 48)
# asm 1: movq   48(<ptr_p=int64#2),>y6=int64#13
# asm 2: movq   48(<ptr_p=%rsi),>y6=%r15
movq   48(%rsi),%r15

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start0:
._loop_start0:

# qhasm: carry? x0 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x0=int64#4
# asm 2: bt <counter=%ebx,<x0=%rcx
bt %ebx,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto skip0 if !carry
jnc ._skip0

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip0:
._skip0:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start0 if !=
jne ._loop_start0

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start1:
._loop_start1:

# qhasm: carry? x1 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x1=int64#5
# asm 2: bt <counter=%ebx,<x1=%r8
bt %ebx,%r8
# comment:fp stack unchanged by jump

# qhasm: goto skip1 if !carry
jnc ._skip1

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip1:
._skip1:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start1 if !=
jne ._loop_start1

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start2:
._loop_start2:

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x2=int64#6
# asm 2: bt <counter=%ebx,<x2=%r9
bt %ebx,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip2 if !carry
jnc ._skip2

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip2:
._skip2:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start2 if !=
jne ._loop_start2

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start3:
._loop_start3:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x3=int64#3
# asm 2: bt <counter=%ebx,<x3=%rdx
bt %ebx,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip3 if !carry
jnc ._skip3

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip3:
._skip3:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#14
# asm 2: cmp  $32,<counter=%rbx
cmp  $32,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start3 if !=
jne ._loop_start3

# qhasm: stack_1 = y1
# asm 1: movq <y1=int64#8,>stack_1=stack64#8
# asm 2: movq <y1=%r10,>stack_1=56(%rsp)
movq %r10,56(%rsp)

# qhasm: stack_2 = y2
# asm 1: movq <y2=int64#9,>stack_2=stack64#9
# asm 2: movq <y2=%r11,>stack_2=64(%rsp)
movq %r11,64(%rsp)

# qhasm: stack_3 = y3
# asm 1: movq <y3=int64#10,>stack_3=stack64#10
# asm 2: movq <y3=%r12,>stack_3=72(%rsp)
movq %r12,72(%rsp)

# qhasm: stack_4 = y4
# asm 1: movq <y4=int64#11,>stack_4=stack64#11
# asm 2: movq <y4=%r13,>stack_4=80(%rsp)
movq %r13,80(%rsp)

# qhasm: stack_5 = y5
# asm 1: movq <y5=int64#12,>stack_5=stack64#12
# asm 2: movq <y5=%r14,>stack_5=88(%rsp)
movq %r14,88(%rsp)

# qhasm: stack_6 = y6
# asm 1: movq <y6=int64#13,>stack_6=stack64#13
# asm 2: movq <y6=%r15,>stack_6=96(%rsp)
movq %r15,96(%rsp)

# qhasm: c0 = 0
# asm 1: mov  $0,>c0=int64#3
# asm 2: mov  $0,>c0=%rdx
mov  $0,%rdx

# qhasm: zero = 0
# asm 1: mov  $0,>zero=int64#8
# asm 2: mov  $0,>zero=%r10
mov  $0,%r10

# qhasm: q0 = 0
# asm 1: mov  $0,>q0=int64#9
# asm 2: mov  $0,>q0=%r11
mov  $0,%r11

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#10
# asm 2: mov  $0,>out_counter=%r12
mov  $0,%r12

# qhasm: cubic_y0_start:
._cubic_y0_start:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#3
# asm 2: mov  $0,>c2=%rdx
mov  $0,%rdx

# qhasm: q2 = 0
# asm 1: mov  $0,>q2=int64#8
# asm 2: mov  $0,>q2=%r10
mov  $0,%r10

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#9
# asm 2: mov  $0,>counter=%r11
mov  $0,%r11

# qhasm: cubic_y0_inner:
._cubic_y0_inner:

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#10d,<c2=int64#3
# asm 2: btc <out_counter=%r12d,<c2=%rdx
btc %r12d,%rdx

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r13
movzbq 0(%rsi),%r13

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x0=int64#4
# asm 2: bt <c0=%r13d,<x0=%rcx
bt %r13d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#8,<c2=int64#3
# asm 2: cmovnc <c1=%r10,<c2=%rdx
cmovnc %r10,%rdx

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r13
movzbq 1(%rsi),%r13

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x1=int64#5
# asm 2: bt <c0=%r13d,<x1=%r8
bt %r13d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#8,<c2=int64#3
# asm 2: cmovnc <c1=%r10,<c2=%rdx
cmovnc %r10,%rdx

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r13
movzbq 2(%rsi),%r13

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x2=int64#6
# asm 2: bt <c0=%r13d,<x2=%r9
bt %r13d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#8,<c2=int64#3
# asm 2: cmovnc <c1=%r10,<c2=%rdx
cmovnc %r10,%rdx

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y0 ^= c2
# asm 1: xor  <c2=int64#3,<y0=int64#7
# asm 2: xor  <c2=%rdx,<y0=%rax
xor  %rdx,%rax

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11

# qhasm: =? counter - 40
# asm 1: cmp  $40,<counter=int64#9
# asm 2: cmp  $40,<counter=%r11
cmp  $40,%r11
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y0_inner if !=
jne ._cubic_y0_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#10
# asm 2: add  $1,<out_counter=%r12
add  $1,%r12

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#10
# asm 2: cmp  $64,<out_counter=%r12
cmp  $64,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y0_start if !=
jne ._cubic_y0_start

# qhasm: *(int64 *)(ptr_y + 0) = y0
# asm 1: movq   <y0=int64#7,0(<ptr_y=int64#1)
# asm 2: movq   <y0=%rax,0(<ptr_y=%rdi)
movq   %rax,0(%rdi)

# qhasm: y1 = stack_1
# asm 1: movq <stack_1=stack64#8,>y1=int64#3
# asm 2: movq <stack_1=56(%rsp),>y1=%rdx
movq 56(%rsp),%rdx

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#7
# asm 2: mov  $0,>out_counter=%rax
mov  $0,%rax

# qhasm: cubic_y1_start:
._cubic_y1_start:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#8
# asm 2: mov  $0,>c2=%r10
mov  $0,%r10

# qhasm: q2 = 0
# asm 1: mov  $0,>q2=int64#9
# asm 2: mov  $0,>q2=%r11
mov  $0,%r11

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#10
# asm 2: mov  $0,>counter=%r12
mov  $0,%r12

# qhasm: cubic_y1_inner:
._cubic_y1_inner:

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#7d,<c2=int64#8
# asm 2: btc <out_counter=%eax,<c2=%r10
btc %eax,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r13
movzbq 0(%rsi),%r13

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x0=int64#4
# asm 2: bt <c0=%r13d,<x0=%rcx
bt %r13d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r13
movzbq 1(%rsi),%r13

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x1=int64#5
# asm 2: bt <c0=%r13d,<x1=%r8
bt %r13d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r13
movzbq 2(%rsi),%r13

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x2=int64#6
# asm 2: bt <c0=%r13d,<x2=%r9
bt %r13d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y1 ^= c2
# asm 1: xor  <c2=int64#8,<y1=int64#3
# asm 2: xor  <c2=%r10,<y1=%rdx
xor  %r10,%rdx

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#10
# asm 2: add  $1,<counter=%r12
add  $1,%r12

# qhasm: =? counter - 40
# asm 1: cmp  $40,<counter=int64#10
# asm 2: cmp  $40,<counter=%r12
cmp  $40,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y1_inner if !=
jne ._cubic_y1_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#7
# asm 2: add  $1,<out_counter=%rax
add  $1,%rax

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#7
# asm 2: cmp  $64,<out_counter=%rax
cmp  $64,%rax
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y1_start if !=
jne ._cubic_y1_start

# qhasm: *(int64 *)(ptr_y + 8) = y1
# asm 1: movq   <y1=int64#3,8(<ptr_y=int64#1)
# asm 2: movq   <y1=%rdx,8(<ptr_y=%rdi)
movq   %rdx,8(%rdi)

# qhasm: y2 = stack_2
# asm 1: movq <stack_2=stack64#9,>y2=int64#3
# asm 2: movq <stack_2=64(%rsp),>y2=%rdx
movq 64(%rsp),%rdx

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#7
# asm 2: mov  $0,>out_counter=%rax
mov  $0,%rax

# qhasm: cubic_y2_start:
._cubic_y2_start:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#8
# asm 2: mov  $0,>c2=%r10
mov  $0,%r10

# qhasm: q2 = 0
# asm 1: mov  $0,>q2=int64#9
# asm 2: mov  $0,>q2=%r11
mov  $0,%r11

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#10
# asm 2: mov  $0,>counter=%r12
mov  $0,%r12

# qhasm: cubic_y2_inner:
._cubic_y2_inner:

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#7d,<c2=int64#8
# asm 2: btc <out_counter=%eax,<c2=%r10
btc %eax,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r13
movzbq 0(%rsi),%r13

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x0=int64#4
# asm 2: bt <c0=%r13d,<x0=%rcx
bt %r13d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r13
movzbq 1(%rsi),%r13

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x1=int64#5
# asm 2: bt <c0=%r13d,<x1=%r8
bt %r13d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r13
movzbq 2(%rsi),%r13

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x2=int64#6
# asm 2: bt <c0=%r13d,<x2=%r9
bt %r13d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y2 ^= c2
# asm 1: xor  <c2=int64#8,<y2=int64#3
# asm 2: xor  <c2=%r10,<y2=%rdx
xor  %r10,%rdx

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#10
# asm 2: add  $1,<counter=%r12
add  $1,%r12

# qhasm: =? counter - 40
# asm 1: cmp  $40,<counter=int64#10
# asm 2: cmp  $40,<counter=%r12
cmp  $40,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y2_inner if !=
jne ._cubic_y2_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#7
# asm 2: add  $1,<out_counter=%rax
add  $1,%rax

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#7
# asm 2: cmp  $64,<out_counter=%rax
cmp  $64,%rax
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y2_start if !=
jne ._cubic_y2_start

# qhasm: *(int64 *)(ptr_y + 16) = y2
# asm 1: movq   <y2=int64#3,16(<ptr_y=int64#1)
# asm 2: movq   <y2=%rdx,16(<ptr_y=%rdi)
movq   %rdx,16(%rdi)

# qhasm: y3 = stack_3
# asm 1: movq <stack_3=stack64#10,>y3=int64#3
# asm 2: movq <stack_3=72(%rsp),>y3=%rdx
movq 72(%rsp),%rdx

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#7
# asm 2: mov  $0,>out_counter=%rax
mov  $0,%rax

# qhasm: cubic_y3_start:
._cubic_y3_start:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#8
# asm 2: mov  $0,>c2=%r10
mov  $0,%r10

# qhasm: q2 = 0
# asm 1: mov  $0,>q2=int64#9
# asm 2: mov  $0,>q2=%r11
mov  $0,%r11

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#10
# asm 2: mov  $0,>counter=%r12
mov  $0,%r12

# qhasm: cubic_y3_inner:
._cubic_y3_inner:

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#7d,<c2=int64#8
# asm 2: btc <out_counter=%eax,<c2=%r10
btc %eax,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r13
movzbq 0(%rsi),%r13

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x0=int64#4
# asm 2: bt <c0=%r13d,<x0=%rcx
bt %r13d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r13
movzbq 1(%rsi),%r13

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x1=int64#5
# asm 2: bt <c0=%r13d,<x1=%r8
bt %r13d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r13
movzbq 2(%rsi),%r13

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x2=int64#6
# asm 2: bt <c0=%r13d,<x2=%r9
bt %r13d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y3 ^= c2
# asm 1: xor  <c2=int64#8,<y3=int64#3
# asm 2: xor  <c2=%r10,<y3=%rdx
xor  %r10,%rdx

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#10
# asm 2: add  $1,<counter=%r12
add  $1,%r12

# qhasm: =? counter - 40
# asm 1: cmp  $40,<counter=int64#10
# asm 2: cmp  $40,<counter=%r12
cmp  $40,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y3_inner if !=
jne ._cubic_y3_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#7
# asm 2: add  $1,<out_counter=%rax
add  $1,%rax

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#7
# asm 2: cmp  $64,<out_counter=%rax
cmp  $64,%rax
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y3_start if !=
jne ._cubic_y3_start

# qhasm: *(int64 *)(ptr_y + 24) = y3
# asm 1: movq   <y3=int64#3,24(<ptr_y=int64#1)
# asm 2: movq   <y3=%rdx,24(<ptr_y=%rdi)
movq   %rdx,24(%rdi)

# qhasm: y4 = stack_4
# asm 1: movq <stack_4=stack64#11,>y4=int64#3
# asm 2: movq <stack_4=80(%rsp),>y4=%rdx
movq 80(%rsp),%rdx

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#7
# asm 2: mov  $0,>out_counter=%rax
mov  $0,%rax

# qhasm: cubic_y4_start:
._cubic_y4_start:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#8
# asm 2: mov  $0,>c2=%r10
mov  $0,%r10

# qhasm: q2 = 0
# asm 1: mov  $0,>q2=int64#9
# asm 2: mov  $0,>q2=%r11
mov  $0,%r11

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#10
# asm 2: mov  $0,>counter=%r12
mov  $0,%r12

# qhasm: cubic_y4_inner:
._cubic_y4_inner:

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#7d,<c2=int64#8
# asm 2: btc <out_counter=%eax,<c2=%r10
btc %eax,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r13
movzbq 0(%rsi),%r13

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x0=int64#4
# asm 2: bt <c0=%r13d,<x0=%rcx
bt %r13d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r13
movzbq 1(%rsi),%r13

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x1=int64#5
# asm 2: bt <c0=%r13d,<x1=%r8
bt %r13d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r13
movzbq 2(%rsi),%r13

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x2=int64#6
# asm 2: bt <c0=%r13d,<x2=%r9
bt %r13d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y4 ^= c2
# asm 1: xor  <c2=int64#8,<y4=int64#3
# asm 2: xor  <c2=%r10,<y4=%rdx
xor  %r10,%rdx

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#10
# asm 2: add  $1,<counter=%r12
add  $1,%r12

# qhasm: =? counter - 40
# asm 1: cmp  $40,<counter=int64#10
# asm 2: cmp  $40,<counter=%r12
cmp  $40,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y4_inner if !=
jne ._cubic_y4_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#7
# asm 2: add  $1,<out_counter=%rax
add  $1,%rax

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#7
# asm 2: cmp  $64,<out_counter=%rax
cmp  $64,%rax
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y4_start if !=
jne ._cubic_y4_start

# qhasm: *(int64 *)(ptr_y + 32) = y4
# asm 1: movq   <y4=int64#3,32(<ptr_y=int64#1)
# asm 2: movq   <y4=%rdx,32(<ptr_y=%rdi)
movq   %rdx,32(%rdi)

# qhasm: y5 = stack_5
# asm 1: movq <stack_5=stack64#12,>y5=int64#3
# asm 2: movq <stack_5=88(%rsp),>y5=%rdx
movq 88(%rsp),%rdx

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#7
# asm 2: mov  $0,>out_counter=%rax
mov  $0,%rax

# qhasm: cubic_y5_start:
._cubic_y5_start:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#8
# asm 2: mov  $0,>c2=%r10
mov  $0,%r10

# qhasm: q2 = 0
# asm 1: mov  $0,>q2=int64#9
# asm 2: mov  $0,>q2=%r11
mov  $0,%r11

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#10
# asm 2: mov  $0,>counter=%r12
mov  $0,%r12

# qhasm: cubic_y5_inner:
._cubic_y5_inner:

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#7d,<c2=int64#8
# asm 2: btc <out_counter=%eax,<c2=%r10
btc %eax,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r13
movzbq 0(%rsi),%r13

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x0=int64#4
# asm 2: bt <c0=%r13d,<x0=%rcx
bt %r13d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r13
movzbq 1(%rsi),%r13

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x1=int64#5
# asm 2: bt <c0=%r13d,<x1=%r8
bt %r13d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r13
movzbq 2(%rsi),%r13

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x2=int64#6
# asm 2: bt <c0=%r13d,<x2=%r9
bt %r13d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#9,<c2=int64#8
# asm 2: cmovnc <c1=%r11,<c2=%r10
cmovnc %r11,%r10

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y5 ^= c2
# asm 1: xor  <c2=int64#8,<y5=int64#3
# asm 2: xor  <c2=%r10,<y5=%rdx
xor  %r10,%rdx

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#10
# asm 2: add  $1,<counter=%r12
add  $1,%r12

# qhasm: =? counter - 40
# asm 1: cmp  $40,<counter=int64#10
# asm 2: cmp  $40,<counter=%r12
cmp  $40,%r12
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y5_inner if !=
jne ._cubic_y5_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#7
# asm 2: add  $1,<out_counter=%rax
add  $1,%rax

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#7
# asm 2: cmp  $64,<out_counter=%rax
cmp  $64,%rax
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y5_start if !=
jne ._cubic_y5_start

# qhasm: *(int64 *)(ptr_y + 40) = y5
# asm 1: movq   <y5=int64#3,40(<ptr_y=int64#1)
# asm 2: movq   <y5=%rdx,40(<ptr_y=%rdi)
movq   %rdx,40(%rdi)

# qhasm: y6 = stack_6
# asm 1: movq <stack_6=stack64#13,>y6=int64#3
# asm 2: movq <stack_6=96(%rsp),>y6=%rdx
movq 96(%rsp),%rdx

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#7
# asm 2: mov  $0,>out_counter=%rax
mov  $0,%rax

# qhasm: cubic_y6_start:
._cubic_y6_start:

# qhasm: c2 = 0
# asm 1: mov  $0,>c2=int64#8
# asm 2: mov  $0,>c2=%r10
mov  $0,%r10

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#9
# asm 2: mov  $0,>counter=%r11
mov  $0,%r11

# qhasm: cubic_y6_inner:
._cubic_y6_inner:

# qhasm: c2 <btc> (uint32) out_counter
# asm 1: btc <out_counter=int64#7d,<c2=int64#8
# asm 2: btc <out_counter=%eax,<c2=%r10
btc %eax,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 0)
# asm 1: movzbq 0(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 0(<ptr_p=%rsi),>c0=%r13
movzbq 0(%rsi),%r13

# qhasm: carry ? x0 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x0=int64#4
# asm 2: bt <c0=%r13d,<x0=%rcx
bt %r13d,%rcx

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#10,<c2=int64#8
# asm 2: cmovnc <c1=%r12,<c2=%r10
cmovnc %r12,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 1)
# asm 1: movzbq 1(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 1(<ptr_p=%rsi),>c0=%r13
movzbq 1(%rsi),%r13

# qhasm: carry ? x1 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x1=int64#5
# asm 2: bt <c0=%r13d,<x1=%r8
bt %r13d,%r8

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#10,<c2=int64#8
# asm 2: cmovnc <c1=%r12,<c2=%r10
cmovnc %r12,%r10

# qhasm: c0 = *(uint8 *)(ptr_p + 2)
# asm 1: movzbq 2(<ptr_p=int64#2),>c0=int64#11
# asm 2: movzbq 2(<ptr_p=%rsi),>c0=%r13
movzbq 2(%rsi),%r13

# qhasm: carry ? x2 <bt> (uint32) c0
# asm 1: bt <c0=int64#11d,<x2=int64#6
# asm 2: bt <c0=%r13d,<x2=%r9
bt %r13d,%r9

# qhasm: c2 = c1 if !carry
# asm 1: cmovnc <c1=int64#10,<c2=int64#8
# asm 2: cmovnc <c1=%r12,<c2=%r10
cmovnc %r12,%r10

# qhasm: ptr_p += 3
# asm 1: add  $3,<ptr_p=int64#2
# asm 2: add  $3,<ptr_p=%rsi
add  $3,%rsi

# qhasm: y6 ^= c2
# asm 1: xor  <c2=int64#8,<y6=int64#3
# asm 2: xor  <c2=%r10,<y6=%rdx
xor  %r10,%rdx

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#9
# asm 2: add  $1,<counter=%r11
add  $1,%r11

# qhasm: =? counter - 40
# asm 1: cmp  $40,<counter=int64#9
# asm 2: cmp  $40,<counter=%r11
cmp  $40,%r11
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y6_inner if !=
jne ._cubic_y6_inner

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#7
# asm 2: add  $1,<out_counter=%rax
add  $1,%rax

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#7
# asm 2: cmp  $64,<out_counter=%rax
cmp  $64,%rax
# comment:fp stack unchanged by jump

# qhasm: goto cubic_y6_start if !=
jne ._cubic_y6_start

# qhasm: *(int64 *)(ptr_y + 48) = y6
# asm 1: movq   <y6=int64#3,48(<ptr_y=int64#1)
# asm 2: movq   <y6=%rdx,48(<ptr_y=%rdi)
movq   %rdx,48(%rdi)

# qhasm: r11 = r11_stack
# asm 1: movq <r11_stack=stack64#1,>r11=int64#9
# asm 2: movq <r11_stack=0(%rsp),>r11=%r11
movq 0(%rsp),%r11

# qhasm: r12 = r12_stack
# asm 1: movq <r12_stack=stack64#2,>r12=int64#10
# asm 2: movq <r12_stack=8(%rsp),>r12=%r12
movq 8(%rsp),%r12

# qhasm: r13 = r13_stack
# asm 1: movq <r13_stack=stack64#3,>r13=int64#11
# asm 2: movq <r13_stack=16(%rsp),>r13=%r13
movq 16(%rsp),%r13

# qhasm: r14 = r14_stack
# asm 1: movq <r14_stack=stack64#4,>r14=int64#12
# asm 2: movq <r14_stack=24(%rsp),>r14=%r14
movq 24(%rsp),%r14

# qhasm: r15 = r15_stack
# asm 1: movq <r15_stack=stack64#5,>r15=int64#13
# asm 2: movq <r15_stack=32(%rsp),>r15=%r15
movq 32(%rsp),%r15

# qhasm: rbx = rbx_stack
# asm 1: movq <rbx_stack=stack64#6,>rbx=int64#14
# asm 2: movq <rbx_stack=40(%rsp),>rbx=%rbx
movq 40(%rsp),%rbx

# qhasm: rbp = rbp_stack
# asm 1: movq <rbp_stack=stack64#7,>rbp=int64#15
# asm 2: movq <rbp_stack=48(%rsp),>rbp=%rbp
movq 48(%rsp),%rbp

# qhasm: leave
add %r11,%rsp
mov %rdi,%rax
mov %rsi,%rdx
ret
