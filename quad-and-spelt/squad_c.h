
#ifndef _SQUAD_C_H_
#define _SQUAD_C_H_

#include <stdint.h>
#include <stdlib.h>

// 448 poly , 224 variable
struct squad_poly
{
	uint64_t c[7];
	uint64_t l[224][7];
	uint16_t q_idx[224*223/2][4]; // 4 for padding
	uint8_t tri_idx[448][120];  // 40 cubic term each polynomail
};


void rand_poly( squad_poly * polys );


extern "C" void eval_squad_asm( uint64_t *y , squad_poly * polys , uint64_t * x );

extern "C" void eval_squad_asm3( uint64_t *y , squad_poly * polys , uint64_t * x );

extern "C" void eval_squad_asm4( uint64_t *y , squad_poly * polys , uint64_t * x );

void eval_c( uint64_t y[7] , squad_poly * polys , uint64_t x[4] );



// 448 poly , 224 variable
struct squad_poly2
{
	uint64_t c[7];
	uint64_t l[224][7];
	uint8_t q_idx[448][512]; // 256 quadratic term each polynomial
	uint8_t tri_idx[448][120];  // 40 cubic term each polynomail
};

void rand_poly2( squad_poly2 * polys );

extern "C" void eval_squad_asm2( uint64_t *y , squad_poly2 * polys , uint64_t * x );




inline void set_bit( uint64_t &x , int idx )
{
	uint64_t m = 1;
	m <<= idx;
	x |= m;	
}

inline uint64_t get_bit( uint64_t &x , int idx )
{
	uint64_t m = 1;
	return ((x>>idx)&m);
}

inline void flip_bit( uint64_t & x , int idx )
{
	uint64_t m = 1;
	m <<= idx;
	x ^= m;	
}

inline void rand_x( uint64_t *x , int len )
{
	for(int i=0;i<len;i++) if(rand()&1) set_bit( x[i>>6] , i&63 );
};


#endif
