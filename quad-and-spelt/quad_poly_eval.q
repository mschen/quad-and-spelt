int64 r11
int64 r12
int64 r13
int64 r14
int64 r15
int64 rbx
int64 rbp

caller r11
caller r12
caller r13
caller r14
caller r15
caller rbx
caller rbp

stack64 r11_stack
stack64 r12_stack
stack64 r13_stack
stack64 r14_stack
stack64 r15_stack
stack64 rbx_stack
stack64 rbp_stack


int64 ptr_y
int64 ptr_p
int64 ptr_x

input ptr_y
input ptr_p
input ptr_x

int64 ptr_n
int64 y0
int64 y1
int64 y2
int64 y3
int64 y4
int64 y5
int64 y6

int64 x0
int64 x1
int64 x2
int64 x3

int64 counter
int64 out_counter
int64 offset


stack64 stack_56



enter quad_poly_eval

r11_stack = r11
r12_stack = r12
r13_stack = r13
r14_stack = r14
r15_stack = r15
rbx_stack = rbx
rbp_stack = rbp

offset = 56
stack_56 = offset

x0 = *(int64 *)(ptr_x + 0)
x1 = *(int64 *)(ptr_x + 8)
x2 = *(int64 *)(ptr_x + 16)
x3 = *(int64 *)(ptr_x + 24)

# read constant term
y0 = *(int64 *)(ptr_p + 0)
y1 = *(int64 *)(ptr_p + 8)
y2 = *(int64 *)(ptr_p + 16)
y3 = *(int64 *)(ptr_p + 24)
y4 = *(int64 *)(ptr_p + 32)
y5 = *(int64 *)(ptr_p + 40)
y6 = *(int64 *)(ptr_p + 48)
ptr_p += 56


#
#   Linear Term
#

counter = 0
loop_start0:
carry? x0 <bt> (uint32) counter
goto skip0 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip0:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start0 if !=


counter = 0
loop_start1:
carry? x1 <bt> (uint32) counter
goto skip1 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip1:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start1 if !=


counter = 0
loop_start2:
carry? x2 <bt> (uint32) counter
goto skip2 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip2:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start2 if !=

counter = 0
loop_start3:
carry? x3 <bt> (uint32) counter
goto skip3 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip3:
ptr_p += 56
counter += 1
=? counter - 32
goto loop_start3 if !=


#
#   Quadratic Term
#






out_counter = 0

0000_outer_start:
=? out_counter - 64
goto 0000_outer_end if =

carry? x0 <bt> (uint32) out_counter
goto 0000_inner_start if carry

offset = 223
offset -= out_counter
offset *= stack_56
ptr_p += offset
out_counter += 1
goto 0000_outer_start

0000_inner_start:
counter = 1
counter += out_counter
0000_inner_main:
unsigned>? counter - 63
goto 0000_inner_end if unsigned>

carry? x0 <bt> (uint32) counter
goto skip00 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip00:
ptr_p += 56
counter += 1
goto 0000_inner_main
0000_inner_end:

counter = 0
loop_start01:
carry? x1 <bt> (uint32) counter
goto skip01 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip01:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start01 if !=


counter = 0
loop_start02:
carry? x2 <bt> (uint32) counter
goto skip02 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip02:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start02 if !=

counter = 0
loop_start03:
carry? x3 <bt> (uint32) counter
goto skip03 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip03:
ptr_p += 56
counter += 1
=? counter - 32
goto loop_start03 if !=


out_counter += 1
goto 0000_outer_start
0000_outer_end:











out_counter = 0

1111_outer_start:
=? out_counter - 64
goto 1111_outer_end if =

carry? x1 <bt> (uint32) out_counter
goto 1111_inner_start if carry

offset = 159
offset -= out_counter
offset *= stack_56
ptr_p += offset
out_counter += 1
goto 1111_outer_start

1111_inner_start:
counter = 1
counter += out_counter
1111_inner_main:
unsigned>? counter - 63
goto 1111_inner_end if unsigned>

carry? x1 <bt> (uint32) counter
goto skip11 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip11:
ptr_p += 56
counter += 1
goto 1111_inner_main
1111_inner_end:

counter = 0
loop_start12:
carry? x2 <bt> (uint32) counter
goto skip12 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip12:
ptr_p += 56
counter += 1
=? counter - 64
goto loop_start12 if !=

counter = 0
loop_start13:
carry? x3 <bt> (uint32) counter
goto skip13 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip13:
ptr_p += 56
counter += 1
=? counter - 32
goto loop_start13 if !=

out_counter += 1
goto 1111_outer_start

1111_outer_end:














out_counter = 0

2222_outer_start:
=? out_counter - 64
goto 2222_outer_end if =

carry? x2 <bt> (uint32) out_counter
goto 2222_inner_start if carry

offset = 95
offset -= out_counter
offset *= stack_56
ptr_p += offset
out_counter += 1
goto 2222_outer_start


2222_inner_start:
counter = 1
counter += out_counter
2222_inner_main:
unsigned>? counter - 63
goto 2222_inner_end if unsigned>

carry? x2 <bt> (uint32) counter
goto skip22 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip22:
ptr_p += 56
counter += 1
goto 2222_inner_main
2222_inner_end:

counter = 0
loop_start23:
carry? x3 <bt> (uint32) counter
goto skip23 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip23:
ptr_p += 56
counter += 1
=? counter - 32
goto loop_start23 if !=


out_counter += 1
goto 2222_outer_start

2222_outer_end:







out_counter = 0

3333_outer_start:
=? out_counter - 32
goto 3333_outer_end if =

carry? x3 <bt> (uint32) out_counter
goto 3333_inner_start if carry

offset = 31
offset -= out_counter
offset *= stack_56
ptr_p += offset
out_counter += 1
goto 3333_outer_start


3333_inner_start:
counter = 1
counter += out_counter
3333_inner_main:
unsigned>? counter - 31
goto 3333_inner_end if unsigned>

carry? x3 <bt> (uint32) counter
goto skip33 if !carry
y0 ^= *(uint64 *)(ptr_p + 0)
y1 ^= *(uint64 *)(ptr_p + 8)
y2 ^= *(uint64 *)(ptr_p + 16)
y3 ^= *(uint64 *)(ptr_p + 24)
y4 ^= *(uint64 *)(ptr_p + 32)
y5 ^= *(uint64 *)(ptr_p + 40)
y6 ^= *(uint64 *)(ptr_p + 48)
skip33:
ptr_p += 56
counter += 1
goto 3333_inner_main
3333_inner_end:


out_counter += 1
goto 3333_outer_start

3333_outer_end:












#write back
*(int64 *)(ptr_y + 0) = y0
*(int64 *)(ptr_y + 8) = y1
*(int64 *)(ptr_y + 16) = y2
*(int64 *)(ptr_y + 24) = y3
*(int64 *)(ptr_y + 32) = y4
*(int64 *)(ptr_y + 40) = y5
*(int64 *)(ptr_y + 48) = y6



r11 = r11_stack
r12 = r12_stack
r13 = r13_stack
r14 = r14_stack
r15 = r15_stack
rbx = rbx_stack
rbp = rbp_stack

leave
