
#ifndef _QUAD_C_H_
#define _QUAD_C_H_

#include <stdint.h>
#include <stdlib.h>

// 448 poly , 224 variable
struct quad_poly
{
	uint64_t c[7];
	uint64_t l[224][7];
	uint64_t q[224*223/2][7];
};


void rand_quad_poly( quad_poly * polys );

void quad_poly_eval_c( uint64_t *y , quad_poly * polys , uint64_t *x );
extern "C" void quad_poly_eval( uint64_t *y , quad_poly * polys , uint64_t *x );



#endif
