
# qhasm: int64 r11

# qhasm: int64 r12

# qhasm: int64 r13

# qhasm: int64 r14

# qhasm: int64 r15

# qhasm: int64 rbx

# qhasm: int64 rbp

# qhasm: caller r11

# qhasm: caller r12

# qhasm: caller r13

# qhasm: caller r14

# qhasm: caller r15

# qhasm: caller rbx

# qhasm: caller rbp

# qhasm: stack64 r11_stack

# qhasm: stack64 r12_stack

# qhasm: stack64 r13_stack

# qhasm: stack64 r14_stack

# qhasm: stack64 r15_stack

# qhasm: stack64 rbx_stack

# qhasm: stack64 rbp_stack

# qhasm: int64 ptr_y

# qhasm: int64 ptr_p

# qhasm: int64 ptr_x

# qhasm: input ptr_y

# qhasm: input ptr_p

# qhasm: input ptr_x

# qhasm: int64 ptr_n

# qhasm: int64 y0

# qhasm: int64 y1

# qhasm: int64 y2

# qhasm: int64 y3

# qhasm: int64 y4

# qhasm: int64 y5

# qhasm: int64 y6

# qhasm: int64 x0

# qhasm: int64 x1

# qhasm: int64 x2

# qhasm: int64 x3

# qhasm: int64 counter

# qhasm: int64 out_counter

# qhasm: int64 offset

# qhasm: stack64 stack_56

# qhasm: enter quad_poly_eval
.text
.p2align 5
.globl _quad_poly_eval
.globl quad_poly_eval
_quad_poly_eval:
quad_poly_eval:
mov %rsp,%r11
and $31,%r11
add $64,%r11
sub %r11,%rsp

# qhasm: r11_stack = r11
# asm 1: movq <r11=int64#9,>r11_stack=stack64#1
# asm 2: movq <r11=%r11,>r11_stack=0(%rsp)
movq %r11,0(%rsp)

# qhasm: r12_stack = r12
# asm 1: movq <r12=int64#10,>r12_stack=stack64#2
# asm 2: movq <r12=%r12,>r12_stack=8(%rsp)
movq %r12,8(%rsp)

# qhasm: r13_stack = r13
# asm 1: movq <r13=int64#11,>r13_stack=stack64#3
# asm 2: movq <r13=%r13,>r13_stack=16(%rsp)
movq %r13,16(%rsp)

# qhasm: r14_stack = r14
# asm 1: movq <r14=int64#12,>r14_stack=stack64#4
# asm 2: movq <r14=%r14,>r14_stack=24(%rsp)
movq %r14,24(%rsp)

# qhasm: r15_stack = r15
# asm 1: movq <r15=int64#13,>r15_stack=stack64#5
# asm 2: movq <r15=%r15,>r15_stack=32(%rsp)
movq %r15,32(%rsp)

# qhasm: rbx_stack = rbx
# asm 1: movq <rbx=int64#14,>rbx_stack=stack64#6
# asm 2: movq <rbx=%rbx,>rbx_stack=40(%rsp)
movq %rbx,40(%rsp)

# qhasm: rbp_stack = rbp
# asm 1: movq <rbp=int64#15,>rbp_stack=stack64#7
# asm 2: movq <rbp=%rbp,>rbp_stack=48(%rsp)
movq %rbp,48(%rsp)

# qhasm: offset = 56
# asm 1: mov  $56,>offset=int64#4
# asm 2: mov  $56,>offset=%rcx
mov  $56,%rcx

# qhasm: stack_56 = offset
# asm 1: movq <offset=int64#4,>stack_56=stack64#8
# asm 2: movq <offset=%rcx,>stack_56=56(%rsp)
movq %rcx,56(%rsp)

# qhasm: x0 = *(int64 *)(ptr_x + 0)
# asm 1: movq   0(<ptr_x=int64#3),>x0=int64#4
# asm 2: movq   0(<ptr_x=%rdx),>x0=%rcx
movq   0(%rdx),%rcx

# qhasm: x1 = *(int64 *)(ptr_x + 8)
# asm 1: movq   8(<ptr_x=int64#3),>x1=int64#5
# asm 2: movq   8(<ptr_x=%rdx),>x1=%r8
movq   8(%rdx),%r8

# qhasm: x2 = *(int64 *)(ptr_x + 16)
# asm 1: movq   16(<ptr_x=int64#3),>x2=int64#6
# asm 2: movq   16(<ptr_x=%rdx),>x2=%r9
movq   16(%rdx),%r9

# qhasm: x3 = *(int64 *)(ptr_x + 24)
# asm 1: movq   24(<ptr_x=int64#3),>x3=int64#3
# asm 2: movq   24(<ptr_x=%rdx),>x3=%rdx
movq   24(%rdx),%rdx

# qhasm: y0 = *(int64 *)(ptr_p + 0)
# asm 1: movq   0(<ptr_p=int64#2),>y0=int64#7
# asm 2: movq   0(<ptr_p=%rsi),>y0=%rax
movq   0(%rsi),%rax

# qhasm: y1 = *(int64 *)(ptr_p + 8)
# asm 1: movq   8(<ptr_p=int64#2),>y1=int64#8
# asm 2: movq   8(<ptr_p=%rsi),>y1=%r10
movq   8(%rsi),%r10

# qhasm: y2 = *(int64 *)(ptr_p + 16)
# asm 1: movq   16(<ptr_p=int64#2),>y2=int64#9
# asm 2: movq   16(<ptr_p=%rsi),>y2=%r11
movq   16(%rsi),%r11

# qhasm: y3 = *(int64 *)(ptr_p + 24)
# asm 1: movq   24(<ptr_p=int64#2),>y3=int64#10
# asm 2: movq   24(<ptr_p=%rsi),>y3=%r12
movq   24(%rsi),%r12

# qhasm: y4 = *(int64 *)(ptr_p + 32)
# asm 1: movq   32(<ptr_p=int64#2),>y4=int64#11
# asm 2: movq   32(<ptr_p=%rsi),>y4=%r13
movq   32(%rsi),%r13

# qhasm: y5 = *(int64 *)(ptr_p + 40)
# asm 1: movq   40(<ptr_p=int64#2),>y5=int64#12
# asm 2: movq   40(<ptr_p=%rsi),>y5=%r14
movq   40(%rsi),%r14

# qhasm: y6 = *(int64 *)(ptr_p + 48)
# asm 1: movq   48(<ptr_p=int64#2),>y6=int64#13
# asm 2: movq   48(<ptr_p=%rsi),>y6=%r15
movq   48(%rsi),%r15

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start0:
._loop_start0:

# qhasm: carry? x0 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x0=int64#4
# asm 2: bt <counter=%ebx,<x0=%rcx
bt %ebx,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto skip0 if !carry
jnc ._skip0

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip0:
._skip0:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start0 if !=
jne ._loop_start0

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start1:
._loop_start1:

# qhasm: carry? x1 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x1=int64#5
# asm 2: bt <counter=%ebx,<x1=%r8
bt %ebx,%r8
# comment:fp stack unchanged by jump

# qhasm: goto skip1 if !carry
jnc ._skip1

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip1:
._skip1:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start1 if !=
jne ._loop_start1

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start2:
._loop_start2:

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x2=int64#6
# asm 2: bt <counter=%ebx,<x2=%r9
bt %ebx,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip2 if !carry
jnc ._skip2

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip2:
._skip2:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start2 if !=
jne ._loop_start2

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start3:
._loop_start3:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x3=int64#3
# asm 2: bt <counter=%ebx,<x3=%rdx
bt %ebx,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip3 if !carry
jnc ._skip3

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip3:
._skip3:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#14
# asm 2: cmp  $32,<counter=%rbx
cmp  $32,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start3 if !=
jne ._loop_start3

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#14
# asm 2: mov  $0,>out_counter=%rbx
mov  $0,%rbx

# qhasm: 0000_outer_start:
._0000_outer_start:

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#14
# asm 2: cmp  $64,<out_counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto 0000_outer_end if =
je ._0000_outer_end

# qhasm: carry? x0 <bt> (uint32) out_counter
# asm 1: bt <out_counter=int64#14d,<x0=int64#4
# asm 2: bt <out_counter=%ebx,<x0=%rcx
bt %ebx,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 0000_inner_start if carry
jc ._0000_inner_start

# qhasm: offset = 223
# asm 1: mov  $223,>offset=int64#15
# asm 2: mov  $223,>offset=%rbp
mov  $223,%rbp

# qhasm: offset -= out_counter
# asm 1: sub  <out_counter=int64#14,<offset=int64#15
# asm 2: sub  <out_counter=%rbx,<offset=%rbp
sub  %rbx,%rbp

# qhasm: offset *= stack_56
# asm 1: imul  <stack_56=stack64#8,<offset=int64#15
# asm 2: imul  <stack_56=56(%rsp),<offset=%rbp
imul  56(%rsp),%rbp

# qhasm: ptr_p += offset
# asm 1: add  <offset=int64#15,<ptr_p=int64#2
# asm 2: add  <offset=%rbp,<ptr_p=%rsi
add  %rbp,%rsi

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#14
# asm 2: add  $1,<out_counter=%rbx
add  $1,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto 0000_outer_start
jmp ._0000_outer_start

# qhasm: 0000_inner_start:
._0000_inner_start:

# qhasm: counter = 1
# asm 1: mov  $1,>counter=int64#15
# asm 2: mov  $1,>counter=%rbp
mov  $1,%rbp

# qhasm: counter += out_counter
# asm 1: add  <out_counter=int64#14,<counter=int64#15
# asm 2: add  <out_counter=%rbx,<counter=%rbp
add  %rbx,%rbp

# qhasm: 0000_inner_main:
._0000_inner_main:

# qhasm: unsigned>? counter - 63
# asm 1: cmp  $63,<counter=int64#15
# asm 2: cmp  $63,<counter=%rbp
cmp  $63,%rbp
# comment:fp stack unchanged by jump

# qhasm: goto 0000_inner_end if unsigned>
ja ._0000_inner_end

# qhasm: carry? x0 <bt> (uint32) counter
# asm 1: bt <counter=int64#15d,<x0=int64#4
# asm 2: bt <counter=%ebp,<x0=%rcx
bt %ebp,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto skip00 if !carry
jnc ._skip00

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip00:
._skip00:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#15
# asm 2: add  $1,<counter=%rbp
add  $1,%rbp
# comment:fp stack unchanged by jump

# qhasm: goto 0000_inner_main
jmp ._0000_inner_main

# qhasm: 0000_inner_end:
._0000_inner_end:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#15
# asm 2: mov  $0,>counter=%rbp
mov  $0,%rbp

# qhasm: loop_start01:
._loop_start01:

# qhasm: carry? x1 <bt> (uint32) counter
# asm 1: bt <counter=int64#15d,<x1=int64#5
# asm 2: bt <counter=%ebp,<x1=%r8
bt %ebp,%r8
# comment:fp stack unchanged by jump

# qhasm: goto skip01 if !carry
jnc ._skip01

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip01:
._skip01:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#15
# asm 2: add  $1,<counter=%rbp
add  $1,%rbp

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#15
# asm 2: cmp  $64,<counter=%rbp
cmp  $64,%rbp
# comment:fp stack unchanged by jump

# qhasm: goto loop_start01 if !=
jne ._loop_start01

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#15
# asm 2: mov  $0,>counter=%rbp
mov  $0,%rbp

# qhasm: loop_start02:
._loop_start02:

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#15d,<x2=int64#6
# asm 2: bt <counter=%ebp,<x2=%r9
bt %ebp,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip02 if !carry
jnc ._skip02

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip02:
._skip02:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#15
# asm 2: add  $1,<counter=%rbp
add  $1,%rbp

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#15
# asm 2: cmp  $64,<counter=%rbp
cmp  $64,%rbp
# comment:fp stack unchanged by jump

# qhasm: goto loop_start02 if !=
jne ._loop_start02

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#15
# asm 2: mov  $0,>counter=%rbp
mov  $0,%rbp

# qhasm: loop_start03:
._loop_start03:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#15d,<x3=int64#3
# asm 2: bt <counter=%ebp,<x3=%rdx
bt %ebp,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip03 if !carry
jnc ._skip03

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip03:
._skip03:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#15
# asm 2: add  $1,<counter=%rbp
add  $1,%rbp

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#15
# asm 2: cmp  $32,<counter=%rbp
cmp  $32,%rbp
# comment:fp stack unchanged by jump

# qhasm: goto loop_start03 if !=
jne ._loop_start03

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#14
# asm 2: add  $1,<out_counter=%rbx
add  $1,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto 0000_outer_start
jmp ._0000_outer_start

# qhasm: 0000_outer_end:
._0000_outer_end:

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#4
# asm 2: mov  $0,>out_counter=%rcx
mov  $0,%rcx

# qhasm: 1111_outer_start:
._1111_outer_start:

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#4
# asm 2: cmp  $64,<out_counter=%rcx
cmp  $64,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 1111_outer_end if =
je ._1111_outer_end

# qhasm: carry? x1 <bt> (uint32) out_counter
# asm 1: bt <out_counter=int64#4d,<x1=int64#5
# asm 2: bt <out_counter=%ecx,<x1=%r8
bt %ecx,%r8
# comment:fp stack unchanged by jump

# qhasm: goto 1111_inner_start if carry
jc ._1111_inner_start

# qhasm: offset = 159
# asm 1: mov  $159,>offset=int64#14
# asm 2: mov  $159,>offset=%rbx
mov  $159,%rbx

# qhasm: offset -= out_counter
# asm 1: sub  <out_counter=int64#4,<offset=int64#14
# asm 2: sub  <out_counter=%rcx,<offset=%rbx
sub  %rcx,%rbx

# qhasm: offset *= stack_56
# asm 1: imul  <stack_56=stack64#8,<offset=int64#14
# asm 2: imul  <stack_56=56(%rsp),<offset=%rbx
imul  56(%rsp),%rbx

# qhasm: ptr_p += offset
# asm 1: add  <offset=int64#14,<ptr_p=int64#2
# asm 2: add  <offset=%rbx,<ptr_p=%rsi
add  %rbx,%rsi

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#4
# asm 2: add  $1,<out_counter=%rcx
add  $1,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 1111_outer_start
jmp ._1111_outer_start

# qhasm: 1111_inner_start:
._1111_inner_start:

# qhasm: counter = 1
# asm 1: mov  $1,>counter=int64#14
# asm 2: mov  $1,>counter=%rbx
mov  $1,%rbx

# qhasm: counter += out_counter
# asm 1: add  <out_counter=int64#4,<counter=int64#14
# asm 2: add  <out_counter=%rcx,<counter=%rbx
add  %rcx,%rbx

# qhasm: 1111_inner_main:
._1111_inner_main:

# qhasm: unsigned>? counter - 63
# asm 1: cmp  $63,<counter=int64#14
# asm 2: cmp  $63,<counter=%rbx
cmp  $63,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto 1111_inner_end if unsigned>
ja ._1111_inner_end

# qhasm: carry? x1 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x1=int64#5
# asm 2: bt <counter=%ebx,<x1=%r8
bt %ebx,%r8
# comment:fp stack unchanged by jump

# qhasm: goto skip11 if !carry
jnc ._skip11

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip11:
._skip11:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto 1111_inner_main
jmp ._1111_inner_main

# qhasm: 1111_inner_end:
._1111_inner_end:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start12:
._loop_start12:

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x2=int64#6
# asm 2: bt <counter=%ebx,<x2=%r9
bt %ebx,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip12 if !carry
jnc ._skip12

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip12:
._skip12:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 64
# asm 1: cmp  $64,<counter=int64#14
# asm 2: cmp  $64,<counter=%rbx
cmp  $64,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start12 if !=
jne ._loop_start12

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#14
# asm 2: mov  $0,>counter=%rbx
mov  $0,%rbx

# qhasm: loop_start13:
._loop_start13:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#14d,<x3=int64#3
# asm 2: bt <counter=%ebx,<x3=%rdx
bt %ebx,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip13 if !carry
jnc ._skip13

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip13:
._skip13:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#14
# asm 2: add  $1,<counter=%rbx
add  $1,%rbx

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#14
# asm 2: cmp  $32,<counter=%rbx
cmp  $32,%rbx
# comment:fp stack unchanged by jump

# qhasm: goto loop_start13 if !=
jne ._loop_start13

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#4
# asm 2: add  $1,<out_counter=%rcx
add  $1,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 1111_outer_start
jmp ._1111_outer_start

# qhasm: 1111_outer_end:
._1111_outer_end:

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#4
# asm 2: mov  $0,>out_counter=%rcx
mov  $0,%rcx

# qhasm: 2222_outer_start:
._2222_outer_start:

# qhasm: =? out_counter - 64
# asm 1: cmp  $64,<out_counter=int64#4
# asm 2: cmp  $64,<out_counter=%rcx
cmp  $64,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 2222_outer_end if =
je ._2222_outer_end

# qhasm: carry? x2 <bt> (uint32) out_counter
# asm 1: bt <out_counter=int64#4d,<x2=int64#6
# asm 2: bt <out_counter=%ecx,<x2=%r9
bt %ecx,%r9
# comment:fp stack unchanged by jump

# qhasm: goto 2222_inner_start if carry
jc ._2222_inner_start

# qhasm: offset = 95
# asm 1: mov  $95,>offset=int64#5
# asm 2: mov  $95,>offset=%r8
mov  $95,%r8

# qhasm: offset -= out_counter
# asm 1: sub  <out_counter=int64#4,<offset=int64#5
# asm 2: sub  <out_counter=%rcx,<offset=%r8
sub  %rcx,%r8

# qhasm: offset *= stack_56
# asm 1: imul  <stack_56=stack64#8,<offset=int64#5
# asm 2: imul  <stack_56=56(%rsp),<offset=%r8
imul  56(%rsp),%r8

# qhasm: ptr_p += offset
# asm 1: add  <offset=int64#5,<ptr_p=int64#2
# asm 2: add  <offset=%r8,<ptr_p=%rsi
add  %r8,%rsi

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#4
# asm 2: add  $1,<out_counter=%rcx
add  $1,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 2222_outer_start
jmp ._2222_outer_start

# qhasm: 2222_inner_start:
._2222_inner_start:

# qhasm: counter = 1
# asm 1: mov  $1,>counter=int64#5
# asm 2: mov  $1,>counter=%r8
mov  $1,%r8

# qhasm: counter += out_counter
# asm 1: add  <out_counter=int64#4,<counter=int64#5
# asm 2: add  <out_counter=%rcx,<counter=%r8
add  %rcx,%r8

# qhasm: 2222_inner_main:
._2222_inner_main:

# qhasm: unsigned>? counter - 63
# asm 1: cmp  $63,<counter=int64#5
# asm 2: cmp  $63,<counter=%r8
cmp  $63,%r8
# comment:fp stack unchanged by jump

# qhasm: goto 2222_inner_end if unsigned>
ja ._2222_inner_end

# qhasm: carry? x2 <bt> (uint32) counter
# asm 1: bt <counter=int64#5d,<x2=int64#6
# asm 2: bt <counter=%r8d,<x2=%r9
bt %r8d,%r9
# comment:fp stack unchanged by jump

# qhasm: goto skip22 if !carry
jnc ._skip22

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip22:
._skip22:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#5
# asm 2: add  $1,<counter=%r8
add  $1,%r8
# comment:fp stack unchanged by jump

# qhasm: goto 2222_inner_main
jmp ._2222_inner_main

# qhasm: 2222_inner_end:
._2222_inner_end:

# qhasm: counter = 0
# asm 1: mov  $0,>counter=int64#5
# asm 2: mov  $0,>counter=%r8
mov  $0,%r8

# qhasm: loop_start23:
._loop_start23:

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#5d,<x3=int64#3
# asm 2: bt <counter=%r8d,<x3=%rdx
bt %r8d,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip23 if !carry
jnc ._skip23

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip23:
._skip23:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#5
# asm 2: add  $1,<counter=%r8
add  $1,%r8

# qhasm: =? counter - 32
# asm 1: cmp  $32,<counter=int64#5
# asm 2: cmp  $32,<counter=%r8
cmp  $32,%r8
# comment:fp stack unchanged by jump

# qhasm: goto loop_start23 if !=
jne ._loop_start23

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#4
# asm 2: add  $1,<out_counter=%rcx
add  $1,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 2222_outer_start
jmp ._2222_outer_start

# qhasm: 2222_outer_end:
._2222_outer_end:

# qhasm: out_counter = 0
# asm 1: mov  $0,>out_counter=int64#4
# asm 2: mov  $0,>out_counter=%rcx
mov  $0,%rcx

# qhasm: 3333_outer_start:
._3333_outer_start:

# qhasm: =? out_counter - 32
# asm 1: cmp  $32,<out_counter=int64#4
# asm 2: cmp  $32,<out_counter=%rcx
cmp  $32,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 3333_outer_end if =
je ._3333_outer_end

# qhasm: carry? x3 <bt> (uint32) out_counter
# asm 1: bt <out_counter=int64#4d,<x3=int64#3
# asm 2: bt <out_counter=%ecx,<x3=%rdx
bt %ecx,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto 3333_inner_start if carry
jc ._3333_inner_start

# qhasm: offset = 31
# asm 1: mov  $31,>offset=int64#5
# asm 2: mov  $31,>offset=%r8
mov  $31,%r8

# qhasm: offset -= out_counter
# asm 1: sub  <out_counter=int64#4,<offset=int64#5
# asm 2: sub  <out_counter=%rcx,<offset=%r8
sub  %rcx,%r8

# qhasm: offset *= stack_56
# asm 1: imul  <stack_56=stack64#8,<offset=int64#5
# asm 2: imul  <stack_56=56(%rsp),<offset=%r8
imul  56(%rsp),%r8

# qhasm: ptr_p += offset
# asm 1: add  <offset=int64#5,<ptr_p=int64#2
# asm 2: add  <offset=%r8,<ptr_p=%rsi
add  %r8,%rsi

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#4
# asm 2: add  $1,<out_counter=%rcx
add  $1,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 3333_outer_start
jmp ._3333_outer_start

# qhasm: 3333_inner_start:
._3333_inner_start:

# qhasm: counter = 1
# asm 1: mov  $1,>counter=int64#5
# asm 2: mov  $1,>counter=%r8
mov  $1,%r8

# qhasm: counter += out_counter
# asm 1: add  <out_counter=int64#4,<counter=int64#5
# asm 2: add  <out_counter=%rcx,<counter=%r8
add  %rcx,%r8

# qhasm: 3333_inner_main:
._3333_inner_main:

# qhasm: unsigned>? counter - 31
# asm 1: cmp  $31,<counter=int64#5
# asm 2: cmp  $31,<counter=%r8
cmp  $31,%r8
# comment:fp stack unchanged by jump

# qhasm: goto 3333_inner_end if unsigned>
ja ._3333_inner_end

# qhasm: carry? x3 <bt> (uint32) counter
# asm 1: bt <counter=int64#5d,<x3=int64#3
# asm 2: bt <counter=%r8d,<x3=%rdx
bt %r8d,%rdx
# comment:fp stack unchanged by jump

# qhasm: goto skip33 if !carry
jnc ._skip33

# qhasm: y0 ^= *(uint64 *)(ptr_p + 0)
# asm 1: xorq 0(<ptr_p=int64#2),<y0=int64#7
# asm 2: xorq 0(<ptr_p=%rsi),<y0=%rax
xorq 0(%rsi),%rax

# qhasm: y1 ^= *(uint64 *)(ptr_p + 8)
# asm 1: xorq 8(<ptr_p=int64#2),<y1=int64#8
# asm 2: xorq 8(<ptr_p=%rsi),<y1=%r10
xorq 8(%rsi),%r10

# qhasm: y2 ^= *(uint64 *)(ptr_p + 16)
# asm 1: xorq 16(<ptr_p=int64#2),<y2=int64#9
# asm 2: xorq 16(<ptr_p=%rsi),<y2=%r11
xorq 16(%rsi),%r11

# qhasm: y3 ^= *(uint64 *)(ptr_p + 24)
# asm 1: xorq 24(<ptr_p=int64#2),<y3=int64#10
# asm 2: xorq 24(<ptr_p=%rsi),<y3=%r12
xorq 24(%rsi),%r12

# qhasm: y4 ^= *(uint64 *)(ptr_p + 32)
# asm 1: xorq 32(<ptr_p=int64#2),<y4=int64#11
# asm 2: xorq 32(<ptr_p=%rsi),<y4=%r13
xorq 32(%rsi),%r13

# qhasm: y5 ^= *(uint64 *)(ptr_p + 40)
# asm 1: xorq 40(<ptr_p=int64#2),<y5=int64#12
# asm 2: xorq 40(<ptr_p=%rsi),<y5=%r14
xorq 40(%rsi),%r14

# qhasm: y6 ^= *(uint64 *)(ptr_p + 48)
# asm 1: xorq 48(<ptr_p=int64#2),<y6=int64#13
# asm 2: xorq 48(<ptr_p=%rsi),<y6=%r15
xorq 48(%rsi),%r15
# comment:fp stack unchanged by fallthrough

# qhasm: skip33:
._skip33:

# qhasm: ptr_p += 56
# asm 1: add  $56,<ptr_p=int64#2
# asm 2: add  $56,<ptr_p=%rsi
add  $56,%rsi

# qhasm: counter += 1
# asm 1: add  $1,<counter=int64#5
# asm 2: add  $1,<counter=%r8
add  $1,%r8
# comment:fp stack unchanged by jump

# qhasm: goto 3333_inner_main
jmp ._3333_inner_main

# qhasm: 3333_inner_end:
._3333_inner_end:

# qhasm: out_counter += 1
# asm 1: add  $1,<out_counter=int64#4
# asm 2: add  $1,<out_counter=%rcx
add  $1,%rcx
# comment:fp stack unchanged by jump

# qhasm: goto 3333_outer_start
jmp ._3333_outer_start

# qhasm: 3333_outer_end:
._3333_outer_end:

# qhasm: *(int64 *)(ptr_y + 0) = y0
# asm 1: movq   <y0=int64#7,0(<ptr_y=int64#1)
# asm 2: movq   <y0=%rax,0(<ptr_y=%rdi)
movq   %rax,0(%rdi)

# qhasm: *(int64 *)(ptr_y + 8) = y1
# asm 1: movq   <y1=int64#8,8(<ptr_y=int64#1)
# asm 2: movq   <y1=%r10,8(<ptr_y=%rdi)
movq   %r10,8(%rdi)

# qhasm: *(int64 *)(ptr_y + 16) = y2
# asm 1: movq   <y2=int64#9,16(<ptr_y=int64#1)
# asm 2: movq   <y2=%r11,16(<ptr_y=%rdi)
movq   %r11,16(%rdi)

# qhasm: *(int64 *)(ptr_y + 24) = y3
# asm 1: movq   <y3=int64#10,24(<ptr_y=int64#1)
# asm 2: movq   <y3=%r12,24(<ptr_y=%rdi)
movq   %r12,24(%rdi)

# qhasm: *(int64 *)(ptr_y + 32) = y4
# asm 1: movq   <y4=int64#11,32(<ptr_y=int64#1)
# asm 2: movq   <y4=%r13,32(<ptr_y=%rdi)
movq   %r13,32(%rdi)

# qhasm: *(int64 *)(ptr_y + 40) = y5
# asm 1: movq   <y5=int64#12,40(<ptr_y=int64#1)
# asm 2: movq   <y5=%r14,40(<ptr_y=%rdi)
movq   %r14,40(%rdi)

# qhasm: *(int64 *)(ptr_y + 48) = y6
# asm 1: movq   <y6=int64#13,48(<ptr_y=int64#1)
# asm 2: movq   <y6=%r15,48(<ptr_y=%rdi)
movq   %r15,48(%rdi)

# qhasm: r11 = r11_stack
# asm 1: movq <r11_stack=stack64#1,>r11=int64#9
# asm 2: movq <r11_stack=0(%rsp),>r11=%r11
movq 0(%rsp),%r11

# qhasm: r12 = r12_stack
# asm 1: movq <r12_stack=stack64#2,>r12=int64#10
# asm 2: movq <r12_stack=8(%rsp),>r12=%r12
movq 8(%rsp),%r12

# qhasm: r13 = r13_stack
# asm 1: movq <r13_stack=stack64#3,>r13=int64#11
# asm 2: movq <r13_stack=16(%rsp),>r13=%r13
movq 16(%rsp),%r13

# qhasm: r14 = r14_stack
# asm 1: movq <r14_stack=stack64#4,>r14=int64#12
# asm 2: movq <r14_stack=24(%rsp),>r14=%r14
movq 24(%rsp),%r14

# qhasm: r15 = r15_stack
# asm 1: movq <r15_stack=stack64#5,>r15=int64#13
# asm 2: movq <r15_stack=32(%rsp),>r15=%r15
movq 32(%rsp),%r15

# qhasm: rbx = rbx_stack
# asm 1: movq <rbx_stack=stack64#6,>rbx=int64#14
# asm 2: movq <rbx_stack=40(%rsp),>rbx=%rbx
movq 40(%rsp),%rbx

# qhasm: rbp = rbp_stack
# asm 1: movq <rbp_stack=stack64#7,>rbp=int64#15
# asm 2: movq <rbp_stack=48(%rsp),>rbp=%rbp
movq 48(%rsp),%rbp

# qhasm: leave
add %r11,%rsp
mov %rdi,%rax
mov %rsi,%rdx
ret
