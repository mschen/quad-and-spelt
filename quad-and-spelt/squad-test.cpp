
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "squad_c.h"
#include "quad_c.h"
#include "benchmark.h"



#define NUM_TEST 10000



int main()
{
#ifndef _NO_ASM_
	std::cout << "********** ASM MODE **********\n";
#endif

	benchmark bench_q,bench_s;
	bm_init( & bench_q );
	bm_init( & bench_s );
	
#ifndef _NO_ASM_
	benchmark bench_q_asm,bench_s_asm;  // 4 quad term 40 cubic term
	benchmark bench_s2_asm;   // only 40 cubic term
	benchmark bench_s3_asm;   // 4 quad term 40 cubic term
	benchmark bench_s4_asm;   // 4 quad term 20 cubic term
	bm_init( & bench_q_asm );
	bm_init( & bench_s_asm );
	bm_init( & bench_s2_asm );
	bm_init( & bench_s3_asm );
	bm_init( & bench_s4_asm );
#endif

	squad_poly polys;
	rand_poly( & polys );
	
	squad_poly2 s_polys2;
	rand_poly2( & s_polys2 );
	
	quad_poly q_polys;
	rand_quad_poly( & q_polys );
	
	std::cout << "c: " << q_polys.c[0] << " ... " << q_polys.c[6] << "\n";
	
	uint64_t x[4];
	uint64_t y0[7],y1[7],y2[7];
	
	rand_x( x , 224 );

	eval_c( y0 , & polys , x );

	uint64_t check = 0;
	for(unsigned i=0;i<NUM_TEST;i++){
		BENCHMARK( bench_s , {
		eval_c( y1 , & polys , x );
		});
		BENCHMARK( bench_q , {
		quad_poly_eval_c( y2 , & q_polys , x );
		});
#ifndef _NO_ASM_
		BENCHMARK( bench_q_asm , {
		quad_poly_eval( y2 , & q_polys , x );
		});
		BENCHMARK( bench_s_asm , {
		eval_squad_asm( y1 , & polys , x );
		});
		BENCHMARK( bench_s2_asm , {
		eval_squad_asm2( y1 , & s_polys2 , x );
		});
		BENCHMARK( bench_s3_asm , {
		eval_squad_asm3( y1 , & polys , x );
		});
		BENCHMARK( bench_s4_asm , {
		eval_squad_asm3( y1 , & polys , x );
		});
#endif
		for(int j=0;j<7;j++) check ^= y0[j]^y1[j]^y2[j];
	}
	std::cout << "y: " << y2[0] << " ... " << y2[6] << "\n";
	std::cout << check << "\n" << "\n";
	
	char buff[2048];
	bm_dump( buff , 2048 , &bench_s );
	std::cout << "C   squad :" << buff << "\n";
	
	bm_dump( buff , 2048 , &bench_q );
	std::cout << "C    quad :" << buff << "\n";

#ifndef _NO_ASM_
	bm_dump( buff , 2048 , &bench_q_asm );
	std::cout << "asm  quad :" << buff << "\n";
	
	bm_dump( buff , 2048 , &bench_s_asm );
	std::cout << "3q 40c squad :" << buff << "\n";
	
	bm_dump( buff , 2048 , &bench_s2_asm );
	std::cout << "40c    squad :" << buff << "\n";
	
	bm_dump( buff , 2048 , &bench_s3_asm );
	std::cout << "4q 40c squad :" << buff << "\n";
	
	bm_dump( buff , 2048 , &bench_s4_asm );
	std::cout << "4q 20c squad :" << buff << "\n";
#endif
	return 0;	
}
