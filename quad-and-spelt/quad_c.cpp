
#include "squad_c.h"
#include "quad_c.h"

#define NUM_VARS 224
#define NUM_POLYS 448


//struct quad_poly
//{
//	uint64_t c[7];
//	uint64_t l[224][7];
//	uint64_t q[224*223/2][7];
//};
void rand_quad_poly( quad_poly * polys )
{
	rand_x( polys->c , NUM_POLYS );
	for(int i=0;i<NUM_VARS;i++) rand_x( polys->l[i] , NUM_POLYS );
	int idx = 0;
	for(int i=0;i<NUM_VARS;i++){
		for(int j=i+1;j<NUM_VARS;j++){
			rand_x( polys->q[idx] , NUM_POLYS );
			idx++;
		}	
	}
}


//struct quad_poly
//{
//	uint64_t c[7];
//	uint64_t l[224][7];
//	uint64_t q[224*223/2][7];
//};



void quad_poly_eval_c( uint64_t *y , quad_poly * polys , uint64_t *x )
{
	for(int i=0;i<7;i++) y[i] = polys->c[i];
	for(int j=0;j<NUM_VARS;j++){
		if( !get_bit( x[j>>6] , j&63 )) continue;
		for(int i=0;i<7;i++) y[i] ^= polys->l[j][i];
	}
	int idx = 0;
	for(int j=0;j<NUM_VARS;j++) {
		if( !get_bit(x[j>>6],j&63) ) { idx+=(NUM_VARS-1-j); continue; }
		for(int k=j+1;k<NUM_VARS;k++){
			if(!get_bit(x[k>>6],k&63)) { idx++; continue; }	
			for(int i=0;i<7;i++) y[i] ^= polys->q[idx][i];
			idx++;
		}
	}
}
