


#include "batch_squad.h"

//#include <tmmintrin.h>  /// ssse3

#include <stdlib.h>
#include <string.h>
#include <stdint.h>


void batch_squad_16_32_32_4_10_8_5( __m128i * res , __m128i * inp )
{

}

void batch_squad_16_32_32_4_20_15_10( __m128i * res , __m128i * inp )
{

}

void batch_squad_16_96_96_4_32_16_8( __m128i * res , __m128i * inp )
{

}


/*

static __m128i _mask_lo = _mm_set1_epi32( 0x0f0f0f0f );
static __m128i _mask_sign = _mm_set1_epi32( 0x80808080 );


static __m128i _log_tab = _mm_set_epi32( 0x0c0b0d06, 0x07090e03 , 0x0a050802 , 0x04010081 );
static __m128i _alog_tab = _mm_set_epi32( 0x01090d0f , 0x0e070a05 , 0x0b0c0603 , 0x08040201 );

#define ADDS(x,y) _mm_adds_epi8((x),(y))
#define ADD(x,y) _mm_add_epi8((x),(y))
#define PSHUFB(x,y) _mm_shuffle_epi8((x),(y))
#define AND(x,y) _mm_and_si128((x),(y))
#define OR(x,y) _mm_or_si128((x),(y))

inline __m128i _mod_15_epi8( const __m128i inp )
{
	__m128i rop = ADD(AND(inp,_mask_lo),AND(_mask_lo,_mm_srli_epi32(inp,4)));
	return  ADD(AND(rop,_mask_lo),AND(_mask_lo,_mm_srli_epi32(rop,4)));
}

inline __m128i _mul_2( const __m128i log_a , const __m128i log_b )
{
	__m128i sum = ADDS( log_a , log_b );
	__m128i sign = AND( _mask_sign , sum );
	sum = ADD(AND(sum,_mask_lo),AND(_mask_lo,_mm_srli_epi32(sum,4)));
	return PSHUFB( _alog_tab , OR( sign , sum ) );
}

inline __m128i _mul_3( const __m128i log_a , const __m128i log_b , const __m128i log_c )
{
	__m128i sum = ADDS( ADDS( log_a , log_b ) , log_c );
	__m128i sign = AND( _mask_sign , sum );
	return PSHUFB( _alog_tab , OR( sign , _mod_15_epi8(sum) ) );
}

inline __m128i _mul_4( const __m128i log_a , const __m128i log_b 
				, const __m128i log_c , const __m128i log_d )
{
	__m128i sum = ADDS( ADDS( log_a, log_b ) , ADDS( log_c , log_d ) );
	__m128i sign = AND( _mask_sign , sum );
	return PSHUFB( _alog_tab , OR( sign , _mod_15_epi8(sum) ) );
}

#undef ADDS
#undef PSHUFB
#undef AND
#undef OR

static __m128i _gf16_mul_tab[16] = {
	_mm_set_epi32(0x00000000,0x00000000,0x00000000,0x00000000),
	_mm_set_epi32(0x0f0e0d0c,0x0b0a0908,0x07060504,0x03020100),
	_mm_set_epi32(0x0d0f090b,0x05070103,0x0e0c0a08,0x06040200),
	_mm_set_epi32(0x02010407,0x0e0d080b,0x090a0f0c,0x05060300),
	_mm_set_epi32(0x090d0105,0x0a0e0206,0x0f0b0703,0x0c080400),
	_mm_set_epi32(0x06030c09,0x01040b0e,0x080d0207,0x0f0a0500),
	_mm_set_epi32(0x0402080e,0x0f090305,0x01070d0b,0x0a0c0600),
	_mm_set_epi32(0x0b0c0502,0x04030a0d,0x0601080f,0x090e0700),
	_mm_set_epi32(0x0109020a,0x070f040c,0x0d050e06,0x0b030800),
	_mm_set_epi32(0x0e070f06,0x0c050d04,0x0a030b02,0x08010900),
	_mm_set_epi32(0x0c060b01,0x0208050f,0x0309040e,0x0d070a00),
	_mm_set_epi32(0x0308060d,0x09020c07,0x040f010a,0x0e050b00),
	_mm_set_epi32(0x0804030f,0x0d01060a,0x020e0905,0x070b0c00),
	_mm_set_epi32(0x070a0e03,0x060b0f02,0x05080c01,0x04090d00),
	_mm_set_epi32(0x050b0a04,0x08060709,0x0c02030d,0x010f0e00),
	_mm_set_epi32(0x0a050708,0x030c0e01,0x0b040609,0x020d0f00)
};

inline __m128i _mul( uint8_t a , const __m128i b ){
	return _mm_shuffle_epi8( _gf16_mul_tab[a] , b );	
}



////////////////////////////////////////////////////////////////////////////////

// L1 cache size: 2048 xmm register
//static uint8_t linear_16_32_10[32][32] = {0};
//static uint8_t quadratic_16_32_10[32][10*2] = {0};
//static uint8_t cubic_16_32_10[32][8*3] = {0};
//static uint8_t quartic_16_32_10[32][4*5] = {0};

#include "key_16_32.cpp"

void _batch_squad_16_32_32_4_10_8_5( __m128i * res , __m128i * inp )
{
	for(unsigned i=0;i<32;i++){
		res[i] = _mul( linear_16_32_10[i][0] , inp[0] );
		for(unsigned j=1;j<32;j++)
			res[i] = _mm_xor_si128( res[i] , _mul( linear_16_32_10[i][j] , inp[j] ));	
	}
	
	__m128i log_inp[32];
	for(unsigned i=0;i<32;i++) log_inp[i] = _mm_shuffle_epi8(_log_tab,inp[i]);
	
	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<10;j++){
			res[i] = _mm_xor_si128(res[i],_mul_2(log_inp[quadratic_16_32_10[i][k]] 
						, log_inp[quadratic_16_32_10[i][k+1]]));
			k+=2;
		}
	}
	
	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<8;j++){
			res[i] = _mm_xor_si128(res[i],_mul_3(log_inp[cubic_16_32_10[i][k]] 
				, log_inp[cubic_16_32_10[i][k+1]] , log_inp[cubic_16_32_10[i][k+2]] ));
			k+=3;
		}
	}	

	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<5;j++){
			res[i] = _mm_xor_si128(res[i],_mul_4(log_inp[quartic_16_32_10[i][k]] 
				, log_inp[quartic_16_32_10[i][k+1]] , log_inp[quartic_16_32_10[i][k+2]] 
				, log_inp[quartic_16_32_10[i][k+3]] ));
			k+=4;
		}
	}
}

void batch_squad_16_32_32_4_10_8_5( __m128i * res , __m128i * inp )
{
	__m128i par_inp[32];
	__m128i par_res[32];
	for(unsigned i=0;i<32;i++) par_inp[i] = _mm_and_si128( _mask_lo , inp[i] );
	_batch_squad_16_32_32_4_10_8_5( par_res , par_inp );
	for(unsigned i=0;i<32;i++) par_inp[i] = _mm_and_si128( _mask_lo , _mm_srli_epi32(inp[i],4) );
	_batch_squad_16_32_32_4_10_8_5( res , par_inp );
	for(unsigned i=0;i<32;i++) res[i] = _mm_or_si128( res[i] , par_res[i] );
}



/////////////////////////////////////////////////////////////////////////////////



//static uint8_t linear_16_32_20[32][32] = {0};
//static uint8_t quadratic_16_32_20[32][20*2] = {0};
//static uint8_t cubic_16_32_20[32][15*3] = {0};
//static uint8_t quartic_16_32_20[32][10*4] = {0};

void _batch_squad_16_32_32_4_20_15_10( __m128i * res , __m128i * inp )
{
	for(unsigned i=0;i<32;i++){
		res[i] = _mul( linear_16_32_20[i][0] , inp[0] );
		for(unsigned j=1;j<32;j++)
			res[i] = _mm_xor_si128( res[i] , _mul( linear_16_32_20[i][j] , inp[j] ));	
	}
	
	__m128i log_inp[32];
	for(unsigned i=0;i<32;i++) log_inp[i] = _mm_shuffle_epi8(_log_tab,inp[i]);
	
	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<20;j++){
			res[i] = _mm_xor_si128(res[i],_mul_2(log_inp[quadratic_16_32_20[i][k]] 
						, log_inp[quadratic_16_32_20[i][k+1]]));
			k+=2;
		}
	}
	
	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<15;j++){
			res[i] = _mm_xor_si128(res[i],_mul_3(log_inp[cubic_16_32_20[i][k]] 
				, log_inp[cubic_16_32_20[i][k+1]] , log_inp[cubic_16_32_20[i][k+2]] ));
			k+=3;
		}
	}	

	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<10;j++){
			res[i] = _mm_xor_si128(res[i],_mul_4(log_inp[quartic_16_32_20[i][k]] 
				, log_inp[quartic_16_32_20[i][k+1]] , log_inp[quartic_16_32_20[i][k+2]] 
				, log_inp[quartic_16_32_20[i][k+3]] ));
			k+=4;
		}
	}
}



void batch_squad_16_32_32_4_20_15_10( __m128i * res , __m128i * inp )
{
	__m128i par_inp[32];
	__m128i par_res[32];
	for(unsigned i=0;i<32;i++) par_inp[i] = _mm_and_si128( _mask_lo , inp[i] );
	_batch_squad_16_32_32_4_20_15_10( par_res , par_inp );
	for(unsigned i=0;i<32;i++) par_inp[i] = _mm_and_si128( _mask_lo , _mm_srli_epi32(inp[i],4) );
	_batch_squad_16_32_32_4_20_15_10( res , par_inp );
	for(unsigned i=0;i<32;i++) res[i] = _mm_or_si128( res[i] , par_res[i] );

}

//////////////////////////////////////////////////////////////////////////////

void _batch_squad_16_96_96_4_32_16_8( __m128i * res , __m128i * inp )
{
	for(unsigned i=0;i<96;i++){
		res[i] = _mul( linear_16_96_32[i][0] , inp[0] );
		for(unsigned j=1;j<96;j++)
			res[i] = _mm_xor_si128( res[i] , _mul( linear_16_96_32[i][j] , inp[j] ));	
	}
	
	__m128i log_inp[96];
	for(unsigned i=0;i<96;i++) log_inp[i] = _mm_shuffle_epi8(_log_tab,inp[i]);
	
	for(unsigned i=0;i<96;i++){
		unsigned k=0;
		for(unsigned j=0;j<32;j++){
			res[i] = _mm_xor_si128(res[i],_mul_2(log_inp[quadratic_16_96_32[i][k]] 
						, log_inp[quadratic_16_96_32[i][k+1]]));
			k+=2;
		}
	}
	
	for(unsigned i=0;i<96;i++){
		unsigned k=0;
		for(unsigned j=0;j<16;j++){
			res[i] = _mm_xor_si128(res[i],_mul_3(log_inp[cubic_16_96_32[i][k]] 
				, log_inp[cubic_16_96_32[i][k+1]] , log_inp[cubic_16_96_32[i][k+2]] ));
			k+=3;
		}
	}	

	for(unsigned i=0;i<96;i++){
		unsigned k=0;
		for(unsigned j=0;j<8;j++){
			res[i] = _mm_xor_si128(res[i],_mul_4(log_inp[quartic_16_96_32[i][k]] 
				, log_inp[quartic_16_96_32[i][k+1]] , log_inp[quartic_16_96_32[i][k+2]] 
				, log_inp[quartic_16_96_32[i][k+3]] ));
			k+=4;
		}
	}
}



void batch_squad_16_96_96_4_32_16_8( __m128i * res , __m128i * inp )
{
	__m128i par_inp[96];
	__m128i par_res[96];
	for(unsigned i=0;i<96;i++) par_inp[i] = _mm_and_si128( _mask_lo , inp[i] );
	_batch_squad_16_96_96_4_32_16_8( par_res , par_inp );
	for(unsigned i=0;i<96;i++) par_inp[i] = _mm_and_si128( _mask_lo , _mm_srli_epi32(inp[i],4) );
	_batch_squad_16_96_96_4_32_16_8( res , par_inp );
	for(unsigned i=0;i<96;i++) res[i] = _mm_or_si128( res[i] , par_res[i] );

}

////////////////////////////////////////////////////////////////////


*/

