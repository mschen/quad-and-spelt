


#include "batch_squad.h"

#include <tmmintrin.h>  /// ssse3

#include <stdlib.h>
#include <string.h>
#include <stdint.h>


static __m128i _mask_lo = _mm_set1_epi32( 0x0f0f0f0f );
static __m128i _mask_sign = _mm_set1_epi32( 0x80808080 );


static __m128i _log_tab = _mm_set_epi32( 0x0c0b0d06, 0x07090e03 , 0x0a050802 , 0x04010081 );
static __m128i _alog_tab = _mm_set_epi32( 0x01090d0f , 0x0e070a05 , 0x0b0c0603 , 0x08040201 );

#define ADDS(x,y) _mm_adds_epi8((x),(y))
#define ADD(x,y) _mm_add_epi8((x),(y))
#define PSHUFB(x,y) _mm_shuffle_epi8((x),(y))
#define AND(x,y) _mm_and_si128((x),(y))
#define OR(x,y) _mm_or_si128((x),(y))

inline __m128i _mod_15_epi8( const __m128i inp )
{
	__m128i rop = ADD(AND(inp,_mask_lo),AND(_mask_lo,_mm_srli_epi32(inp,4)));
	return  AND(_mask_lo,ADD(rop,_mm_srli_epi32(rop,4)));
}

inline __m128i _mul_2( const __m128i log_a , const __m128i log_b )
{
	__m128i sum = ADDS( log_a , log_b );
	__m128i sign = AND( _mask_sign , sum );
	sum = AND(_mask_lo,ADD(sum,_mm_srli_epi32(sum,4)));
	return PSHUFB( _alog_tab , OR( sign , sum ) );
}

inline __m128i _mul_3( const __m128i log_a , const __m128i log_b , const __m128i log_c )
{
	__m128i sum = ADDS( ADDS( log_a , log_b ) , log_c );
	__m128i sign = AND( _mask_sign , sum );
	return PSHUFB( _alog_tab , OR( sign , _mod_15_epi8(sum) ) );
}

inline __m128i _mul_4( const __m128i log_a , const __m128i log_b 
				, const __m128i log_c , const __m128i log_d )
{
	__m128i sum = ADDS( ADDS( log_a, log_b ) , ADDS( log_c , log_d ) );
	__m128i sign = AND( _mask_sign , sum );
	return PSHUFB( _alog_tab , OR( sign , _mod_15_epi8(sum) ) );
}

#undef ADDS
#undef PSHUFB
#undef AND
#undef OR

static __m128i _gf16_mul_tab[16] = {
	_mm_set_epi32(0x00000000,0x00000000,0x00000000,0x00000000),
	_mm_set_epi32(0x0f0e0d0c,0x0b0a0908,0x07060504,0x03020100),
	_mm_set_epi32(0x0d0f090b,0x05070103,0x0e0c0a08,0x06040200),
	_mm_set_epi32(0x02010407,0x0e0d080b,0x090a0f0c,0x05060300),
	_mm_set_epi32(0x090d0105,0x0a0e0206,0x0f0b0703,0x0c080400),
	_mm_set_epi32(0x06030c09,0x01040b0e,0x080d0207,0x0f0a0500),
	_mm_set_epi32(0x0402080e,0x0f090305,0x01070d0b,0x0a0c0600),
	_mm_set_epi32(0x0b0c0502,0x04030a0d,0x0601080f,0x090e0700),
	_mm_set_epi32(0x0109020a,0x070f040c,0x0d050e06,0x0b030800),
	_mm_set_epi32(0x0e070f06,0x0c050d04,0x0a030b02,0x08010900),
	_mm_set_epi32(0x0c060b01,0x0208050f,0x0309040e,0x0d070a00),
	_mm_set_epi32(0x0308060d,0x09020c07,0x040f010a,0x0e050b00),
	_mm_set_epi32(0x0804030f,0x0d01060a,0x020e0905,0x070b0c00),
	_mm_set_epi32(0x070a0e03,0x060b0f02,0x05080c01,0x04090d00),
	_mm_set_epi32(0x050b0a04,0x08060709,0x0c02030d,0x010f0e00),
	_mm_set_epi32(0x0a050708,0x030c0e01,0x0b040609,0x020d0f00)
};

inline __m128i _mul( uint8_t a , const __m128i b ){
	return _mm_shuffle_epi8( _gf16_mul_tab[a] , b );	
}



////////////////////////////////////////////////////////////////////////////////

// L1 cache size: 2048 xmm register
//static uint8_t linear_16_32_10[32][32] = {0};
//static uint8_t quadratic_16_32_10[32][10*2] = {0};
//static uint8_t cubic_16_32_10[32][8*3] = {0};
//static uint8_t quartic_16_32_10[32][4*5] = {0};

#include "key_16_32.cpp"

void _batch_squad_16_32_32_4_10_8_5( __m128i * res , __m128i * inp )
{
	for(unsigned i=0;i<32;i++){
		res[i] = _mul( linear_16_32_10[i][0] , inp[0] );
		for(unsigned j=1;j<32;j++)
			res[i] = _mm_xor_si128( res[i] , _mul( linear_16_32_10[i][j] , inp[j] ));	
	}
	
	__m128i log_inp[32];
	for(unsigned i=0;i<32;i++) log_inp[i] = _mm_shuffle_epi8(_log_tab,inp[i]);
	
	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<10;j++){
			res[i] = _mm_xor_si128(res[i],_mul_2(log_inp[quadratic_16_32_10[i][k]] 
						, log_inp[quadratic_16_32_10[i][k+1]]));
			k+=2;
		}
	}
	
	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<8;j++){
			res[i] = _mm_xor_si128(res[i],_mul_3(log_inp[cubic_16_32_10[i][k]] 
				, log_inp[cubic_16_32_10[i][k+1]] , log_inp[cubic_16_32_10[i][k+2]] ));
			k+=3;
		}
	}	

	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<5;j++){
			res[i] = _mm_xor_si128(res[i],_mul_4(log_inp[quartic_16_32_10[i][k]] 
				, log_inp[quartic_16_32_10[i][k+1]] , log_inp[quartic_16_32_10[i][k+2]] 
				, log_inp[quartic_16_32_10[i][k+3]] ));
			k+=4;
		}
	}
}

void batch_squad_16_32_32_4_10_8_5_ssse3( __m128i * res , __m128i * inp )
{
	__m128i par_inp[32];
	__m128i par_res[32];
	for(unsigned i=0;i<32;i++) par_inp[i] = _mm_and_si128( _mask_lo , inp[i] );
	_batch_squad_16_32_32_4_10_8_5( res , par_inp );
	for(unsigned i=0;i<32;i++) par_inp[i] = _mm_and_si128( _mask_lo , _mm_srli_epi32(inp[i],4) );
	_batch_squad_16_32_32_4_10_8_5( par_res , par_inp );
	for(unsigned i=0;i<32;i++) res[i] = _mm_or_si128( res[i] , _mm_slli_epi32(par_res[i],4) );
}



/////////////////////////////////////////////////////////////////////////////////



//static uint8_t linear_16_32_20[32][32] = {0};
//static uint8_t quadratic_16_32_20[32][20*2] = {0};
//static uint8_t cubic_16_32_20[32][15*3] = {0};
//static uint8_t quartic_16_32_20[32][10*4] = {0};

void _batch_squad_16_32_32_4_20_15_10( __m128i * res , __m128i * inp )
{
	for(unsigned i=0;i<32;i++){
		res[i] = _mul( linear_16_32_20[i][0] , inp[0] );
		for(unsigned j=1;j<32;j++)
			res[i] = _mm_xor_si128( res[i] , _mul( linear_16_32_20[i][j] , inp[j] ));	
	}
	
	__m128i log_inp[32];
	for(unsigned i=0;i<32;i++) log_inp[i] = _mm_shuffle_epi8(_log_tab,inp[i]);
	
	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<20;j++){
			res[i] = _mm_xor_si128(res[i],_mul_2(log_inp[quadratic_16_32_20[i][k]] 
						, log_inp[quadratic_16_32_20[i][k+1]]));
			k+=2;
		}
	}
	
	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<15;j++){
			res[i] = _mm_xor_si128(res[i],_mul_3(log_inp[cubic_16_32_20[i][k]] 
				, log_inp[cubic_16_32_20[i][k+1]] , log_inp[cubic_16_32_20[i][k+2]] ));
			k+=3;
		}
	}	

	for(unsigned i=0;i<32;i++){
		unsigned k=0;
		for(unsigned j=0;j<10;j++){
			res[i] = _mm_xor_si128(res[i],_mul_4(log_inp[quartic_16_32_20[i][k]] 
				, log_inp[quartic_16_32_20[i][k+1]] , log_inp[quartic_16_32_20[i][k+2]] 
				, log_inp[quartic_16_32_20[i][k+3]] ));
			k+=4;
		}
	}
}



void batch_squad_16_32_32_4_20_15_10_ssse3( __m128i * res , __m128i * inp )
{
	__m128i par_inp[32];
	__m128i par_res[32];
	for(unsigned i=0;i<32;i++) par_inp[i] = _mm_and_si128( _mask_lo , inp[i] );
	_batch_squad_16_32_32_4_20_15_10( res , par_inp );
	for(unsigned i=0;i<32;i++) par_inp[i] = _mm_and_si128( _mask_lo , _mm_srli_epi32(inp[i],4) );
	_batch_squad_16_32_32_4_20_15_10( par_res , par_inp );
	for(unsigned i=0;i<32;i++) res[i] = _mm_or_si128( res[i] , _mm_slli_epi32(par_res[i],4) );

}

//////////////////////////////////////////////////////////////////////////////

void _batch_squad_16_96_96_4_32_16_8( __m128i * res , __m128i * inp )
{
	for(unsigned i=0;i<96;i++){
		res[i] = _mul( linear_16_96_32[i][0] , inp[0] );
		for(unsigned j=1;j<96;j++)
			res[i] = _mm_xor_si128( res[i] , _mul( linear_16_96_32[i][j] , inp[j] ));	
	}
	
	__m128i log_inp[96];
	for(unsigned i=0;i<96;i++) log_inp[i] = _mm_shuffle_epi8(_log_tab,inp[i]);
	
	for(unsigned i=0;i<96;i++){
		unsigned k=0;
		for(unsigned j=0;j<32;j++){
			res[i] = _mm_xor_si128(res[i],_mul_2(log_inp[quadratic_16_96_32[i][k]] 
						, log_inp[quadratic_16_96_32[i][k+1]]));
			k+=2;
		}
	}
	
	for(unsigned i=0;i<96;i++){
		unsigned k=0;
		for(unsigned j=0;j<16;j++){
			res[i] = _mm_xor_si128(res[i],_mul_3(log_inp[cubic_16_96_32[i][k]] 
				, log_inp[cubic_16_96_32[i][k+1]] , log_inp[cubic_16_96_32[i][k+2]] ));
			k+=3;
		}
	}	

	for(unsigned i=0;i<96;i++){
		unsigned k=0;
		for(unsigned j=0;j<8;j++){
			res[i] = _mm_xor_si128(res[i],_mul_4(log_inp[quartic_16_96_32[i][k]] 
				, log_inp[quartic_16_96_32[i][k+1]] , log_inp[quartic_16_96_32[i][k+2]] 
				, log_inp[quartic_16_96_32[i][k+3]] ));
			k+=4;
		}
	}
}



void batch_squad_16_96_96_4_32_16_8_ssse3( __m128i * res , __m128i * inp )
{
	__m128i par_inp[96];
	__m128i par_res[96];
	for(unsigned i=0;i<96;i++) par_inp[i] = _mm_and_si128( _mask_lo , inp[i] );
	_batch_squad_16_96_96_4_32_16_8( res , par_inp );
	for(unsigned i=0;i<96;i++) par_inp[i] = _mm_and_si128( _mask_lo , _mm_srli_epi32(inp[i],4) );
	_batch_squad_16_96_96_4_32_16_8( par_res , par_inp );
	for(unsigned i=0;i<96;i++) res[i] = _mm_or_si128( res[i] , _mm_slli_epi32(par_res[i],4) );

}

void _transpose_store_nibble_96_96( __m128i * res , __m128i * inp )
{

	__m128i tmp1[16];
	__m128i tmp2[16];

	for(unsigned i=0;i<96;i+=32){
		unsigned j=i>>5;
		tmp1[0]=_mm_unpacklo_epi8(inp[i+0],inp[i+2]);
		tmp1[8]=_mm_unpackhi_epi8(inp[i+0],inp[i+2]);
		tmp1[1]=_mm_unpacklo_epi8(inp[i+4],inp[i+6]);
		tmp1[9]=_mm_unpackhi_epi8(inp[i+4],inp[i+6]);
		tmp1[2]=_mm_unpacklo_epi8(inp[i+8],inp[i+10]);
		tmp1[10]=_mm_unpackhi_epi8(inp[i+8],inp[i+10]);
		tmp1[3]=_mm_unpacklo_epi8(inp[i+12],inp[i+14]);
		tmp1[11]=_mm_unpackhi_epi8(inp[i+12],inp[i+14]);
		tmp1[4]=_mm_unpacklo_epi8(inp[i+16],inp[i+18]);
		tmp1[12]=_mm_unpackhi_epi8(inp[i+16],inp[i+18]);
		tmp1[5]=_mm_unpacklo_epi8(inp[i+20],inp[i+22]);
		tmp1[13]=_mm_unpackhi_epi8(inp[i+20],inp[i+22]);
		tmp1[6]=_mm_unpacklo_epi8(inp[i+24],inp[i+26]);
		tmp1[14]=_mm_unpackhi_epi8(inp[i+24],inp[i+26]);
		tmp1[7]=_mm_unpacklo_epi8(inp[i+28],inp[i+30]);
		tmp1[15]=_mm_unpackhi_epi8(inp[i+28],inp[i+30]);

		tmp2[0]=_mm_unpacklo_epi16(tmp1[0],tmp1[1]);
		tmp2[4]=_mm_unpackhi_epi16(tmp1[0],tmp1[1]);
		tmp2[1]=_mm_unpacklo_epi16(tmp1[2],tmp1[3]);
		tmp2[5]=_mm_unpackhi_epi16(tmp1[2],tmp1[3]);
		tmp2[2]=_mm_unpacklo_epi16(tmp1[4],tmp1[5]);
		tmp2[6]=_mm_unpackhi_epi16(tmp1[4],tmp1[5]);
		tmp2[3]=_mm_unpacklo_epi16(tmp1[6],tmp1[7]);
		tmp2[7]=_mm_unpackhi_epi16(tmp1[6],tmp1[7]);
		tmp2[8]=_mm_unpacklo_epi16(tmp1[8],tmp1[9]);
		tmp2[12]=_mm_unpackhi_epi16(tmp1[8],tmp1[9]);
		tmp2[9]=_mm_unpacklo_epi16(tmp1[10],tmp1[11]);
		tmp2[13]=_mm_unpackhi_epi16(tmp1[10],tmp1[11]);
		tmp2[10]=_mm_unpacklo_epi16(tmp1[12],tmp1[13]);
		tmp2[14]=_mm_unpackhi_epi16(tmp1[12],tmp1[13]);
		tmp2[11]=_mm_unpacklo_epi16(tmp1[14],tmp1[15]);
		tmp2[15]=_mm_unpackhi_epi16(tmp1[14],tmp1[15]);

		tmp1[0]=_mm_unpacklo_epi32(tmp2[0],tmp2[1]);
		tmp1[2]=_mm_unpackhi_epi32(tmp2[0],tmp2[1]);
		tmp1[1]=_mm_unpacklo_epi32(tmp2[2],tmp2[3]);
		tmp1[3]=_mm_unpackhi_epi32(tmp2[2],tmp2[3]);
		tmp1[4]=_mm_unpacklo_epi32(tmp2[4],tmp2[5]);
		tmp1[6]=_mm_unpackhi_epi32(tmp2[4],tmp2[5]);
		tmp1[5]=_mm_unpacklo_epi32(tmp2[6],tmp2[7]);
		tmp1[7]=_mm_unpackhi_epi32(tmp2[6],tmp2[7]);
		tmp1[8]=_mm_unpacklo_epi32(tmp2[8],tmp2[9]);
		tmp1[10]=_mm_unpackhi_epi32(tmp2[8],tmp2[9]);
		tmp1[9]=_mm_unpacklo_epi32(tmp2[10],tmp2[11]);
		tmp1[11]=_mm_unpackhi_epi32(tmp2[10],tmp2[11]);
		tmp1[12]=_mm_unpacklo_epi32(tmp2[12],tmp2[13]);
		tmp1[14]=_mm_unpackhi_epi32(tmp2[12],tmp2[13]);
		tmp1[13]=_mm_unpacklo_epi32(tmp2[14],tmp2[15]);
		tmp1[15]=_mm_unpackhi_epi32(tmp2[14],tmp2[15]);

		res[j+0]=_mm_unpacklo_epi64(tmp1[0],tmp1[1]);
		res[j+6]=_mm_unpackhi_epi64(tmp1[0],tmp1[1]);
		res[j+12]=_mm_unpacklo_epi64(tmp1[2],tmp1[3]);
		res[j+18]=_mm_unpackhi_epi64(tmp1[2],tmp1[3]);
		res[j+24]=_mm_unpacklo_epi64(tmp1[4],tmp1[5]);
		res[j+30]=_mm_unpackhi_epi64(tmp1[4],tmp1[5]);
		res[j+36]=_mm_unpacklo_epi64(tmp1[6],tmp1[7]);
		res[j+42]=_mm_unpackhi_epi64(tmp1[6],tmp1[7]);
		res[j+48]=_mm_unpacklo_epi64(tmp1[8],tmp1[9]);
		res[j+54]=_mm_unpackhi_epi64(tmp1[8],tmp1[9]);
		res[j+60]=_mm_unpacklo_epi64(tmp1[10],tmp1[11]);
		res[j+66]=_mm_unpackhi_epi64(tmp1[10],tmp1[11]);
		res[j+72]=_mm_unpacklo_epi64(tmp1[12],tmp1[13]);
		res[j+78]=_mm_unpackhi_epi64(tmp1[12],tmp1[13]);
		res[j+84]=_mm_unpacklo_epi64(tmp1[14],tmp1[15]);
		res[j+90]=_mm_unpackhi_epi64(tmp1[14],tmp1[15]);


		tmp1[0]=_mm_unpacklo_epi8(inp[i+1],inp[i+3]);
		tmp1[8]=_mm_unpackhi_epi8(inp[i+1],inp[i+3]);
		tmp1[1]=_mm_unpacklo_epi8(inp[i+5],inp[i+7]);
		tmp1[9]=_mm_unpackhi_epi8(inp[i+5],inp[i+7]);
		tmp1[2]=_mm_unpacklo_epi8(inp[i+9],inp[i+11]);
		tmp1[10]=_mm_unpackhi_epi8(inp[i+9],inp[i+11]);
		tmp1[3]=_mm_unpacklo_epi8(inp[i+13],inp[i+15]);
		tmp1[11]=_mm_unpackhi_epi8(inp[i+13],inp[i+15]);
		tmp1[4]=_mm_unpacklo_epi8(inp[i+17],inp[i+19]);
		tmp1[12]=_mm_unpackhi_epi8(inp[i+17],inp[i+19]);
		tmp1[5]=_mm_unpacklo_epi8(inp[i+21],inp[i+23]);
		tmp1[13]=_mm_unpackhi_epi8(inp[i+21],inp[i+23]);
		tmp1[6]=_mm_unpacklo_epi8(inp[i+25],inp[i+27]);
		tmp1[14]=_mm_unpackhi_epi8(inp[i+25],inp[i+27]);
		tmp1[7]=_mm_unpacklo_epi8(inp[i+29],inp[i+31]);
		tmp1[15]=_mm_unpackhi_epi8(inp[i+29],inp[i+31]);

		tmp2[0]=_mm_unpacklo_epi16(tmp1[0],tmp1[1]);
		tmp2[4]=_mm_unpackhi_epi16(tmp1[0],tmp1[1]);
		tmp2[1]=_mm_unpacklo_epi16(tmp1[2],tmp1[3]);
		tmp2[5]=_mm_unpackhi_epi16(tmp1[2],tmp1[3]);
		tmp2[2]=_mm_unpacklo_epi16(tmp1[4],tmp1[5]);
		tmp2[6]=_mm_unpackhi_epi16(tmp1[4],tmp1[5]);
		tmp2[3]=_mm_unpacklo_epi16(tmp1[6],tmp1[7]);
		tmp2[7]=_mm_unpackhi_epi16(tmp1[6],tmp1[7]);
		tmp2[8]=_mm_unpacklo_epi16(tmp1[8],tmp1[9]);
		tmp2[12]=_mm_unpackhi_epi16(tmp1[8],tmp1[9]);
		tmp2[9]=_mm_unpacklo_epi16(tmp1[10],tmp1[11]);
		tmp2[13]=_mm_unpackhi_epi16(tmp1[10],tmp1[11]);
		tmp2[10]=_mm_unpacklo_epi16(tmp1[12],tmp1[13]);
		tmp2[14]=_mm_unpackhi_epi16(tmp1[12],tmp1[13]);
		tmp2[11]=_mm_unpacklo_epi16(tmp1[14],tmp1[15]);
		tmp2[15]=_mm_unpackhi_epi16(tmp1[14],tmp1[15]);

		tmp1[0]=_mm_unpacklo_epi32(tmp2[0],tmp2[1]);
		tmp1[2]=_mm_unpackhi_epi32(tmp2[0],tmp2[1]);
		tmp1[1]=_mm_unpacklo_epi32(tmp2[2],tmp2[3]);
		tmp1[3]=_mm_unpackhi_epi32(tmp2[2],tmp2[3]);
		tmp1[4]=_mm_unpacklo_epi32(tmp2[4],tmp2[5]);
		tmp1[6]=_mm_unpackhi_epi32(tmp2[4],tmp2[5]);
		tmp1[5]=_mm_unpacklo_epi32(tmp2[6],tmp2[7]);
		tmp1[7]=_mm_unpackhi_epi32(tmp2[6],tmp2[7]);
		tmp1[8]=_mm_unpacklo_epi32(tmp2[8],tmp2[9]);
		tmp1[10]=_mm_unpackhi_epi32(tmp2[8],tmp2[9]);
		tmp1[9]=_mm_unpacklo_epi32(tmp2[10],tmp2[11]);
		tmp1[11]=_mm_unpackhi_epi32(tmp2[10],tmp2[11]);
		tmp1[12]=_mm_unpacklo_epi32(tmp2[12],tmp2[13]);
		tmp1[14]=_mm_unpackhi_epi32(tmp2[12],tmp2[13]);
		tmp1[13]=_mm_unpacklo_epi32(tmp2[14],tmp2[15]);
		tmp1[15]=_mm_unpackhi_epi32(tmp2[14],tmp2[15]);

		res[j+0]=_mm_or_si128(res[j+0],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[0],tmp1[1]),4));
		res[j+6]=_mm_or_si128(res[j+6],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[0],tmp1[1]),4));
		res[j+12]=_mm_or_si128(res[j+12],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[2],tmp1[3]),4));
		res[j+18]=_mm_or_si128(res[j+18],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[2],tmp1[3]),4));
		res[j+24]=_mm_or_si128(res[j+24],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[4],tmp1[5]),4));
		res[j+30]=_mm_or_si128(res[j+30],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[4],tmp1[5]),4));
		res[j+36]=_mm_or_si128(res[j+36],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[6],tmp1[7]),4));
		res[j+42]=_mm_or_si128(res[j+42],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[6],tmp1[7]),4));
		res[j+48]=_mm_or_si128(res[j+48],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[8],tmp1[9]),4));
		res[j+54]=_mm_or_si128(res[j+54],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[8],tmp1[9]),4));
		res[j+60]=_mm_or_si128(res[j+60],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[10],tmp1[11]),4));
		res[j+66]=_mm_or_si128(res[j+66],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[10],tmp1[11]),4));
		res[j+72]=_mm_or_si128(res[j+72],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[12],tmp1[13]),4));
		res[j+78]=_mm_or_si128(res[j+78],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[12],tmp1[13]),4));
		res[j+84]=_mm_or_si128(res[j+84],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[14],tmp1[15]),4));
		res[j+90]=_mm_or_si128(res[j+90],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[14],tmp1[15]),4));

	}


}


void batch_squad_16_96_96_4_32_16_8_transpose_ssse3( __m128i * res , __m128i * inp )
{
	__m128i par_inp[96];
	__m128i par_res[96];
	for(unsigned i=0;i<96;i++) par_inp[i] = _mm_and_si128( _mask_lo , inp[i] );
	_batch_squad_16_96_96_4_32_16_8( par_res , par_inp );

	_transpose_store_nibble_96_96( res , par_res );
	
	for(unsigned i=0;i<96;i++) par_inp[i] = _mm_and_si128( _mask_lo , _mm_srli_epi32(inp[i],4) );
	_batch_squad_16_96_96_4_32_16_8( par_res , par_inp );

	_transpose_store_nibble_96_96( &res[3] , par_res );
}


////////////////////////////////////////////////////////////////////



