
#include <stdio.h>
#include <stdlib.h>

#include <time.h>

// every polynomial contains 112 linear term <-- dense
//static uint8_t linear[224][112] = {0};

// 224 * 448 <-- quadratic term
// 224 * 112 <-- num of monomial
// every quadratic monomial belong to 4 polynomail
//static uint8_t quadratic[224*112][4] = {0};

// every polynomial contains 20 cubic term
//static uint8_t cubic[224][20*3] = {0}; 

#define NUM_VAR 224
#define NUM_QUAD 4
#define NUM_CUBIC 20


int main()
{
	srand( time(NULL) );
	
//static uint8_t linear[224][112] = {0};
	printf("#ifndef _KEY_2_%d_\n",NUM_VAR);
	printf("#define _KEY_2_%d_\n",NUM_VAR);
	printf("static uint8_t linear_2_%d[%d][%d] = {\n",NUM_VAR,NUM_VAR,NUM_VAR/2);
	for(unsigned i=0;i<NUM_VAR-1;i++){
		printf("{");
		for(unsigned j=0;j<NUM_VAR/2-1;j++){
			printf("%d,",rand()%NUM_VAR);
		}
		printf("%d},\n",rand()%NUM_VAR);
	}	
	printf("{");
	for(unsigned j=0;j<NUM_VAR/2-1;j++){
		printf("%d,",rand()%NUM_VAR);
	}
	printf("%d}\n",rand()%NUM_VAR);
	printf("};\n");
	
//static uint8_t quadratic[224*112][4] = {0};
	printf("static uint8_t quadratic_2_%d[%d*%d][%d] = {\n",NUM_VAR,NUM_VAR,NUM_VAR/2,NUM_QUAD);
	for(unsigned i=0;i<NUM_VAR*NUM_VAR/2-1;i++){
		printf("{%d,%d,%d,%d},\n",rand()%NUM_VAR,rand()%NUM_VAR,rand()%NUM_VAR,rand()%NUM_VAR);
	}
	printf("{%d,%d,%d,%d}\n",rand()%NUM_VAR,rand()%NUM_VAR,rand()%NUM_VAR,rand()%NUM_VAR);
	printf("};\n");
	
//static uint8_t cubic[224][20*3] = {0}; 
	printf("static uint8_t cubic_2_%d[%d][%d*3] = {\n",NUM_VAR,NUM_VAR,NUM_CUBIC);
	for(unsigned i=0;i<NUM_VAR-1;i++){
		printf("{");
		for(unsigned j=0;j<NUM_CUBIC*3-1;j++)
			printf("%d,",rand()%NUM_VAR);
		printf("%d},\n",rand()%NUM_VAR);
	}
	printf("{");
	for(unsigned j=0;j<NUM_CUBIC*3-1;j++)
		printf("%d,",rand()%NUM_VAR);
	printf("%d}\n",rand()%NUM_VAR);
	printf("};\n");
	
	printf("#endif");

	return 0;
}
