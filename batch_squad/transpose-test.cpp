


#include "benchmark.h"
#include "transpose.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <iostream>


#define NUM_TEST 1

#define NUM_INP 32
#define NUM_RES 32

void print( __m128i a )
{
	uint8_t * ptr_byte = (uint8_t *)(& a);
	for(int i=0;i<16;i++) printf( "%2X,",ptr_byte[i] ); printf("\n");
}

void gen_data( __m128i * a )
{
	uint8_t * ptr_byte = (uint8_t *)a;

	uint8_t c = 0x00;
	for(int i=0;i<NUM_INP;i++){
		for(int j=0;j<16;j++) ptr_byte[j] = c++;
		c = 0;
		ptr_byte += 16;
	}
}


int main()
{
	
	__m128i inp[NUM_INP];
	__m128i res[NUM_RES];
	
	__m128i * ptr0 = inp;
	__m128i * ptr1 = res;
	gen_data( inp );
	
	benchmark bench;
	bm_init( & bench );
	
	for(unsigned i=0;i<NUM_TEST;i++){
		BENCHMARK( bench , {
//			transpose_byte(res,inp);
			transpose_nibble(res,inp);
		});
		__m128i * t = ptr1;
		ptr1 = ptr0;
		ptr0 = t;
	}

	for(int i=0;i<NUM_INP;i++) print( inp[i] );
	printf("\n");
	for(int i=0;i<NUM_RES;i++) print( res[i] );
	
	
	char buff[2048];
	bm_dump( buff , 2048 , &bench );
	std::cout << "\n" << buff << "\n";
	
	return 0;	
}






