
#include "batch_squad.h"

#include <stdlib.h>
#include <string.h>

#include <stdint.h>



#include "key_2_208.cpp"


#define XOR(x,y) _mm_xor_si128((x),(y))
#define AND(x,y) _mm_and_si128((x),(y))


// L1 cache size: 2048 xmm register
void batch_squad_2_208_208_3_480_20( __m128i * res , __m128i * inp )
{
// linear:
	for(unsigned i=0;i<208;i++){
		res[i] = inp[linear_2_208[i][0]];
		for(unsigned j=1;j<104;j++)
			res[i] = XOR( res[i] , inp[linear_2_208[i][j]] );
	}
// cubic:
	for(unsigned i=0;i<208;i++){
		unsigned j=0;
		for( unsigned k=0;k<20;k++) {
			res[i] = 
				XOR(res[i],AND(AND(inp[cubic_2_208[i][j]],inp[cubic_2_208[i][j+1]]),inp[cubic_2_208[i][j+2]]) );
			j += 3;
		}
	}
// quadratic:
	unsigned c = 0;
	for( unsigned i=0;i<208;i++){
		for( unsigned j=i;j<208;j++){
			__m128i r = AND( inp[i] , inp[j] );
			for(unsigned k=0;k<5;k++)
				res[quadratic_2_208[c][k]] = XOR( res[quadratic_2_208[c][k]] , r );
			c++;
		}
	}
}


#include "key_2_224.cpp"

void batch_squad_2_224_224_3_448_20( __m128i * res , __m128i * inp )
{
// linear:
	for(unsigned i=0;i<224;i++){
		res[i] = inp[linear_2_224[i][0]];
		for(unsigned j=1;j<112;j++)
			res[i] = XOR( res[i] , inp[linear_2_224[i][j]] );
	}
// cubic:
	for(unsigned i=0;i<224;i++){
		unsigned j=0;
		for( unsigned k=0;k<20;k++) {
			res[i] = 
				XOR(res[i],AND(AND(inp[cubic_2_224[i][j]],inp[cubic_2_224[i][j+1]]),inp[cubic_2_224[i][j+2]]) );
			j += 3;
		}
	}
// quadratic:
	unsigned c = 0;
	for( unsigned i=0;i<224;i++){
		for( unsigned j=i;j<224;j++){
			__m128i r = AND( inp[i] , inp[j] );
			for(unsigned k=0;k<4;k++)
				res[quadratic_2_224[c][k]] = XOR( res[quadratic_2_224[c][k]] , r );
			c++;
		}
	}
}


