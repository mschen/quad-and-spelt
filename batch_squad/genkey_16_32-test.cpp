

#include <stdio.h>
#include <stdlib.h>

#include <time.h>

//static uint8_t linear_16_32_10[32][32] = {0};
//static uint8_t quadratic_16_32_10[32][10*2] = {0};
//static uint8_t cubic_16_32_10[32][8*3] = {0};
//static uint8_t quartic_16_32_10[32][4*5] = {0};

//static uint8_t linear_16_32_20[32][32] = {0};
//static uint8_t quadratic_16_32_20[32][20*2] = {0};
//static uint8_t cubic_16_32_20[32][15*3] = {0};
//static uint8_t quartic_16_32_20[32][10*4] = {0};



int main()
{
	srand( time(NULL) );

	printf("#ifndef _KEY_16_32_\n");
	printf("#define _KEY_16_32_\n");
	
	unsigned int num_var = 32;
	unsigned int num_2 = 10;
	unsigned int num_3 = 8;
	unsigned int num_4 = 5;
	
	printf("static uint8_t linear_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_var);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_var-1;j++){
			printf("%d,",rand()%16);
		}
		printf("%d},\n",rand()%16);
	}	
	printf("{");
	for(unsigned j=0;j<num_var-1;j++){
		printf("%d,",rand()%16);
	}
	printf("%d}\n",rand()%16);
	printf("};\n");

	printf("static uint8_t quadratic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_2*2);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_2*2-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_2*2-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");

	printf("static uint8_t cubic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_3*3);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_3*3-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_3*3-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");

	printf("static uint8_t quartic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_4*4);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_4*4-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_4*4-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");
	
	num_2 = 20;
	num_3 = 15;
	num_4 = 10;
	
	printf("static uint8_t linear_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_var);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_var-1;j++){
			printf("%d,",rand()%16);
		}
		printf("%d},\n",rand()%16);
	}	
	printf("{");
	for(unsigned j=0;j<num_var-1;j++){
		printf("%d,",rand()%16);
	}
	printf("%d}\n",rand()%16);
	printf("};\n");

	printf("static uint8_t quadratic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_2*2);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_2*2-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_2*2-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");

	printf("static uint8_t cubic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_3*3);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_3*3-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_3*3-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");

	printf("static uint8_t quartic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_4*4);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_4*4-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_4*4-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");

	num_var = 96;
	num_2 = 32;
	num_3 = 16;
	num_4 = 8;
	
	printf("static uint8_t linear_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_var);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_var-1;j++){
			printf("%d,",rand()%16);
		}
		printf("%d},\n",rand()%16);
	}	
	printf("{");
	for(unsigned j=0;j<num_var-1;j++){
		printf("%d,",rand()%16);
	}
	printf("%d}\n",rand()%16);
	printf("};\n");

	printf("static uint8_t quadratic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_2*2);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_2*2-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_2*2-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");

	printf("static uint8_t cubic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_3*3);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_3*3-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_3*3-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");

	printf("static uint8_t quartic_16_%d_%d[%d][%d] = {\n",num_var,num_2,num_var,num_4*4);
	for(unsigned i=0;i<num_var-1;i++){
		printf("{");
		for(unsigned j=0;j<num_4*4-1;j++){
			printf("%d,",rand()%num_var);
		}
		printf("%d},\n",rand()%num_var);
	}	
	printf("{");
	for(unsigned j=0;j<num_4*4-1;j++){
		printf("%d,",rand()%num_var);
	}
	printf("%d}\n",rand()%num_var);
	printf("};\n");
	
	printf("#endif");

	return 0;
}

