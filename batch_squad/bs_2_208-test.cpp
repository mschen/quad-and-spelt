
#include "benchmark.h"
#include "batch_squad.h"

#include <stdlib.h>
#include <iostream>


void rand_inp_data(__m128i * data );

#define NUM_TEST 10000
#define NUM_128_INP 208

int main()
{
	__m128i inp[NUM_128_INP];
	__m128i res[NUM_128_INP];
	
	rand_inp_data(inp);
	
	__m128i * ptr0 = inp;
	__m128i * ptr1 = res;
	
	benchmark bench;
	bm_init( & bench );
	
	for(unsigned i=0;i<NUM_TEST;i++){
		BENCHMARK( bench , {
			batch_squad_2_208_208_3_480_20( ptr1 , ptr0 );
//			batch_squad_2_224_224_3_448_20( ptr1 , ptr0 );
		});
		__m128i * t = ptr1;
		ptr1 = ptr0;
		ptr0 = t;
	}
	uint32_t * rr = (uint32_t*)res;
	std::cout << rr[0] << "\n";
	
	char buff[2048];
	bm_dump( buff , 2048 , &bench );
	std::cout << "\n" << buff << "\n";
	
	return 0;	
}

void rand_inp_data(__m128i * data )
{
	uint16_t * d = (uint16_t*) data;
	
	for(unsigned i=0;i<NUM_128_INP*8;i++){
		d[i] = (uint16_t)(rand() & 0xffff);
	}
}


