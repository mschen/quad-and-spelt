
#include "benchmark.h"
#include "batch_squad.h"

#include <stdlib.h>
#include <stdio.h>
#include <iostream>


void rand_inp_data(__m128i * data );

#define NUM_TEST 10000
#define NUM_128_INP 96




int main()
{
	
	__m128i inp[NUM_128_INP];
	__m128i res[NUM_128_INP];
	
	rand_inp_data(inp);
	
	__m128i * ptr0 = inp;
	__m128i * ptr1 = res;
	
	benchmark bench,bench2;
	bm_init( & bench );
	bm_init( & bench2 );

#ifndef _SSSE3_
	std::cout << "\n\n****************  type make SSSE3=1 *****************\n\n\n";
#endif
	
	for(unsigned i=0;i<NUM_TEST;i++){
		BENCHMARK( bench , {
//			batch_squad_2_208_208_3_480_20( ptr1 , ptr0 );
//			batch_squad_2_224_224_3_448_20( ptr1 , ptr0 );
//			batch_squad_16_32_32_4_10_8_5( ptr1 , ptr0 );
//			batch_squad_16_32_32_4_20_15_10( ptr1 , ptr0 );
//			batch_squad_16_96_96_4_32_16_8( ptr1 , ptr0 );
#ifdef _SSSE3_
//			batch_squad_16_96_96_4_32_16_8_ssse3( ptr1 , ptr0 );
			batch_squad_16_96_96_4_32_16_8_transpose_ssse3( ptr1 , ptr0 );
#endif
		});
		BENCHMARK( bench2 , {
#ifdef _SSSE3_
			batch_squad_16_96_96_4_32_16_8_ssse3( ptr1 , ptr0 );
#endif
		});
		__m128i * t = ptr1;
		ptr1 = ptr0;
		ptr0 = t;
	}
	uint32_t * rr = (uint32_t*)res;
	std::cout << rr[0] << "\n";

	std::cout << "batch 32 gf16 squad (16,96,96,(4,32,16,8)) with transpose\n";
	char buff[2048];
	bm_dump( buff , 2048 , &bench );
	std::cout << buff << "\n\n";

	std::cout << "WITHOUT transpose.\n";
	bm_dump( buff , 2048 , &bench2 );
	std::cout << buff << "\n\n";
	
	return 0;	
}

void rand_inp_data(__m128i * data )
{
	uint16_t * d = (uint16_t*) data;
	
	for(unsigned i=0;i<NUM_128_INP*8;i++){
		d[i] = (uint16_t)(rand() & 0xffff);
	}
}


