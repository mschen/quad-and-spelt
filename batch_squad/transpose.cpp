


#include <emmintrin.h>

#include <stdlib.h>
#include <string.h>
#include <stdint.h>




void transpose_byte( __m128i * res , __m128i * inp )
{
	__m128i tmp1[16];
	__m128i tmp2[16];

	tmp1[0]=_mm_unpacklo_epi8(inp[0],inp[1]);
	tmp1[8]=_mm_unpackhi_epi8(inp[0],inp[1]);
	tmp1[1]=_mm_unpacklo_epi8(inp[2],inp[3]);
	tmp1[9]=_mm_unpackhi_epi8(inp[2],inp[3]);
	tmp1[2]=_mm_unpacklo_epi8(inp[4],inp[5]);
	tmp1[10]=_mm_unpackhi_epi8(inp[4],inp[5]);
	tmp1[3]=_mm_unpacklo_epi8(inp[6],inp[7]);
	tmp1[11]=_mm_unpackhi_epi8(inp[6],inp[7]);
	tmp1[4]=_mm_unpacklo_epi8(inp[8],inp[9]);
	tmp1[12]=_mm_unpackhi_epi8(inp[8],inp[9]);
	tmp1[5]=_mm_unpacklo_epi8(inp[10],inp[11]);
	tmp1[13]=_mm_unpackhi_epi8(inp[10],inp[11]);
	tmp1[6]=_mm_unpacklo_epi8(inp[12],inp[13]);
	tmp1[14]=_mm_unpackhi_epi8(inp[12],inp[13]);
	tmp1[7]=_mm_unpacklo_epi8(inp[14],inp[15]);
	tmp1[15]=_mm_unpackhi_epi8(inp[14],inp[15]);

	tmp2[0]=_mm_unpacklo_epi16(tmp1[0],tmp1[1]);
	tmp2[4]=_mm_unpackhi_epi16(tmp1[0],tmp1[1]);
	tmp2[1]=_mm_unpacklo_epi16(tmp1[2],tmp1[3]);
	tmp2[5]=_mm_unpackhi_epi16(tmp1[2],tmp1[3]);
	tmp2[2]=_mm_unpacklo_epi16(tmp1[4],tmp1[5]);
	tmp2[6]=_mm_unpackhi_epi16(tmp1[4],tmp1[5]);
	tmp2[3]=_mm_unpacklo_epi16(tmp1[6],tmp1[7]);
	tmp2[7]=_mm_unpackhi_epi16(tmp1[6],tmp1[7]);
	tmp2[8]=_mm_unpacklo_epi16(tmp1[8],tmp1[9]);
	tmp2[12]=_mm_unpackhi_epi16(tmp1[8],tmp1[9]);
	tmp2[9]=_mm_unpacklo_epi16(tmp1[10],tmp1[11]);
	tmp2[13]=_mm_unpackhi_epi16(tmp1[10],tmp1[11]);
	tmp2[10]=_mm_unpacklo_epi16(tmp1[12],tmp1[13]);
	tmp2[14]=_mm_unpackhi_epi16(tmp1[12],tmp1[13]);
	tmp2[11]=_mm_unpacklo_epi16(tmp1[14],tmp1[15]);
	tmp2[15]=_mm_unpackhi_epi16(tmp1[14],tmp1[15]);

	tmp1[0]=_mm_unpacklo_epi32(tmp2[0],tmp2[1]);
	tmp1[2]=_mm_unpackhi_epi32(tmp2[0],tmp2[1]);
	tmp1[1]=_mm_unpacklo_epi32(tmp2[2],tmp2[3]);
	tmp1[3]=_mm_unpackhi_epi32(tmp2[2],tmp2[3]);
	tmp1[4]=_mm_unpacklo_epi32(tmp2[4],tmp2[5]);
	tmp1[6]=_mm_unpackhi_epi32(tmp2[4],tmp2[5]);
	tmp1[5]=_mm_unpacklo_epi32(tmp2[6],tmp2[7]);
	tmp1[7]=_mm_unpackhi_epi32(tmp2[6],tmp2[7]);
	tmp1[8]=_mm_unpacklo_epi32(tmp2[8],tmp2[9]);
	tmp1[10]=_mm_unpackhi_epi32(tmp2[8],tmp2[9]);
	tmp1[9]=_mm_unpacklo_epi32(tmp2[10],tmp2[11]);
	tmp1[11]=_mm_unpackhi_epi32(tmp2[10],tmp2[11]);
	tmp1[12]=_mm_unpacklo_epi32(tmp2[12],tmp2[13]);
	tmp1[14]=_mm_unpackhi_epi32(tmp2[12],tmp2[13]);
	tmp1[13]=_mm_unpacklo_epi32(tmp2[14],tmp2[15]);
	tmp1[15]=_mm_unpackhi_epi32(tmp2[14],tmp2[15]);

	res[0]=_mm_unpacklo_epi64(tmp1[0],tmp1[1]);
	res[1]=_mm_unpackhi_epi64(tmp1[0],tmp1[1]);
	res[2]=_mm_unpacklo_epi64(tmp1[2],tmp1[3]);
	res[3]=_mm_unpackhi_epi64(tmp1[2],tmp1[3]);
	res[4]=_mm_unpacklo_epi64(tmp1[4],tmp1[5]);
	res[5]=_mm_unpackhi_epi64(tmp1[4],tmp1[5]);
	res[6]=_mm_unpacklo_epi64(tmp1[6],tmp1[7]);
	res[7]=_mm_unpackhi_epi64(tmp1[6],tmp1[7]);
	res[8]=_mm_unpacklo_epi64(tmp1[8],tmp1[9]);
	res[9]=_mm_unpackhi_epi64(tmp1[8],tmp1[9]);
	res[10]=_mm_unpacklo_epi64(tmp1[10],tmp1[11]);
	res[11]=_mm_unpackhi_epi64(tmp1[10],tmp1[11]);
	res[12]=_mm_unpacklo_epi64(tmp1[12],tmp1[13]);
	res[13]=_mm_unpackhi_epi64(tmp1[12],tmp1[13]);
	res[14]=_mm_unpacklo_epi64(tmp1[14],tmp1[15]);
	res[15]=_mm_unpackhi_epi64(tmp1[14],tmp1[15]);
}

void transpose_nibble( __m128i * res , __m128i * inp )
{

	__m128i tmp1[16];
	__m128i tmp2[16];

	tmp1[0]=_mm_unpacklo_epi8(inp[0],inp[2]);
	tmp1[8]=_mm_unpackhi_epi8(inp[0],inp[2]);
	tmp1[1]=_mm_unpacklo_epi8(inp[4],inp[6]);
	tmp1[9]=_mm_unpackhi_epi8(inp[4],inp[6]);
	tmp1[2]=_mm_unpacklo_epi8(inp[8],inp[10]);
	tmp1[10]=_mm_unpackhi_epi8(inp[8],inp[10]);
	tmp1[3]=_mm_unpacklo_epi8(inp[12],inp[14]);
	tmp1[11]=_mm_unpackhi_epi8(inp[12],inp[14]);
	tmp1[4]=_mm_unpacklo_epi8(inp[16],inp[18]);
	tmp1[12]=_mm_unpackhi_epi8(inp[16],inp[18]);
	tmp1[5]=_mm_unpacklo_epi8(inp[20],inp[22]);
	tmp1[13]=_mm_unpackhi_epi8(inp[20],inp[22]);
	tmp1[6]=_mm_unpacklo_epi8(inp[24],inp[26]);
	tmp1[14]=_mm_unpackhi_epi8(inp[24],inp[26]);
	tmp1[7]=_mm_unpacklo_epi8(inp[28],inp[30]);
	tmp1[15]=_mm_unpackhi_epi8(inp[28],inp[30]);

	tmp2[0]=_mm_unpacklo_epi16(tmp1[0],tmp1[1]);
	tmp2[4]=_mm_unpackhi_epi16(tmp1[0],tmp1[1]);
	tmp2[1]=_mm_unpacklo_epi16(tmp1[2],tmp1[3]);
	tmp2[5]=_mm_unpackhi_epi16(tmp1[2],tmp1[3]);
	tmp2[2]=_mm_unpacklo_epi16(tmp1[4],tmp1[5]);
	tmp2[6]=_mm_unpackhi_epi16(tmp1[4],tmp1[5]);
	tmp2[3]=_mm_unpacklo_epi16(tmp1[6],tmp1[7]);
	tmp2[7]=_mm_unpackhi_epi16(tmp1[6],tmp1[7]);
	tmp2[8]=_mm_unpacklo_epi16(tmp1[8],tmp1[9]);
	tmp2[12]=_mm_unpackhi_epi16(tmp1[8],tmp1[9]);
	tmp2[9]=_mm_unpacklo_epi16(tmp1[10],tmp1[11]);
	tmp2[13]=_mm_unpackhi_epi16(tmp1[10],tmp1[11]);
	tmp2[10]=_mm_unpacklo_epi16(tmp1[12],tmp1[13]);
	tmp2[14]=_mm_unpackhi_epi16(tmp1[12],tmp1[13]);
	tmp2[11]=_mm_unpacklo_epi16(tmp1[14],tmp1[15]);
	tmp2[15]=_mm_unpackhi_epi16(tmp1[14],tmp1[15]);

	tmp1[0]=_mm_unpacklo_epi32(tmp2[0],tmp2[1]);
	tmp1[2]=_mm_unpackhi_epi32(tmp2[0],tmp2[1]);
	tmp1[1]=_mm_unpacklo_epi32(tmp2[2],tmp2[3]);
	tmp1[3]=_mm_unpackhi_epi32(tmp2[2],tmp2[3]);
	tmp1[4]=_mm_unpacklo_epi32(tmp2[4],tmp2[5]);
	tmp1[6]=_mm_unpackhi_epi32(tmp2[4],tmp2[5]);
	tmp1[5]=_mm_unpacklo_epi32(tmp2[6],tmp2[7]);
	tmp1[7]=_mm_unpackhi_epi32(tmp2[6],tmp2[7]);
	tmp1[8]=_mm_unpacklo_epi32(tmp2[8],tmp2[9]);
	tmp1[10]=_mm_unpackhi_epi32(tmp2[8],tmp2[9]);
	tmp1[9]=_mm_unpacklo_epi32(tmp2[10],tmp2[11]);
	tmp1[11]=_mm_unpackhi_epi32(tmp2[10],tmp2[11]);
	tmp1[12]=_mm_unpacklo_epi32(tmp2[12],tmp2[13]);
	tmp1[14]=_mm_unpackhi_epi32(tmp2[12],tmp2[13]);
	tmp1[13]=_mm_unpacklo_epi32(tmp2[14],tmp2[15]);
	tmp1[15]=_mm_unpackhi_epi32(tmp2[14],tmp2[15]);

	res[0]=_mm_unpacklo_epi64(tmp1[0],tmp1[1]);
	res[2]=_mm_unpackhi_epi64(tmp1[0],tmp1[1]);
	res[4]=_mm_unpacklo_epi64(tmp1[2],tmp1[3]);
	res[6]=_mm_unpackhi_epi64(tmp1[2],tmp1[3]);
	res[8]=_mm_unpacklo_epi64(tmp1[4],tmp1[5]);
	res[10]=_mm_unpackhi_epi64(tmp1[4],tmp1[5]);
	res[12]=_mm_unpacklo_epi64(tmp1[6],tmp1[7]);
	res[14]=_mm_unpackhi_epi64(tmp1[6],tmp1[7]);
	res[16]=_mm_unpacklo_epi64(tmp1[8],tmp1[9]);
	res[18]=_mm_unpackhi_epi64(tmp1[8],tmp1[9]);
	res[20]=_mm_unpacklo_epi64(tmp1[10],tmp1[11]);
	res[22]=_mm_unpackhi_epi64(tmp1[10],tmp1[11]);
	res[24]=_mm_unpacklo_epi64(tmp1[12],tmp1[13]);
	res[26]=_mm_unpackhi_epi64(tmp1[12],tmp1[13]);
	res[28]=_mm_unpacklo_epi64(tmp1[14],tmp1[15]);
	res[30]=_mm_unpackhi_epi64(tmp1[14],tmp1[15]);


	tmp1[0]=_mm_unpacklo_epi8(inp[1],inp[3]);
	tmp1[8]=_mm_unpackhi_epi8(inp[1],inp[3]);
	tmp1[1]=_mm_unpacklo_epi8(inp[5],inp[7]);
	tmp1[9]=_mm_unpackhi_epi8(inp[5],inp[7]);
	tmp1[2]=_mm_unpacklo_epi8(inp[9],inp[11]);
	tmp1[10]=_mm_unpackhi_epi8(inp[9],inp[11]);
	tmp1[3]=_mm_unpacklo_epi8(inp[13],inp[15]);
	tmp1[11]=_mm_unpackhi_epi8(inp[13],inp[15]);
	tmp1[4]=_mm_unpacklo_epi8(inp[17],inp[19]);
	tmp1[12]=_mm_unpackhi_epi8(inp[17],inp[19]);
	tmp1[5]=_mm_unpacklo_epi8(inp[21],inp[23]);
	tmp1[13]=_mm_unpackhi_epi8(inp[21],inp[23]);
	tmp1[6]=_mm_unpacklo_epi8(inp[25],inp[27]);
	tmp1[14]=_mm_unpackhi_epi8(inp[25],inp[27]);
	tmp1[7]=_mm_unpacklo_epi8(inp[29],inp[31]);
	tmp1[15]=_mm_unpackhi_epi8(inp[29],inp[31]);

	tmp2[0]=_mm_unpacklo_epi16(tmp1[0],tmp1[1]);
	tmp2[4]=_mm_unpackhi_epi16(tmp1[0],tmp1[1]);
	tmp2[1]=_mm_unpacklo_epi16(tmp1[2],tmp1[3]);
	tmp2[5]=_mm_unpackhi_epi16(tmp1[2],tmp1[3]);
	tmp2[2]=_mm_unpacklo_epi16(tmp1[4],tmp1[5]);
	tmp2[6]=_mm_unpackhi_epi16(tmp1[4],tmp1[5]);
	tmp2[3]=_mm_unpacklo_epi16(tmp1[6],tmp1[7]);
	tmp2[7]=_mm_unpackhi_epi16(tmp1[6],tmp1[7]);
	tmp2[8]=_mm_unpacklo_epi16(tmp1[8],tmp1[9]);
	tmp2[12]=_mm_unpackhi_epi16(tmp1[8],tmp1[9]);
	tmp2[9]=_mm_unpacklo_epi16(tmp1[10],tmp1[11]);
	tmp2[13]=_mm_unpackhi_epi16(tmp1[10],tmp1[11]);
	tmp2[10]=_mm_unpacklo_epi16(tmp1[12],tmp1[13]);
	tmp2[14]=_mm_unpackhi_epi16(tmp1[12],tmp1[13]);
	tmp2[11]=_mm_unpacklo_epi16(tmp1[14],tmp1[15]);
	tmp2[15]=_mm_unpackhi_epi16(tmp1[14],tmp1[15]);

	tmp1[0]=_mm_unpacklo_epi32(tmp2[0],tmp2[1]);
	tmp1[2]=_mm_unpackhi_epi32(tmp2[0],tmp2[1]);
	tmp1[1]=_mm_unpacklo_epi32(tmp2[2],tmp2[3]);
	tmp1[3]=_mm_unpackhi_epi32(tmp2[2],tmp2[3]);
	tmp1[4]=_mm_unpacklo_epi32(tmp2[4],tmp2[5]);
	tmp1[6]=_mm_unpackhi_epi32(tmp2[4],tmp2[5]);
	tmp1[5]=_mm_unpacklo_epi32(tmp2[6],tmp2[7]);
	tmp1[7]=_mm_unpackhi_epi32(tmp2[6],tmp2[7]);
	tmp1[8]=_mm_unpacklo_epi32(tmp2[8],tmp2[9]);
	tmp1[10]=_mm_unpackhi_epi32(tmp2[8],tmp2[9]);
	tmp1[9]=_mm_unpacklo_epi32(tmp2[10],tmp2[11]);
	tmp1[11]=_mm_unpackhi_epi32(tmp2[10],tmp2[11]);
	tmp1[12]=_mm_unpacklo_epi32(tmp2[12],tmp2[13]);
	tmp1[14]=_mm_unpackhi_epi32(tmp2[12],tmp2[13]);
	tmp1[13]=_mm_unpacklo_epi32(tmp2[14],tmp2[15]);
	tmp1[15]=_mm_unpackhi_epi32(tmp2[14],tmp2[15]);

	res[0]=_mm_or_si128(res[0],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[0],tmp1[1]),4));
	res[2]=_mm_or_si128(res[2],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[0],tmp1[1]),4));
	res[4]=_mm_or_si128(res[4],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[2],tmp1[3]),4));
	res[6]=_mm_or_si128(res[6],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[2],tmp1[3]),4));
	res[8]=_mm_or_si128(res[8],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[4],tmp1[5]),4));
	res[10]=_mm_or_si128(res[10],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[4],tmp1[5]),4));
	res[12]=_mm_or_si128(res[12],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[6],tmp1[7]),4));
	res[14]=_mm_or_si128(res[14],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[6],tmp1[7]),4));
	res[16]=_mm_or_si128(res[16],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[8],tmp1[9]),4));
	res[18]=_mm_or_si128(res[18],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[8],tmp1[9]),4));
	res[20]=_mm_or_si128(res[20],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[10],tmp1[11]),4));
	res[22]=_mm_or_si128(res[22],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[10],tmp1[11]),4));
	res[24]=_mm_or_si128(res[24],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[12],tmp1[13]),4));
	res[26]=_mm_or_si128(res[26],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[12],tmp1[13]),4));
	res[28]=_mm_or_si128(res[28],_mm_slli_epi32(_mm_unpacklo_epi64(tmp1[14],tmp1[15]),4));
	res[30]=_mm_or_si128(res[30],_mm_slli_epi32(_mm_unpackhi_epi64(tmp1[14],tmp1[15]),4));

}

