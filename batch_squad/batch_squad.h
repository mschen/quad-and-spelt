#ifndef _BATCH_SQUAD_H_
#define _BATCH_SQUAD_H_

#include <emmintrin.h>  /// sse2

void batch_squad_2_208_208_3_480_20( __m128i * res , __m128i * inp );

void batch_squad_2_224_224_3_448_20( __m128i * res , __m128i * inp );




void batch_squad_16_32_32_4_10_8_5( __m128i * res , __m128i * inp );

void batch_squad_16_32_32_4_20_15_10( __m128i * res , __m128i * inp );


void batch_squad_16_96_96_4_32_16_8( __m128i * res , __m128i * inp );




void batch_squad_16_32_32_4_10_8_5_ssse3( __m128i * res , __m128i * inp );

void batch_squad_16_32_32_4_20_15_10_ssse3( __m128i * res , __m128i * inp );

void batch_squad_16_96_96_4_32_16_8_ssse3( __m128i * res , __m128i * inp );

void batch_squad_16_96_96_4_32_16_8_transpose_ssse3( __m128i * res , __m128i * inp );



#endif
