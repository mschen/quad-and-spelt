
#include <stdio.h>
#include <stdlib.h>

#include <time.h>

// every polynomial contains 104 linear term <-- dense
//static uint8_t linear[208][104] = {0};

// 208 * 480 <-- quadratic term
// 208 * 104 <-- num of monomial
// every quadratic monomial belong to 5 polynomail
//static uint8_t quadratic[208*104][5] = {0};

// every polynomial contains 20 cubic term
//static uint8_t cubic[208][20*3] = {0}; 


int main()
{
	srand( time(NULL) );
	
//static uint8_t linear[208][104] = {0};
	printf("#ifndef _KEY_2_208_\n");
	printf("#define _KEY_2_208_\n");
	printf("static uint8_t linear_2_208[208][104] = {\n");
	for(unsigned i=0;i<207;i++){
		
		printf("{");
		for(unsigned j=0;j<103;j++){
			printf("%d,",rand()%208);
		}
		printf("%d},\n",rand()%208);
	}	
	printf("{");
	for(unsigned j=0;j<103;j++){
		printf("%d,",rand()%208);
	}
	printf("%d}\n",rand()%208);
	
	printf("};\n");
	
//static uint8_t quadratic[208*104][5] = {0};
	printf("static uint8_t quadratic_2_208[208*104][5] = {\n");
	for(unsigned i=0;i<208*104-1;i++){
		printf("{%d,%d,%d,%d,%d},\n",rand()%208,rand()%208,rand()%208,rand()%208,rand()%208);
	}
	printf("{%d,%d,%d,%d,%d}\n",rand()%208,rand()%208,rand()%208,rand()%208,rand()%208);
	printf("};\n");
	
//static uint8_t cubic[208][20*3] = {0}; 
	printf("static uint8_t cubic_2_208[208][20*3] = {\n");
	for(unsigned i=0;i<207;i++){
		printf("{");
		for(unsigned j=0;j<59;j++)
			printf("%d,",rand()%208);
		printf("%d},\n",rand()%208);
	}
	printf("{");
	for(unsigned j=0;j<59;j++)
		printf("%d,",rand()%208);
	printf("%d}\n",rand()%208);
	printf("};\n");
	
	printf("#endif");

	return 0;
}
